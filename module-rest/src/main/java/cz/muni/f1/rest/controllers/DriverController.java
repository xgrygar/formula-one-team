package cz.muni.f1.rest.controllers;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.Driver.DriverCreateDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.facade.DriverFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@RestController
@RequestMapping("/driver")
public class DriverController {
    private static final Logger logger = LoggerFactory.getLogger(DriverController.class);

    @Inject
    private DriverFacade driverFacade;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DriverDto>> findAllDrivers() {
        logger.debug("/driver/");
        return ResponseEntity.ok(driverFacade.findAllDrivers());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DriverDto> findDriverById(@PathVariable Long id) {
        logger.debug("/driver/{}", id);
        return ResponseEntity.ok(driverFacade.getDriverById(id));
    }

    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DriverDto> createDriver(@RequestBody DriverCreateDto driverCreateDto) {
        logger.debug("/driver/create: {}", driverCreateDto);
        try {
            return ResponseEntity.ok(driverFacade.createDriverRest(driverCreateDto));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/delete", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteDriver(@RequestBody BaseDto baseDto) {
        Long driverID = baseDto.getId();
        logger.debug("/driver/delete: driverID:{}", driverID);
        var driverDto = new DriverDto();
        try {
            driverDto = driverFacade.getDriverById(driverID);
            driverFacade.deleteDriver(driverDto);
            return ResponseEntity.ok(true);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DriverDto> updateDriver(@RequestBody DriverDto driverDto) {
        logger.debug("/driver/update: {}", driverDto);
        try {
            return ResponseEntity.ok(driverFacade.updateDriver(driverDto));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
