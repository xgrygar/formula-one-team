package cz.muni.f1.rest.controllers;


import cz.muni.f1.dto.Car.CarDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.dto.Team.TeamCarDto;
import cz.muni.f1.dto.Team.TeamDriverDto;
import cz.muni.f1.dto.Team.TeamDto;
import cz.muni.f1.facade.TeamFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@RestController
@RequestMapping("/team")
public class TeamController {

    private static final Logger logger = LoggerFactory.getLogger(TeamController.class);

    @Inject
    private TeamFacade teamFacade;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TeamDto>> findAllTeams() {
        logger.debug("/team/");
        return ResponseEntity.ok(teamFacade.findAllTeams());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> getTeamById(@PathVariable Long id) {
        logger.debug("/team/{}", id);
        TeamDto response;
        try {
            response = teamFacade.getTeamById(id);
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @PostMapping(value = "/driver/add", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DriverDto> addDriver(@RequestBody TeamDriverDto teamDriverDto) {
        Long teamID = teamDriverDto.getTeamID();
        Long driverID = teamDriverDto.getDriverID();

        logger.debug("/team/driver/add - teamID:{}, driverID:{}", teamID, driverID);
        try {
            return ResponseEntity.ok(teamFacade.addDriver(teamID, driverID));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Unused in FE
    @PostMapping(value = "/driver/remove", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> removeDriver(@RequestBody TeamDriverDto teamDriverDto) {
        Long teamID = teamDriverDto.getTeamID();
        Long driverID = teamDriverDto.getDriverID();

        logger.debug("/team/driver/remove - teamID:{}, driverID:{}", teamID, driverID);
        try {
            teamFacade.removeDriver(teamID, driverID);
            return ResponseEntity.ok(true);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Unused in FE
    @PostMapping(value = "/car/add", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> addCar(@RequestBody TeamCarDto teamCarDto) {
        Long teamID = teamCarDto.getTeamId();
        Long carID = teamCarDto.getCarId();

        logger.debug("/team/car/add - teamID:{}, carID:{}", teamID, carID);
        try {
            return ResponseEntity.ok(teamFacade.addCar(teamID, carID));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Unused in FE
    @PostMapping(value = "/car/remove", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> removeCar(@RequestBody TeamCarDto teamCarDto) {
        Long teamID = teamCarDto.getTeamId();
        Long carID = teamCarDto.getCarId();

        logger.debug("/team/car/remove - teamID:{}, carID:{}", teamID, carID);
        try {
            return ResponseEntity.ok(teamFacade.removeCar(teamID, carID));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}