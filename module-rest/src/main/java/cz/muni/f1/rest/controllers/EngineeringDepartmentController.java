package cz.muni.f1.rest.controllers;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.EngineeringDepartment.EngineeringDepartmentDto;
import cz.muni.f1.facade.EngineeringDepartmentFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@RestController
@RequestMapping("/engDept")
public class EngineeringDepartmentController {

    private static final Logger logger = LoggerFactory.getLogger(EngineeringDepartmentController.class);

    @Inject
    private EngineeringDepartmentFacade engineeringDepartmentFacade;

    // Unused in FE
    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EngineeringDepartmentDto>> findAllEngineeringDepartments() {
        logger.debug("/engDept/");
        return ResponseEntity.ok(engineeringDepartmentFacade.findAllEngineeringDepartments());
    }

    // Unused in FE
    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<EngineeringDepartmentDto> findEngineeringDepartmentById(@PathVariable Long id) {
        logger.debug("/engDept/{}", id);
        return ResponseEntity.ok(engineeringDepartmentFacade.getEngineeringDepartmentById(id));
    }

    // Unused in FE
    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<EngineeringDepartmentDto> createEngineeringDepartment(@RequestBody EngineeringDepartmentDto engineeringDepartmentDto) {
        logger.debug("/engDept/create");
        return ResponseEntity.ok(engineeringDepartmentFacade.createEngineeringDepartment(engineeringDepartmentDto));
    }

    // Unused in FE
    @PostMapping(value = "/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<EngineeringDepartmentDto> updateEngineeringDepartment(@RequestBody EngineeringDepartmentDto engineeringDepartmentDto) {
        logger.debug("/engDept/update");
        return ResponseEntity.ok(engineeringDepartmentFacade.updateEngineeringDepartment(engineeringDepartmentDto));

    }

    // Unused in FE
    @PostMapping(value = "/delete", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteEngineeringDepartment(@RequestBody BaseDto idDto) {
        Long id = idDto.getId();
        logger.debug("/engDept/delete - id:{}", id);
        return ResponseEntity.ok(engineeringDepartmentFacade.deleteEngineeringDepartment(id));
    }
}
