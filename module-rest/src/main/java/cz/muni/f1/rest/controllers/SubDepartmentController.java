package cz.muni.f1.rest.controllers;


import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentCreateDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentDto;
import cz.muni.f1.facade.SubDepartmentFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@RestController
@RequestMapping("/subDept")
public class SubDepartmentController {

    private static final Logger logger = LoggerFactory.getLogger(SubDepartmentController.class);

    @Inject
    private SubDepartmentFacade subDepartmentFacade;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SubDepartmentDto>> findAllSubDepartments() {
        logger.debug("/subDept/");
        return ResponseEntity.ok(subDepartmentFacade.findAllSubDepartments());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<SubDepartmentDto> findSubDepartmentById(@PathVariable Long id) {
        logger.debug("/subDept/{}", id);
        return ResponseEntity.ok(subDepartmentFacade.getSubDepartmentById(id));
    }

    // Unused in FE
    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<SubDepartmentDto> createSubDepartment(@RequestBody SubDepartmentCreateDto subDepartmentDto) {
        logger.debug("/subDept/create");
        return ResponseEntity.ok(subDepartmentFacade.createSubDepartment(subDepartmentDto));
    }

    // Unused in FE
    @PostMapping(value = "/delete", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteSubDepartment(@RequestBody BaseDto idDto) {
        Long id = idDto.getId();
        logger.debug("/subDept/delete - id:{}", id);
        return ResponseEntity.ok(subDepartmentFacade.deleteSubDepartment(id));
    }

    @PostMapping(value = "/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<SubDepartmentDto> updateSubDepartment(@RequestBody SubDepartmentDto subDepartmentDto) {
        logger.debug("/subDept/update - id:{}", subDepartmentDto.getId());
        return ResponseEntity.ok(subDepartmentFacade.updateSubDepartment(subDepartmentDto));
    }

}
