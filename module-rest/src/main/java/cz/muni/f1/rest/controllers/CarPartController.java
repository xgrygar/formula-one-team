package cz.muni.f1.rest.controllers;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.CarPart.CarPartCreateDto;
import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.facade.CarPartFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
* @author: Adam Grygar
*/

@RestController
@RequestMapping("/carPart")
public class CarPartController {

    private static final Logger logger = LoggerFactory.getLogger(CarPartController.class);

    @Inject
    private CarPartFacade carPartFacade;


    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllCarParts() {
        logger.debug("/carPart/");
        return ResponseEntity.ok(carPartFacade.findAllCarParts());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarPartDto> getCarPartById(@PathVariable Long id) {
        logger.debug("/carPart/{}", id);
        CarPartDto response;
        try {
            response = carPartFacade.getCarPartById(id);
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/free", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllFreeCarparts() {
        logger.debug("/carPart/free");
        try {
            return ResponseEntity.ok(carPartFacade.getAllFreeCarParts());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
    }

    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarPartDto> createCarPart(@RequestBody CarPartCreateDto carPartDto) {
        logger.debug("/carPart/create - carPart: {}",  carPartDto);
        CarPartDto response;
        try{
            response = carPartFacade.createRest(carPartDto);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @PostMapping(value = "/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarPartDto> updateCarPart(@RequestBody CarPartDto carPartDto) {

        logger.debug("/carPart/update - carPart: {}",  carPartDto);
        try{
            carPartFacade.update(carPartDto);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(carPartDto);
    }

    // Unused in FE
    @PostMapping(value = "/delete", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> deleteCarPart(@RequestBody BaseDto carPartID) {

        logger.debug("/carPart/delete - carPartID: {}",  carPartID.getId());
        try{
            carPartFacade.remove(carPartID.getId());
            return ResponseEntity.ok(carPartID.getId());
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    // Unused in FE
    @GetMapping(value = "/rearWing/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllRearWings() {
        logger.debug("/carPart/rearWing/");
        List<CarPartDto> response;
        try {
            response = carPartFacade.findAllCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.REAR_WING)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/frontWing/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllFrontWings() {
        logger.debug("/carPart/frontWing/");
        List<CarPartDto> response;
        try {
            response = carPartFacade.findAllCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.FRONT_WING)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/suspension/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllSuspensions() {
        logger.debug("/carPart/suspensions/");
        List<CarPartDto> response;
        try {
            response = carPartFacade.findAllCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.SUSPENSION)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/engine/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllEngines() {
        logger.debug("/carPart/engine/");
        List<CarPartDto> response;
        try {
            response = carPartFacade.findAllCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.ENGINE)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/rearWing/free", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllFreeRearWings() {
        logger.debug("/carPart/rearWing/free");
        List<CarPartDto> response;
        try {
            response = carPartFacade.getAllFreeCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.REAR_WING)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/frontWing/free", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllFreeFrontWings() {
        logger.debug("/carPart/frontWing/free");
        List<CarPartDto> response;
        try {
            response = carPartFacade.getAllFreeCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.FRONT_WING)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/suspension/free", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAllFreeSuspensions() {
        logger.debug("/carPart/suspension/free");
        List<CarPartDto> response;
        try {
            response = carPartFacade.getAllFreeCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.SUSPENSION)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    // Unused in FE
    @GetMapping(value = "/engine/free", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarPartDto>> findAlFreeEngines() {
        logger.debug("/carPart/engine/free");
        List<CarPartDto> response;
        try {
            response = carPartFacade.getAllFreeCarParts();
            response = response.stream().filter(x-> x.getPartType().equals(PartType.ENGINE)).collect(Collectors.toList());
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

}
