package cz.muni.f1.rest.controllers;


import cz.muni.f1.dto.Car.CarCarPartDto;
import cz.muni.f1.dto.Car.CarDto;
import cz.muni.f1.facade.CarFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Car
 */
@RestController
@RequestMapping("/car")
public class CarController {

    private static final Logger logger = LoggerFactory.getLogger(CarController.class);

    @Inject
    private CarFacade carFacade;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CarDto>> findAllCars() {
        logger.debug("/car/");
        return ResponseEntity.ok(carFacade.findAllCars());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> getCarById(@PathVariable Long id) {
        logger.debug("/car/{}", id);
        CarDto response;
        try {
            response = carFacade.getCarById(id);
        } catch (Exception e) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> updateCar(@RequestBody CarDto carDto) {
        logger.debug("/car/update: {}", carDto);
        try {
            carFacade.updateCar(carDto);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(carDto);
    }

    // Unused in FE
    @PostMapping(value = "/engine/assign", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> assignEngine(@RequestBody CarCarPartDto carCarPartDto) {
        Long carID = carCarPartDto.getCarId();
        Long engineID = carCarPartDto.getCarPartId();

        logger.debug("/car/engine/assign - carID:{}, engineID:{}", carID, engineID);
        try {
            List<CarDto> cars = carFacade.findAllCars();
            for (var car : cars) {
                if (Objects.equals(car.getEngine().getId(), engineID)) {
                    return ResponseEntity.ok(null);
                }
            }
            carFacade.changeEngine(engineID, carID);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(carFacade.getCarById(carID));
    }

    // Unused in FE
    @PostMapping(value = "/frontWing/assign", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> assignFrontWing(@RequestBody CarCarPartDto carCarPartDto) {
        Long carID = carCarPartDto.getCarId();
        Long wingID = carCarPartDto.getCarPartId();

        logger.debug("/car/frontWing/assign - carID:{}, wingID:{}", carID, wingID);
        try {
            List<CarDto> cars = carFacade.findAllCars();
            for (var car : cars) {
                if (Objects.equals(car.getFrontWing().getId(), wingID)) {
                    return ResponseEntity.ok(null);
                }
            }
            carFacade.changeFrontWing(wingID, carID);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(carFacade.getCarById(carID));
    }

    // Unused in FE
    @PostMapping(value = "/rearWing/assign", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> assignRearWing(@RequestBody CarCarPartDto carCarPartDto) {
        Long carID = carCarPartDto.getCarId();
        Long wingID = carCarPartDto.getCarPartId();

        logger.debug("/car/rearWing/assign - carID:{}, wingID:{}", carID, wingID);
        try {
            List<CarDto> cars = carFacade.findAllCars();
            for (var car : cars) {
                if (Objects.equals(car.getRearWing().getId(), wingID)) {
                    return ResponseEntity.ok(null);
                }
            }
            carFacade.changeRearWing(wingID, carID);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(carFacade.getCarById(carID));
    }

    // Unused in FE
    @PostMapping(value = "/suspension/assign", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarDto> assignSuspension(@RequestBody CarCarPartDto carCarPartDto) {
        Long carID = carCarPartDto.getCarId();
        Long suspensionID = carCarPartDto.getCarPartId();

        logger.debug("/car/suspension/assign - carID:{}, suspensionID:{}", carID, suspensionID);
        try {
            List<CarDto> cars = carFacade.findAllCars();
            for (var car : cars) {
                if (Objects.equals(car.getSuspension().getId(), suspensionID)) {
                    return ResponseEntity.ok(null);
                }
            }
            carFacade.changeSuspension(suspensionID, carID);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(carFacade.getCarById(carID));
    }

}
