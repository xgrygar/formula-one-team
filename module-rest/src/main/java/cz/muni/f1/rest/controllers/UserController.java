package cz.muni.f1.rest.controllers;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.User.UserCreateDto;
import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.dto.User.UserLoginDto;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.facade.CarFacade;
import cz.muni.f1.facade.DriverFacade;
import cz.muni.f1.facade.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * User controller for REST API
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project formula-one-team
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Inject
    private UserFacade userFacade;

    @Inject
    private DriverFacade driverFacade;

    @Inject
    private CarFacade carFacade;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> findAllUsers() {
        logger.debug("/user/");
        return ResponseEntity.ok(userFacade.findAllUsers());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> findUserById(@PathVariable Long id) {
        logger.debug("/user/{}", id);
        return ResponseEntity.ok(userFacade.getUserById(id));
    }

    @GetMapping(value = "/manager/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> findAllManagers() {
        logger.debug("/user/manager/");
        return ResponseEntity.ok(userFacade.findAllManagers());
    }

    @GetMapping(value = "/engineer/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> findAllEngineers() {
        logger.debug("/user/engineer/");
        return ResponseEntity.ok(userFacade.findAllEngineers());
    }

    @PostMapping(value = "/login", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> login(@RequestBody UserLoginDto userLoginDto) {

        logger.debug("/user/login - username:{}", userLoginDto.getUsername());
        var user = userFacade.logIn(userLoginDto.getUsername(), userLoginDto.getPassword());
        if (user != null) {
            // DO NOT return password
            user.setPassword(null);
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping(value = "/engineer/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> createEngineer(@RequestBody UserCreateDto userDto) {
        if (!(userDto.getRole().equals(UserType.ENGINEER))) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        logger.debug("/user/engineer/create: {}", userDto);
        try {
            return ResponseEntity.ok(userFacade.createUserRest(userDto));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/engineer/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateEngineer(@RequestBody UserDto userDto) {
        logger.debug("/user/engineer/update: {}", userDto);
        if (!(userDto.getRole().equals(UserType.ENGINEER))) {
            return ResponseEntity.notFound().build();
        }
        try {
            return ResponseEntity.ok(userFacade.updateUser(userDto));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/engineer/delete", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteEngineer(@RequestBody BaseDto userID) {
        Long engineerID = userID.getId();
        logger.debug("/user/engineer/delete: engineerID:{}", engineerID);
        try {
            UserDto userDto = userFacade.getUserById(userID.getId());
            if (!(userDto.getRole().equals(UserType.ENGINEER))) {
                return ResponseEntity.badRequest().build();
            }
            userFacade.deleteEngineer(userDto);
            return ResponseEntity.ok(true);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

}
