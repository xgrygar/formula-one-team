import { Box, Button, CircularProgress, List, ListItemButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getEngineers } from '../api/engineerApi';
import { getSubDepartment } from '../api/subDepartmentApi';
import CarPartList from '../components/CarPartsList';
import { ChangeEngineers } from '../components/ChangeEngineers';
import PaperWrapper from '../components/PaperWrapper';
import { SubDepartmentDto } from '../model/SubDepartment';
import { UserDto } from '../model/User';
import { addSpaces } from '../utils/stringResolver';
import { getCarPartTypesForSubDepartment } from '../utils/typesConstants';

export const SubDepartment = () => {
    const params = useParams();
    const departmentId = params.id;
    const navigate = useNavigate();

    const [subDepartment, setSubDepartment] = useState<SubDepartmentDto | undefined>(undefined);
    const [engineers, setEngineers] = useState<UserDto[] | undefined>(undefined);

    const getData = useCallback(async () => {
        if (departmentId) {
            setSubDepartment(await getSubDepartment(parseInt(departmentId)));
            setEngineers(await getEngineers());
        }
    }, [departmentId])

    useEffect(() => {
        (async () => {
            await getData();
        })()
    }, [getData])

    if (!subDepartment || !engineers) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h4">
                {addSpaces(subDepartment.departmentType)}
            </Typography>
            {getCarPartTypesForSubDepartment(subDepartment)
                .map((type) => <Box key={type}>
                    <Button href={`/app/createCarPart/${type}`}>Create {type}</Button>
                </Box>)}
            <CarPartList subDepartment={subDepartment} />
            <Typography variant="h4">
                Engineers:
            </Typography>
            <List>
                {subDepartment.engineers.map(engineer => (<ListItemButton onClick={() => { navigate(`/app/engineer/${engineer.id}`) }} key={engineer.id}>
                    {`${engineer.id}: ${engineer.name} ${engineer.surname}`}
                </ListItemButton>))}
            </List>
            <ChangeEngineers reload={getData} subDepartment={subDepartment} allEngineers={engineers} />
        </PaperWrapper>
    );
} 