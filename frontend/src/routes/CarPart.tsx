import { Box, Button, CircularProgress } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { JsonToTable } from 'react-json-to-table';
import { useNavigate, useParams } from 'react-router-dom';
import { deleteCarPart, getCarPart } from '../api/carPartApi';
import PaperWrapper from '../components/PaperWrapper';
import { useAuth } from '../hooks/AuthContext';
import { CarPartDto } from '../model/CarPart';
import { MANAGER } from '../utils/typesConstants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Author: Adam Grygar
export const CarPart = () => {
    const params = useParams();
    const carPartId = params.id;
    const auth = useAuth();
    const navigate = useNavigate();

    const [carPart, setCarPart] = useState<CarPartDto | undefined>(undefined);

    useEffect(() => {
        (async () => {
            if (carPartId) {
                setCarPart(await getCarPart(parseInt(carPartId)));
            }
        })()
    }, [carPartId])

    if (!carPart) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h4">
                {carPart.partName}
            </Typography>
            {auth.user!.role === MANAGER &&
                (<Button sx={{ color: 'red' }} onClick={async () => {
                    try {
                        await deleteCarPart(carPart);
                        navigate(`/app/team`);
                    } catch (err) {
                        toast(`Cannot delete ${carPart.partName} because it's on a car.`);
                    }
                }}>
                    Delete
                </Button>)}
            <Typography>
                {carPart.partType}
            </Typography>
            <Box sx={{ m: 2 }}>
                <JsonToTable json={JSON.parse(carPart.partInfo === "" || !carPart.partInfo ? "{}" : carPart.partInfo)} />
            </Box>
            <ToastContainer />
        </PaperWrapper>
    );
}
