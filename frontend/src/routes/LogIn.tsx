import { Box, Button, Paper, Typography } from '@mui/material';
import * as React from 'react';
import { useState } from 'react';
import { Form } from 'react-final-form';
import { useNavigate } from 'react-router-dom';
import TextInput from '../components/inputs/TextInput';
import { useAuth } from '../hooks/AuthContext';
import { UserLoginDto } from '../model/User';

export const LogIn = () => {
    const navigate = useNavigate();
    const auth = useAuth();
    const [error, setError] = useState(false);

    const onSubmit = async (values: UserLoginDto) => {
        if (values.username !== '' && values.password !== '') {
            const res = await auth.signin({ username: values.username, password: values.password }, () => {
                navigate('/app/home', { replace: true });
            })
            setError(!res);
        }
    }

    return (
        <Box sx={{ marginTop: 10 }}>
            <Form
                onSubmit={onSubmit}
                render={({ handleSubmit }) => (
                    <form onSubmit={handleSubmit}>
                        <Paper sx={{ padding: 4, display: 'inline-grid' }}>
                            <Typography variant="h4" >Log in</Typography>
                            <TextInput sx={{ m: 2 }} id="username" label="Username" required />
                            <TextInput sx={{ m: 2 }} id="password" label="Password" required type="password" />
                            {error && <Typography sx={{ color: 'red' }}>Bad login info</Typography>}
                            <Button sx={{ m: 2 }} type="submit">LogIn</Button>
                        </Paper>
                    </form>
                )}
            />
        </Box>
    );
}