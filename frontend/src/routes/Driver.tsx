import { Button, CircularProgress } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { deleteDriver, getDriver } from '../api/driversApi';
import PaperWrapper from '../components/PaperWrapper';
import TextWithLabel from '../components/TextWithLabel';
import { DriverDto } from '../model/Driver';

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export const Driver = () => {
    const params = useParams();
    const navigate = useNavigate();
    const driverId = params.id;
    const [driver, setDriver] = useState<DriverDto | undefined>(undefined);

    useEffect(() => {
        (async () => {
            if (driverId) {
                setDriver(await getDriver(parseInt(driverId)));
            }
        })()
    }, [driverId])

    if (!driver) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h4">
                {`${driver.name} ${driver.surname}`}
            </Typography>
            <PaperWrapper>
                <Button onClick={() => { navigate(`/app/editDriver/${driver.id}`) }}>
                    Edit
                </Button>
                <Button sx={{ color: 'red' }} onClick={async () => {
                    await deleteDriver(driver);
                    navigate(`/app/team`);
                }}>
                    Delete
                </Button>
            </PaperWrapper>
            <TextWithLabel label="Nationality:">
                {driver.nationality}
            </TextWithLabel>
            <TextWithLabel label="Agrisivness:">
                {driver.agresiveness}
            </TextWithLabel>
            <TextWithLabel label="Qualification pace:">
                {driver.qualificationPace}
            </TextWithLabel>
            <TextWithLabel label="Race pace:">
                {driver.racePace}
            </TextWithLabel>
            <TextWithLabel label="Wet performance:">
                {driver.wetPerformance}
            </TextWithLabel>
            <TextWithLabel label="Race performance:">
                {driver.racePerformance}
            </TextWithLabel>
        </PaperWrapper>
    );
}