import { CircularProgress, ListItemButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getSubDepartments } from '../api/subDepartmentApi';
import PaperWrapper from '../components/PaperWrapper';
import { useAuth } from '../hooks/AuthContext';
import { SubDepartmentDto } from '../model/SubDepartment';
import { addSpaces } from '../utils/stringResolver';
import { MANAGER } from '../utils/typesConstants';

export const Home = () => {
    const user = useAuth().user!;
    const navigate = useNavigate();

    const [subDepartments, setSubDepartments] = useState<SubDepartmentDto[] | undefined>(undefined);

    useEffect(() => {
        (async () => {

            setSubDepartments(await getSubDepartments());
        })()
    }, [])

    if (!subDepartments) {
        return <CircularProgress />
    }



    return (
        <>
            <Typography sx={{ m: 2 }} variant="h4">
                HOME
            </Typography>

            <PaperWrapper>
                {user.role === MANAGER && (<ListItemButton onClick={() => navigate('/app/team')}>
                    TEAM DETAIL
                </ListItemButton>)}
                {subDepartments.filter(subDepartment => user.role === MANAGER
                    || subDepartment.engineers.map(eng => eng.id).includes(user.id))
                    .map(subDepartment => (
                        <ListItemButton
                            onClick={() => navigate(`/app/department/${subDepartment.id}`)}
                            key={subDepartment.id}>
                            {addSpaces(subDepartment.departmentType)}
                        </ListItemButton>))}
            </PaperWrapper>
        </>
    );
}