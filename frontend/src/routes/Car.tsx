import { Button, CircularProgress, Link } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getCar } from '../api/carApi';
import PaperWrapper from '../components/PaperWrapper';
import TextWithLabel from '../components/TextWithLabel';
import { CarDto } from '../model/Car';

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export const Car = () => {
    const params = useParams();
    const navigate = useNavigate();
    const carId = params.id;
    const [car, setCar] = useState<CarDto | undefined>(undefined);

    useEffect(() => {
        (async () => {
            if (carId) {
                setCar(await getCar(parseInt(carId)));
            }
        })()
    }, [carId])

    if (!car) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h4">
                {`Car: ${car.id}`}
            </Typography>
            <PaperWrapper>
                <Button onClick={() => {
                    navigate(`/app/editCar/${car.id}`)
                }}>
                    Edit
                </Button>
            </PaperWrapper>
            <TextWithLabel label="Driver:">
                <Link href={car.driver && `/app/driver/${car.driver?.id}`}>{car.driver ? `${car.driver?.name} ${car.driver?.surname}` : 'No driver'}</Link>
            </TextWithLabel>
            <TextWithLabel label="Engine:">
                <Link href={car.engine && `/app/carPart/${car.engine?.id}`}>{car.engine ? car.engine?.partName : 'No engine'}</Link>
            </TextWithLabel>
            <TextWithLabel label="Front wing:">
                <Link href={car.frontWing && `/app/carPart/${car.frontWing?.id}`}>{car.frontWing ? car.frontWing?.partName : 'No frontWing'}</Link>
            </TextWithLabel>
            <TextWithLabel label="Rear wing:">
                <Link href={car.rearWing && `/app/carPart/${car.rearWing?.id}`}>{car.rearWing ? car.rearWing?.partName : 'No rearWing'}</Link>
            </TextWithLabel>
            <TextWithLabel label="Suspension:">
                <Link href={car.suspension && `/app/carPart/${car.suspension?.id}`}>{car.suspension ? car.suspension?.partName : 'No suspension'}</Link>
            </TextWithLabel>
        </PaperWrapper>
    );
}