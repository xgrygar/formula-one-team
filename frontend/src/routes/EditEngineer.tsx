import { Button, CircularProgress, Paper, Typography } from '@mui/material';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { Form } from 'react-final-form';
import { useNavigate, useParams } from 'react-router-dom';
import { createEngineer, getEngineer, updateEngineer } from '../api/engineerApi';
import TextInput from '../components/inputs/TextInput';
import { UserCreateDto, UserDto } from '../model/User';
import { ENGINEER } from '../utils/typesConstants';

const createBaseEngineer = () => { return { name: '', surname: '', nationality: '', username: '', password: '', role: ENGINEER } as UserCreateDto; };

type Props = {
    create?: boolean;
}

export const EditEngineer = ({ create }: Props) => {
    const params = useParams();
    const engineerId = params.id;
    const navigate = useNavigate();
    const [engineer, setEngineer] = useState<UserDto | UserCreateDto | undefined>(!create ? undefined : createBaseEngineer())

    const onSubmit = async (values: UserDto | UserCreateDto) => {
        if (create) {

            const createdDriver = await createEngineer(values as UserCreateDto);

            navigate(`/app/engineer/${createdDriver.id}`);
        } else {
            await updateEngineer(values as UserDto);
            navigate(`/app/engineer/${(values as UserDto).id!}`);
        }
    }

    useEffect(() => {
        (async () => {
            if (!create) {
                setEngineer(await getEngineer(parseInt(engineerId!)));
            }
        })()
    }, [create, engineerId])

    if (!engineer) {
        return <CircularProgress />
    }

    return (
        <Form
            initialValues={engineer}
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Paper sx={{ m: 2, padding: 4, display: 'inline-grid' }}>
                        <Typography variant="h4" >{create ? 'Create engineer' : 'Edit engineer'}</Typography>
                        <TextInput sx={{ m: 2 }} id="name" label="Name" required />
                        <TextInput sx={{ m: 2 }} id="surname" label="Surname" required />
                        <TextInput sx={{ m: 2 }} id="nationality" label="Nationality" required />
                        {create && (<>
                            <TextInput sx={{ m: 2 }} id="username" label="Username" required />
                            <TextInput sx={{ m: 2 }} id="password" label="Password" type="password" required />
                        </>
                        )}
                        <Button sx={{ m: 2 }} type="submit">{create ? 'Create' : 'Update'}</Button>
                    </Paper>
                </form>
            )}
        />
    );
}
