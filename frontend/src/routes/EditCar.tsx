import { Button, CircularProgress, Paper, Typography } from '@mui/material';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { Form } from 'react-final-form';
import { useNavigate, useParams } from 'react-router-dom';
import { getCar, updateCar } from '../api/carApi';
import { getAvailableCarParts } from '../api/carPartApi';
import { getDrivers } from '../api/driversApi';
import SelectInput from '../components/inputs/SelectInput';
import { CarDto } from '../model/Car';
import { CarPartDto } from '../model/CarPart';
import { DriverDto } from '../model/Driver';
import { ENGINE, FRONT_WING, REAR_WING, SUSPENSION } from '../utils/typesConstants';

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export const EditCar = () => {
    const params = useParams();
    const carId = params.id;
    const navigate = useNavigate();
    const [car, setCar] = useState<CarDto | undefined>(undefined);
    const [availCarParts, setAvailCarParts] = useState<CarPartDto[] | undefined>(undefined);
    const [drivers, setDrivers] = useState<DriverDto[] | undefined>(undefined);
    const [error, setError] = useState("");

    const onSubmit = async (values: CarDto) => {

        try {
            await updateCar(values as CarDto);
            navigate(`/app/car/${(values as CarDto).id!}`);
        } catch (err) {
            setError("Cannot assign this driver. Already is assigned to the other car.");
        }
    }

    useEffect(() => {
        (async () => {
            setCar(await getCar(parseInt(carId!)));
            setDrivers(await getDrivers());
            setAvailCarParts(await getAvailableCarParts());
        })()
    }, [carId])

    if (!car || !availCarParts || !drivers) {
        return <CircularProgress />
    }

    return (
        <Form
            initialValues={car}
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Paper sx={{ m: 2, padding: 4, display: 'inline-grid' }}>
                        <Typography variant="h4" >Edit car {car.id}</Typography>
                        <SelectInput sx={{ m: 2 }} getId={(d) => d.id} id="driver" options={drivers.filter(driver => driver.id !== car.driver?.id)} defaultOption={car.driver} getText={(o) => `${o.name} ${o.surname}`} />
                        <Typography sx={{ color: 'red' }}>{error}</Typography>
                        <SelectInput sx={{ m: 2 }} getId={(d) => d.id} id="engine" options={availCarParts.filter(o => o.partType === ENGINE)} defaultOption={car.engine} getText={(o) => o.partName} />
                        <SelectInput sx={{ m: 2 }} getId={(d) => d.id} id="suspension" options={availCarParts.filter(o => o.partType === SUSPENSION)} defaultOption={car.suspension} getText={(o) => o.partName} />
                        <SelectInput sx={{ m: 2 }} getId={(d) => d.id} id="frontWing" options={availCarParts.filter(o => o.partType === FRONT_WING)} defaultOption={car.frontWing} getText={(o) => o.partName} />
                        <SelectInput sx={{ m: 2 }} getId={(d) => d.id} id="rearWing" options={availCarParts.filter(o => o.partType === REAR_WING)} defaultOption={car.rearWing} getText={(o) => o.partName} />
                        <Button sx={{ m: 2 }} type="submit">{'Update'}</Button>
                    </Paper >
                </form >
            )}
        />
    );
}