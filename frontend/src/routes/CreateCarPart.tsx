import { Button, Paper, Typography } from '@mui/material';
import * as React from 'react';
import { useState } from 'react';
import { Form } from 'react-final-form';
import { useNavigate, useParams } from 'react-router-dom';
import { createCarPart } from '../api/carPartApi';
import SelectInput from '../components/inputs/SelectInput';
import TextInput from '../components/inputs/TextInput';
import { useAuth } from '../hooks/AuthContext';
import { CarPartCreateDto } from '../model/CarPart';
import { ENGINE, FRONT_WING, MANAGER, REAR_WING, SUSPENSION } from '../utils/typesConstants';

// Author: Adam Grygar
export const CreateCarPart = () => {
    const params = useParams();
    const partType = params.type;
    const auth = useAuth();
    const navigate = useNavigate();
    const [error, setError] = useState("");

    const onSubmit = async (values: CarPartCreateDto) => {

        try {
            JSON.parse(values.partInfo === "" || !values.partInfo ? "{}" : values.partInfo);
            setError("");
            const createdCarPart = await createCarPart(values);
            navigate(`/app/carPart/${createdCarPart.id}`);
        } catch (e) {
            setError("This not correct json");
        }
    }

    if (!partType) {
        return <>type is undefined</>
    }

    if (![FRONT_WING, REAR_WING, SUSPENSION, ENGINE].includes(partType)) {
        return <>BAD CAR PART TYPE</>
    }

    return (
        <Form
            initialValues={{
                partName: '', partType: partType, partInfo: ''
            } as CarPartCreateDto}
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Paper sx={{ m: 2, minWidth: '80%', padding: 4, display: 'inline-grid' }}>
                        <Typography variant="h4" >Create car part</Typography>
                        <TextInput sx={{ m: 2 }} id="partName" label="Part name" required />
                        <SelectInput sx={{ m: 2 }} id="partType" disabledOptions={auth.user!.role! !== MANAGER ? [FRONT_WING, REAR_WING, SUSPENSION, ENGINE].filter(o => o !== partType) : []} options={[FRONT_WING, REAR_WING, SUSPENSION, ENGINE].filter(o => o !== partType)} defaultOption={partType} getText={(o) => o} />
                        <TextInput sx={{ m: 2 }} id="partInfo" multiline
                            rows={10} label="Part info (JSON)"
                            placeholder='{"coolness": 1000}' />
                        {error !== "" && <Typography sx={{ color: "red" }}>{error}</Typography>}
                        <Button sx={{ m: 2 }} type="submit">{'Create'}</Button>
                    </Paper>
                </form>
            )}
        />
    );
}
