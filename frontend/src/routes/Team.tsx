import { Button, CircularProgress, List, ListItemButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getCars } from '../api/carApi';
import { getDrivers } from '../api/driversApi';
import { getEngineers } from '../api/engineerApi';
import { getTeam } from '../api/teamApi';
import PaperWrapper from '../components/PaperWrapper';
import { CarDto } from '../model/Car';
import { DriverDto } from '../model/Driver';
import { TeamDto } from '../model/Team';
import { UserDto } from '../model/User';

//Author: Roman Duris
//Project: Formula One Team

export const Team = () => {
    const [team, setTeam] = useState<TeamDto | undefined>(undefined);
    const [engineers, setEngineers] = useState<UserDto[] | undefined>(undefined);
    const [drivers, setDrivers] = useState<DriverDto[] | undefined>(undefined);
    const [cars, setCars] = useState<CarDto[] | undefined>(undefined)
    const navigate = useNavigate();

    useEffect(() => {
        (async () => {
            setDrivers(await getDrivers());
            setCars(await getCars());
            setTeam(await getTeam());
            setEngineers(await getEngineers());
        })()
    }, [])

    if (!team || !engineers || !drivers || !cars) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h1">
                Team: {team.id}
            </Typography>
            <Typography variant="h4">
                Cars:
            </Typography>
            <List>
                {cars.map(car => (<ListItemButton onClick={() => { navigate(`/app/car/${car.id}`) }} key={car.id}>
                    {`CAR ${car.id}`}
                </ListItemButton>))}
            </List>
            <Typography variant="h4">
                Drivers:
            </Typography>
            <Button onClick={() => { navigate(`/app/createDriver`) }}>Add driver</Button>
            <List>
                {drivers.map(driver => (<ListItemButton onClick={() => { navigate(`/app/driver/${driver.id}`) }} key={driver.id}>
                    {`${driver.id}: ${driver.name} ${driver.surname}`}
                </ListItemButton>))}
            </List>
            <Typography variant="h4">
                Engineers:
            </Typography>
            <Button onClick={() => { navigate(`/app/createEngineer`) }}>Add engineer</Button>
            <List>
                {engineers.map(engineer => (<ListItemButton onClick={() => { navigate(`/app/engineer/${engineer.id}`) }} key={engineer.id}>
                    {`${engineer.id}: ${engineer.name} ${engineer.surname}`}
                </ListItemButton>))}
            </List>
        </PaperWrapper>
    );
}
