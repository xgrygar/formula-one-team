import * as React from 'react';
import Typography from '@mui/material/Typography';
import { useAuth } from '../hooks/AuthContext';
import TextWithLabel from '../components/TextWithLabel';
import PaperWrapper from '../components/PaperWrapper';

export const Profile = () => {
    const authContext = useAuth();
    const user = authContext.user!;

    return (
        <PaperWrapper>
            <Typography variant="h3">
                {`${user.name} ${user.surname}`}
            </Typography>
            <TextWithLabel label="role:">
                {user.role}
            </TextWithLabel>
            <TextWithLabel label="username:">
                {user.username}
            </TextWithLabel>
            <TextWithLabel label="nationality:">
                {user.nationality}
            </TextWithLabel>
        </PaperWrapper>
    );
}