import { Button, CircularProgress } from '@mui/material';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { deleteEngineer, getEngineer } from '../api/engineerApi';
import PaperWrapper from '../components/PaperWrapper';
import TextWithLabel from '../components/TextWithLabel';
import { UserDto } from '../model/User';

export const Engineer = () => {
    const params = useParams();
    const navigate = useNavigate();
    const engineerId = params.id;
    const [engineer, setEngineer] = useState<UserDto | undefined>(undefined);

    useEffect(() => {
        (async () => {
            if (engineerId) {
                setEngineer(await getEngineer(parseInt(engineerId)));
            }
        })()
    }, [engineerId])

    if (!engineer) {
        return <CircularProgress />
    }

    return (
        <PaperWrapper>
            <Typography variant="h4">
                {`${engineer.name} ${engineer.surname}`}
            </Typography>
            <PaperWrapper>
                <Button onClick={() => { navigate(`/app/editEngineer/${engineer.id}`) }}>
                    Edit
                </Button>
                <Button sx={{ color: 'red' }} onClick={async () => {
                    await deleteEngineer(engineer);
                    navigate(`/app/home`);
                }}>
                    Delete
                </Button>
            </PaperWrapper>
            <TextWithLabel label="Nationality:">
                {engineer.nationality}
            </TextWithLabel>
            <TextWithLabel label="Role:">
                {engineer.role}
            </TextWithLabel>
        </PaperWrapper>
    );
} 