import { Button, CircularProgress, Paper, Typography } from '@mui/material';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { Form } from 'react-final-form';
import { useNavigate, useParams } from 'react-router-dom';
import { createDriver, getDriver, updateDriver } from '../api/driversApi';
import NumberInput from '../components/inputs/NumberInput';
import TextInput from '../components/inputs/TextInput';
import { DriverCreateDto, DriverDto } from '../model/Driver';

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
const createBaseDriver = () => { return { name: '', surname: '', nationality: '', agresiveness: 0, racePace: 0, qualificationPace: 0, wetPerformance: 0, racePerformance: 0 } as DriverCreateDto; };

type Props = {
    create?: boolean;
}

export const EditDriver = ({ create }: Props) => {
    const params = useParams();
    const driverId = params.id;
    const navigate = useNavigate();
    const [driver, setDriver] = useState<DriverDto | DriverCreateDto | undefined>(!create ? undefined : createBaseDriver())

    const onSubmit = async (values: DriverDto | DriverCreateDto) => {
        if (create) {
            const createdDriver = await createDriver(values as DriverCreateDto);
            navigate(`/app/driver/${createdDriver.id}`);
        } else {
            await updateDriver(values as DriverDto);
            navigate(`/app/driver/${(values as DriverDto).id!}`);
        }
    }

    useEffect(() => {
        (async () => {
            if (!create) {
                setDriver(await getDriver(parseInt(driverId!)));
            }
        })()
    }, [create, driverId])

    if (!driver) {
        return <CircularProgress />
    }

    return (
        <Form
            initialValues={driver}
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Paper sx={{ m: 2, padding: 4, display: 'inline-grid' }}>
                        <Typography variant="h4" >{create ? 'Create driver' : 'Edit driver'}</Typography>
                        <TextInput sx={{ m: 2 }} id="name" label="Name" required />
                        <TextInput sx={{ m: 2 }} id="surname" label="Surname" required />
                        <TextInput sx={{ m: 2 }} id="nationality" label="Nationality" required />
                        <NumberInput inputProps={{ min: 0, max: 10 }} sx={{ m: 2 }} id="agresiveness" label="Agresiveness" required />
                        <NumberInput inputProps={{ min: 0, max: 10 }} sx={{ m: 2 }} id="racePace" label="Race pace" required />
                        <NumberInput inputProps={{ min: 0, max: 10 }} sx={{ m: 2 }} id="qualificationPace" label="Qualification pace" required />
                        <NumberInput inputProps={{ min: 0, max: 10 }} sx={{ m: 2 }} id="wetPerformance" label="Wet performance" required />
                        <NumberInput inputProps={{ min: 0, max: 10 }} sx={{ m: 2 }} id="racePerformance" label="Race performance" required />
                        <Button sx={{ m: 2 }} type="submit">{create ? 'Create' : 'Update'}</Button>
                    </Paper>
                </form>
            )}
        />
    );
}