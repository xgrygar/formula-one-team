import { Button } from '@mui/material';
import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import { RequireAuth, RequireManager } from './hooks/AuthContext';
import { Car } from './routes/Car';
import { CarPart } from './routes/CarPart';
import { CreateCarPart } from './routes/CreateCarPart';
import { SubDepartment } from './routes/SubDepartment';
import { Driver } from './routes/Driver';
import { EditCar } from './routes/EditCar';
import { EditDriver } from './routes/EditDriver';
import { EditEngineer } from './routes/EditEngineer';
import { Engineer } from './routes/Engineer';
import { Home } from './routes/Home';
import { Layout } from './routes/Layout';
import { LogIn } from './routes/LogIn';
import ResponsiveAppBar from './routes/NavBar';
import { Profile } from './routes/Profile';
import { Team } from './routes/Team';

const App = () => {

  return (
    <div className="App">

      <ResponsiveAppBar />
      <Routes>
        <Route element={<Layout />} >
          <Route path="/" element={<Navigate to="/app/home" />} />
          <Route path="login" element={<LogIn />} />
          <Route path="app">
            <Route path="home" element={
              <RequireAuth>
                <Home />
              </RequireAuth>} />
            <Route path="car/:id" element={
              <RequireAuth>
                <Car />
              </RequireAuth>} />
            <Route path="department/:id" element={
              <RequireAuth>
                <SubDepartment />
              </RequireAuth>} />
            <Route path="driver/:id" element={
              <RequireAuth>
                <RequireManager>
                  <Driver />
                </RequireManager>
              </RequireAuth>} />
            <Route path="editDriver/:id" element={
              <RequireAuth>
                <RequireManager>
                  <EditDriver />
                </RequireManager>
              </RequireAuth>} />
            <Route path="editCar/:id" element={
              <RequireAuth>
                <RequireManager>
                  <EditCar />
                </RequireManager>
              </RequireAuth>} />
            <Route path="createDriver" element={
              <RequireAuth>
                <RequireManager>
                  <EditDriver create />
                </RequireManager>
              </RequireAuth>} />
            <Route path="createEngineer" element={
              <RequireAuth>
                <RequireManager>
                  <EditEngineer create />
                </RequireManager>
              </RequireAuth>} />
            <Route path="editEngineer/:id" element={
              <RequireAuth>
                <RequireManager>
                  <EditEngineer />
                </RequireManager>
              </RequireAuth>} />
            <Route path="engineer/:id" element={
              <RequireAuth>
                <RequireManager>
                  <Engineer />
                </RequireManager>
              </RequireAuth>} />
            <Route path="team" element={
              <RequireAuth>
                <RequireManager>
                  <Team />
                </RequireManager>
              </RequireAuth>} />
            <Route path="carPart/:id" element={
              <RequireAuth>
                <CarPart />
              </RequireAuth>} />
            <Route path="createCarPart/:type" element={
              <RequireAuth>
                <CreateCarPart />
              </RequireAuth>} />
          </Route>
          <Route path="user">
            <Route path="profile" element={
              <RequireAuth>
                <Profile />
              </RequireAuth>} />
          </Route>
        </Route>
        <Route path="*" element={<>NOT FOUND <Button href="/app/home">GO HOME</Button></>} />
      </Routes>
    </div >
  );
}

export default App;
