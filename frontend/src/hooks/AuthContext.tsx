
import * as React from "react";
import { useContext } from "react";
import {
    Navigate, useLocation
} from "react-router-dom";
import { login } from "../api/userApi";
import { UserDto, UserLoginDto } from "../model/User";
import { MANAGER } from "../utils/typesConstants";
import { useStoreState } from "./useStoreState";

export type AuthContextType = {
    user?: UserDto;
    signin: (user: UserLoginDto, callback: () => void) => Promise<boolean>;
    signout: (callback: () => void) => void;
}

const AuthContext = React.createContext<AuthContextType>(null!);

export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
    const [user, setUser] = useStoreState<UserDto | undefined>('user', undefined);

    const signin = async (newUser: UserLoginDto, callback: VoidFunction) => {
        const user = await login(newUser);
        if (!!user) {

            setUser(user);
            callback();
            return true;
        }
        return false;
    };

    const signout = (callback: VoidFunction) => {
        setUser(undefined);
        callback();
    };

    const value = { user, signin, signout };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
    return useContext(AuthContext);
}

export const RequireAuth = ({ children }: { children: JSX.Element }) => {
    const auth = useAuth();
    const location = useLocation();

    if (!auth.user) {
        // Redirect them to the /login page, but save the current location they were
        // trying to go to when they were redirected. This allows us to send them
        // along to that page after they login, which is a nicer user experience
        // than dropping them off on the home page.
        return <Navigate to="/login" state={{ from: location }} replace />;
    }

    return children;
}

export const RequireManager = ({ children }: { children: JSX.Element }) => {
    const auth = useAuth();
    const location = useLocation();

    if (auth.user!.role !== MANAGER) {
        return <Navigate to="/app/home" state={{ from: location }} replace />;
    }

    return children;
}