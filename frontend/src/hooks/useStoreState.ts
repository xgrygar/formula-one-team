import { useState, useEffect } from "react";

const storageId = "-f1team-";

export const useStoreState = <T extends unknown>(
  key: string,
  defaultValue?: T
) => {
  const storageKey = storageId + key;
  const isUndefined = localStorage.getItem(storageKey) === "undefined";

  const [value, setValue] = useState(() => {
    const stickyValue = window.localStorage.getItem(storageKey);
    return stickyValue === null || isUndefined
      ? defaultValue
      : JSON.parse(stickyValue);
  });
  useEffect(() => {
    window.localStorage.setItem(storageKey, JSON.stringify(value));
  }, [storageKey, value]);
  return [value, setValue];
};
