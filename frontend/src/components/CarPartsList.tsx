import { CircularProgress, Link, List, ListItem, Typography } from '@mui/material';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { getCarParts } from '../api/carPartApi';
import { CarPartDto } from '../model/CarPart';
import { SubDepartmentDto } from '../model/SubDepartment';
import { AERODYNAMICS_DEPARTMENT, ENGINE, ENGINE_DEPARTMENT, FRONT_WING, REAR_WING, SUSPENSION, SUSPENSION_DEPARTMENT } from '../utils/typesConstants';

type Props = {
    subDepartment: SubDepartmentDto;
}

const CarPartList = ({ subDepartment }: Props) => {

    const [carParts, setCarParts] = useState<CarPartDto[] | undefined>(undefined);

    useEffect(() => {
        (async () => {
            let carParts = await getCarParts();
            if (subDepartment.departmentType === ENGINE_DEPARTMENT) {
                carParts = carParts.filter(part => part.partType === ENGINE);
            }
            if (subDepartment.departmentType === SUSPENSION_DEPARTMENT) {
                carParts = carParts.filter(part => part.partType === SUSPENSION);
            }
            if (subDepartment.departmentType === AERODYNAMICS_DEPARTMENT) {
                carParts = carParts.filter(part => part.partType === FRONT_WING || part.partType === REAR_WING);
            }
            setCarParts(carParts);
        })()
    }, [subDepartment.departmentType])

    if (!carParts) {
        return <CircularProgress />
    }

    return (
        <>
            <Typography>Car parts:</Typography>
            <List>
                {carParts.filter(part => part.partType)
                    .map(carPart => (<ListItem key={carPart.id}>
                        <Link href={`/app/carPart/${carPart.id}`}>{carPart.partName}</Link>
                    </ListItem>))}
            </List>
        </>
    );
}

export default CarPartList;