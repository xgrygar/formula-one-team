import { Grid, Paper } from '@mui/material';
import * as React from 'react';
import { ReactNode } from 'react';

type Props = {
    children: ReactNode;
}

const PaperWrapper = ({ children }: Props) => {
    return (
        <Grid container
            justifyContent="center"
        >
            <Paper sx={{ minWidth: '80%', textAlign: 'center', padding: 3, m: 4 }}>
                {children}
            </Paper>
        </Grid>
    );
}

export default PaperWrapper;