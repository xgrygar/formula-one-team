import { Box, Typography, TypographyProps } from '@mui/material';
import * as React from 'react';

type Props = {
    label: string;
} & TypographyProps

const TextWithLabel = ({ label, ...props }: Props) => {
    return (
        <Box sx={{ m: 2 }}>
            <Typography>
                {label}
            </Typography>
            <Typography variant="h6" {...props}>
                {props.children}
            </Typography>
        </Box>
    );
}

export default TextWithLabel;