import { TextField, TextFieldProps } from '@mui/material';
import * as React from 'react';
import { useField } from 'react-final-form';

type Props = {
    id: string;
} & Omit<TextFieldProps, 'id'>

const NumberInput = (props: Props) => {
    const { input } = useField<number>(props.id, {
        subscription: { value: true, error: true, touched: true },
        parse: v => v
    });

    return (
        <TextField
            {...input}
            {...props}
            type="number"
        />
    );
}

export default NumberInput; 