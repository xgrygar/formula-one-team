import { MenuItem, Select, SelectProps } from '@mui/material';
import * as React from 'react';
import { useField } from 'react-final-form';

type Props<T extends unknown> = {
    id: string;
    options: readonly T[];
    getText: (value: T) => string;
    defaultOption?: T;
    disabledOptions?: T[];
    getId?: (value: T) => string | number;
} & Omit<SelectProps, 'id'>

const SelectInput = <T extends unknown>({ getText, defaultOption, options, getId, disabledOptions, ...props }: Props<T>) => {
    const { input } = useField<T>(props.id, {
        subscription: { value: true, error: true, touched: true },
        parse: v => v
    });

    return (
        <Select
            {...input}
            {...props}
        >
            {!!defaultOption && <MenuItem value={defaultOption as string}>
                {!!defaultOption ? getText(defaultOption) : '-'}
            </MenuItem>}
            {options.filter(opt => disabledOptions ? !disabledOptions.includes(opt) : true).map(option => (<MenuItem key={getId ? getId(option) : option as string} value={option as string}>
                {getText(option)}
            </MenuItem>))}
        </Select>
    );
}

export default SelectInput;  