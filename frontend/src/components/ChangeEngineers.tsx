import { Box, Button, Typography } from '@mui/material';
import * as React from 'react';
import { useMemo } from 'react';
import { Form } from 'react-final-form';
import { updateSubDepartment } from '../api/subDepartmentApi';
import { useAuth } from '../hooks/AuthContext';
import { SubDepartmentDto } from '../model/SubDepartment';
import { UserDto } from '../model/User';
import { MANAGER } from '../utils/typesConstants';
import SelectInput from './inputs/SelectInput';

type Props = {
    subDepartment: SubDepartmentDto;
    allEngineers: UserDto[];
    reload: () => void;
}

export const ChangeEngineers = ({ subDepartment, allEngineers, reload }: Props) => {
    const auth = useAuth();

    const availableEngineers = useMemo(() => allEngineers
        .filter(eng => !subDepartment.engineers.map(e => e.id).includes(eng.id)),
        [subDepartment, allEngineers]);

    if (auth.user!.role !== MANAGER) {
        return <></>
    }


    const onSubmitAddEngineer = async (values: { engineer: UserDto }) => {
        const newEngineers = [...subDepartment.engineers, values.engineer];
        await updateSubDepartment({ ...subDepartment, engineers: newEngineers } as SubDepartmentDto);
        reload();
    }

    const onSubmitRemoveEngineer = async (values: { engineer: UserDto }) => {
        const newEngineers = subDepartment.engineers.filter(eng => eng.id !== values.engineer.id);
        await updateSubDepartment({ ...subDepartment, engineers: newEngineers } as SubDepartmentDto);
        reload();
    }

    return (
        <>
            <Box sx={{ m: 2 }}>
                <Form
                    initialValues={undefined}
                    onSubmit={onSubmitAddEngineer}
                    render={({ handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <Box sx={{ m: 2, padding: 4, display: 'inline-grid' }}>
                                <Typography variant="h4" >Add engineer</Typography>
                                <SelectInput sx={{ m: 2 }}
                                    getId={(d) => d.id}
                                    id="engineer"
                                    options={availableEngineers}
                                    getText={(o) => `${o.id}: ${o.name} ${o.surname}`}
                                    required />
                                <Button sx={{ m: 2 }} type="submit">{'Add'}</Button>
                            </Box>
                        </form>
                    )}
                />
            </Box>
            <Box sx={{ m: 2 }}>
                <Form
                    initialValues={undefined}
                    onSubmit={onSubmitRemoveEngineer}
                    render={({ handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <Box sx={{ m: 2, padding: 4, display: 'inline-grid' }}>
                                <Typography variant="h4" >Remove engineer</Typography>
                                <SelectInput sx={{ m: 2 }}
                                    getId={(d) => d.id}
                                    id="engineer"
                                    options={subDepartment.engineers}
                                    getText={(o) => `${o.id}: ${o.name} ${o.surname}`}
                                    required />
                                <Button sx={{ m: 2 }} type="submit">{'Remove'}</Button>
                            </Box>
                        </form>
                    )}
                />
            </Box>
        </>
    );
}
