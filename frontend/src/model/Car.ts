import { CarPartDto } from "./CarPart";
import { DriverDto } from "./Driver";

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export type CarDto = {
    id: number;
    driver?: DriverDto;
    engine?: CarPartDto;
    suspension?: CarPartDto;
    frontWing?: CarPartDto;
    rearWing?: CarPartDto;
};