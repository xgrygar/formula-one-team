import { CarPartDto } from "./CarPart";
import { UserDto } from "./User";

export type SubDepartmentDto = {
  id: number;
  engineers: UserDto[];
  carParts: CarPartDto[];
  departmentType: string;
};
 