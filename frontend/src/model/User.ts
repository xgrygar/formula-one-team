export type UserLoginDto = {
  username: string;
  password: string;
};

export type UserCreateDto = {
  username: string;
  name: string;
  surname: string;
  nationality: string;
  role: string;
  password: string;
};

export type UserDto = {
  username: string;
  id: number;
  name: string;
  surname: string;
  nationality: string;
  role: string;
};