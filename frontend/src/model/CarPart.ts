export type CarPartDto = {
  id: number;
  partName: string;
  partType: string;
  partInfo?: string;
};

export type CarPartCreateDto = {
  partName: string;
  partType: string;
  partInfo?: string;
};
