/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export type DriverDto = {
    id: number;
    name: string;
    surname: string;
    nationality: string;
    racePerformance: number;
    wetPerformance: number;
    agresiveness: number;
    racePace: number;
    qualificationPace: number;
};

export type DriverCreateDto = Omit<DriverDto, "id">;