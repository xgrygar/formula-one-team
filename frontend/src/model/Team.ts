import { DriverDto } from "./Driver";

//Author: Roman Duris
//Project: Formula One Team

export type TeamDto = {
    id: number;
    drivers: DriverDto[];
};
