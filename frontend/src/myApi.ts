/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface UserCreateDto {
  name?: string;
  nationality?: string;
  password?: string;
  role?: "MANAGER" | "ENGINEER";
  surname?: string;
  username?: string;
}

export interface UserDto {
  /** @format int64 */
  id?: number;
  name?: string;
  nationality?: string;
  password?: string;
  role?: "MANAGER" | "ENGINEER";
  surname?: string;
  username?: string;
}

export interface BaseDto {
  /** @format int64 */
  id?: number;
}

export interface SubDepartmentCreateDto {
  carParts?: CarPart[];
  departmentType?: "EngineDepartment" | "SuspensionDepartment" | "AeroDynamicsDepartment";
  engineers?: UserDto[];
}

export interface CarDto {
  driver?: DriverDto;
  engine?: CarPartDto;
  frontWing?: CarPartDto;

  /** @format int64 */
  id?: number;
  rearWing?: CarPartDto;
  suspension?: CarPartDto;
}

export interface SubDepartmentDto {
  carParts?: CarPart[];
  departmentType?: "EngineDepartment" | "SuspensionDepartment" | "AeroDynamicsDepartment";
  engineers?: UserDto[];

  /** @format int64 */
  id?: number;
}

export interface CarPartCreateDto {
  partInfo?: string;
  partName?: string;
  partType?: "ENGINE" | "SUSPENSION" | "REAR_WING" | "FRONT_WING";
}

export interface TeamDto {
  drivers?: DriverDto[];

  /** @format int64 */
  id?: number;
  manager?: UserDto;
}

export interface DriverCreateDto {
  /** @format int32 */
  agresiveness?: number;
  name?: string;
  nationality?: string;

  /** @format int32 */
  qualificationPace?: number;

  /** @format int32 */
  racePace?: number;

  /** @format int32 */
  racePerformance?: number;
  surname?: string;

  /** @format int32 */
  wetPerformance?: number;
}

export interface CarPart {
  /** @format int64 */
  id?: number;
  partInfo?: string;
  partName?: string;
  partType?: "ENGINE" | "SUSPENSION" | "REAR_WING" | "FRONT_WING";
}

export interface DriverDto {
  /** @format int32 */
  agresiveness?: number;

  /** @format int64 */
  id?: number;
  name?: string;
  nationality?: string;

  /** @format int32 */
  qualificationPace?: number;

  /** @format int32 */
  racePace?: number;

  /** @format int32 */
  racePerformance?: number;
  surname?: string;

  /** @format int32 */
  wetPerformance?: number;
}

export interface UserLoginDto {
  password?: string;
  username?: string;
}

export interface CarPartDto {
  /** @format int64 */
  id?: number;
  partInfo?: string;
  partName?: string;
  partType?: "ENGINE" | "SUSPENSION" | "REAR_WING" | "FRONT_WING";
}

export interface TeamDriverDto {
  /** @format int64 */
  driverID?: number;

  /** @format int64 */
  teamID?: number;
}

export interface EngineeringDepartmentDto {
  aeroDepartment?: SubDepartmentDto;
  engineDepartment?: SubDepartmentDto;
  engineers?: UserDto[];

  /** @format int64 */
  id?: number;
  suspensionDepartment?: SubDepartmentDto;
}

export interface TeamCarDto {
  /** @format int64 */
  carId?: number;

  /** @format int64 */
  teamId?: number;
}

export interface CarCarPartDto {
  /** @format int64 */
  carId?: number;

  /** @format int64 */
  carPartId?: number;
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "//localhost:8080/pa165/rest";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  private addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  private addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  private mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Api Documentation
 * @version 1.0
 * @license Apache 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * @termsOfService urn:tos
 * @baseUrl //localhost:8080/pa165/rest
 * @contact
 *
 * Api Documentation
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  car = {
    /**
     * No description
     *
     * @tags car-controller
     * @name FindAllCarsUsingGet
     * @summary findAllCars
     * @request GET:/car/
     */
    findAllCarsUsingGet: (params: RequestParams = {}) =>
      this.request<CarDto[], void>({
        path: `/car/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name AssignEngineUsingPost
     * @summary assignEngine
     * @request POST:/car/engine/assign
     */
    assignEngineUsingPost: (carCarPartDto: CarCarPartDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/engine/assign`,
        method: "POST",
        body: carCarPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name AssignFrontWingUsingPost
     * @summary assignFrontWing
     * @request POST:/car/frontWing/assign
     */
    assignFrontWingUsingPost: (carCarPartDto: CarCarPartDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/frontWing/assign`,
        method: "POST",
        body: carCarPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name AssignRearWingUsingPost
     * @summary assignRearWing
     * @request POST:/car/rearWing/assign
     */
    assignRearWingUsingPost: (carCarPartDto: CarCarPartDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/rearWing/assign`,
        method: "POST",
        body: carCarPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name AssignSuspensionUsingPost
     * @summary assignSuspension
     * @request POST:/car/suspension/assign
     */
    assignSuspensionUsingPost: (carCarPartDto: CarCarPartDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/suspension/assign`,
        method: "POST",
        body: carCarPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name UpdateCarUsingPost
     * @summary updateCar
     * @request POST:/car/update
     */
    updateCarUsingPost: (carDto: CarDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/update`,
        method: "POST",
        body: carDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-controller
     * @name GetCarByIdUsingGet
     * @summary getCarById
     * @request GET:/car/{id}
     */
    getCarByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/car/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  carPart = {
    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllCarPartsUsingGet
     * @summary findAllCarParts
     * @request GET:/carPart/
     */
    findAllCarPartsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name CreateCarPartUsingPost
     * @summary createCarPart
     * @request POST:/carPart/create
     */
    createCarPartUsingPost: (carPartDto: CarPartCreateDto, params: RequestParams = {}) =>
      this.request<CarPartDto, void>({
        path: `/carPart/create`,
        method: "POST",
        body: carPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name DeleteCarPartUsingPost
     * @summary deleteCarPart
     * @request POST:/carPart/delete
     */
    deleteCarPartUsingPost: (carPartID: BaseDto, params: RequestParams = {}) =>
      this.request<number, void>({
        path: `/carPart/delete`,
        method: "POST",
        body: carPartID,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllEnginesUsingGet
     * @summary findAllEngines
     * @request GET:/carPart/engine/
     */
    findAllEnginesUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/engine/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAlFreeEnginesUsingGet
     * @summary findAlFreeEngines
     * @request GET:/carPart/engine/free
     */
    findAlFreeEnginesUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/engine/free`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllFreeCarpartsUsingGet
     * @summary findAllFreeCarparts
     * @request GET:/carPart/free
     */
    findAllFreeCarpartsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/free`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllFrontWingsUsingGet
     * @summary findAllFrontWings
     * @request GET:/carPart/frontWing/
     */
    findAllFrontWingsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/frontWing/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllFreeFrontWingsUsingGet
     * @summary findAllFreeFrontWings
     * @request GET:/carPart/frontWing/free
     */
    findAllFreeFrontWingsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/frontWing/free`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllRearWingsUsingGet
     * @summary findAllRearWings
     * @request GET:/carPart/rearWing/
     */
    findAllRearWingsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/rearWing/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllFreeRearWingsUsingGet
     * @summary findAllFreeRearWings
     * @request GET:/carPart/rearWing/free
     */
    findAllFreeRearWingsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/rearWing/free`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllSuspensionsUsingGet
     * @summary findAllSuspensions
     * @request GET:/carPart/suspension/
     */
    findAllSuspensionsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/suspension/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name FindAllFreeSuspensionsUsingGet
     * @summary findAllFreeSuspensions
     * @request GET:/carPart/suspension/free
     */
    findAllFreeSuspensionsUsingGet: (params: RequestParams = {}) =>
      this.request<CarPartDto[], void>({
        path: `/carPart/suspension/free`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name UpdateCarPartUsingPost
     * @summary updateCarPart
     * @request POST:/carPart/update
     */
    updateCarPartUsingPost: (carPartDto: CarPartDto, params: RequestParams = {}) =>
      this.request<CarPartDto, void>({
        path: `/carPart/update`,
        method: "POST",
        body: carPartDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags car-part-controller
     * @name GetCarPartByIdUsingGet
     * @summary getCarPartById
     * @request GET:/carPart/{id}
     */
    getCarPartByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<CarPartDto, void>({
        path: `/carPart/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  driver = {
    /**
     * No description
     *
     * @tags driver-controller
     * @name FindAllDriversUsingGet
     * @summary findAllDrivers
     * @request GET:/driver/
     */
    findAllDriversUsingGet: (params: RequestParams = {}) =>
      this.request<DriverDto[], void>({
        path: `/driver/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags driver-controller
     * @name CreateDriverUsingPost
     * @summary createDriver
     * @request POST:/driver/create
     */
    createDriverUsingPost: (driverCreateDto: DriverCreateDto, params: RequestParams = {}) =>
      this.request<DriverDto, void>({
        path: `/driver/create`,
        method: "POST",
        body: driverCreateDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags driver-controller
     * @name DeleteDriverUsingPost
     * @summary deleteDriver
     * @request POST:/driver/delete
     */
    deleteDriverUsingPost: (baseDto: BaseDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/driver/delete`,
        method: "POST",
        body: baseDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags driver-controller
     * @name UpdateDriverUsingPost
     * @summary updateDriver
     * @request POST:/driver/update
     */
    updateDriverUsingPost: (driverDto: DriverDto, params: RequestParams = {}) =>
      this.request<DriverDto, void>({
        path: `/driver/update`,
        method: "POST",
        body: driverDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags driver-controller
     * @name FindDriverByIdUsingGet
     * @summary findDriverById
     * @request GET:/driver/{id}
     */
    findDriverByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<DriverDto, void>({
        path: `/driver/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  engDept = {
    /**
     * No description
     *
     * @tags engineering-department-controller
     * @name FindAllEngineeringDepartmentsUsingGet
     * @summary findAllEngineeringDepartments
     * @request GET:/engDept/
     */
    findAllEngineeringDepartmentsUsingGet: (params: RequestParams = {}) =>
      this.request<EngineeringDepartmentDto[], void>({
        path: `/engDept/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags engineering-department-controller
     * @name CreateEngineeringDepartmentUsingPost
     * @summary createEngineeringDepartment
     * @request POST:/engDept/create
     */
    createEngineeringDepartmentUsingPost: (
      engineeringDepartmentDto: EngineeringDepartmentDto,
      params: RequestParams = {},
    ) =>
      this.request<EngineeringDepartmentDto, void>({
        path: `/engDept/create`,
        method: "POST",
        body: engineeringDepartmentDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags engineering-department-controller
     * @name DeleteEngineeringDepartmentUsingPost
     * @summary deleteEngineeringDepartment
     * @request POST:/engDept/delete
     */
    deleteEngineeringDepartmentUsingPost: (idDto: BaseDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/engDept/delete`,
        method: "POST",
        body: idDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags engineering-department-controller
     * @name UpdateEngineeringDepartmentUsingPost
     * @summary updateEngineeringDepartment
     * @request POST:/engDept/update
     */
    updateEngineeringDepartmentUsingPost: (
      engineeringDepartmentDto: EngineeringDepartmentDto,
      params: RequestParams = {},
    ) =>
      this.request<EngineeringDepartmentDto, void>({
        path: `/engDept/update`,
        method: "POST",
        body: engineeringDepartmentDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags engineering-department-controller
     * @name FindEngineeringDepartmentByIdUsingGet
     * @summary findEngineeringDepartmentById
     * @request GET:/engDept/{id}
     */
    findEngineeringDepartmentByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<EngineeringDepartmentDto, void>({
        path: `/engDept/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  subDept = {
    /**
     * No description
     *
     * @tags sub-department-controller
     * @name FindAllSubDepartmentsUsingGet
     * @summary findAllSubDepartments
     * @request GET:/subDept/
     */
    findAllSubDepartmentsUsingGet: (params: RequestParams = {}) =>
      this.request<SubDepartmentDto[], void>({
        path: `/subDept/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags sub-department-controller
     * @name CreateSubDepartmentUsingPost
     * @summary createSubDepartment
     * @request POST:/subDept/create
     */
    createSubDepartmentUsingPost: (subDepartmentDto: SubDepartmentCreateDto, params: RequestParams = {}) =>
      this.request<SubDepartmentDto, void>({
        path: `/subDept/create`,
        method: "POST",
        body: subDepartmentDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags sub-department-controller
     * @name DeleteSubDepartmentUsingPost
     * @summary deleteSubDepartment
     * @request POST:/subDept/delete
     */
    deleteSubDepartmentUsingPost: (idDto: BaseDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/subDept/delete`,
        method: "POST",
        body: idDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags sub-department-controller
     * @name UpdateSubDepartmentUsingPost
     * @summary updateSubDepartment
     * @request POST:/subDept/update
     */
    updateSubDepartmentUsingPost: (subDepartmentDto: SubDepartmentDto, params: RequestParams = {}) =>
      this.request<SubDepartmentDto, void>({
        path: `/subDept/update`,
        method: "POST",
        body: subDepartmentDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags sub-department-controller
     * @name FindSubDepartmentByIdUsingGet
     * @summary findSubDepartmentById
     * @request GET:/subDept/{id}
     */
    findSubDepartmentByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<SubDepartmentDto, void>({
        path: `/subDept/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  team = {
    /**
     * No description
     *
     * @tags team-controller
     * @name FindAllTeamsUsingGet
     * @summary findAllTeams
     * @request GET:/team/
     */
    findAllTeamsUsingGet: (params: RequestParams = {}) =>
      this.request<TeamDto[], void>({
        path: `/team/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags team-controller
     * @name AddCarUsingPost
     * @summary addCar
     * @request POST:/team/car/add
     */
    addCarUsingPost: (teamCarDto: TeamCarDto, params: RequestParams = {}) =>
      this.request<CarDto, void>({
        path: `/team/car/add`,
        method: "POST",
        body: teamCarDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags team-controller
     * @name RemoveCarUsingPost
     * @summary removeCar
     * @request POST:/team/car/remove
     */
    removeCarUsingPost: (teamCarDto: TeamCarDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/team/car/remove`,
        method: "POST",
        body: teamCarDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags team-controller
     * @name AddDriverUsingPost
     * @summary addDriver
     * @request POST:/team/driver/add
     */
    addDriverUsingPost: (teamDriverDto: TeamDriverDto, params: RequestParams = {}) =>
      this.request<DriverDto, void>({
        path: `/team/driver/add`,
        method: "POST",
        body: teamDriverDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags team-controller
     * @name RemoveDriverUsingPost
     * @summary removeDriver
     * @request POST:/team/driver/remove
     */
    removeDriverUsingPost: (teamDriverDto: TeamDriverDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/team/driver/remove`,
        method: "POST",
        body: teamDriverDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags team-controller
     * @name GetTeamByIdUsingGet
     * @summary getTeamById
     * @request GET:/team/{id}
     */
    getTeamByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<TeamDto, void>({
        path: `/team/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  user = {
    /**
     * No description
     *
     * @tags user-controller
     * @name FindAllUsersUsingGet
     * @summary findAllUsers
     * @request GET:/user/
     */
    findAllUsersUsingGet: (params: RequestParams = {}) =>
      this.request<UserDto[], void>({
        path: `/user/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name FindAllEngineersUsingGet
     * @summary findAllEngineers
     * @request GET:/user/engineer/
     */
    findAllEngineersUsingGet: (params: RequestParams = {}) =>
      this.request<UserDto[], void>({
        path: `/user/engineer/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name CreateEngineerUsingPost
     * @summary createEngineer
     * @request POST:/user/engineer/create
     */
    createEngineerUsingPost: (userDto: UserCreateDto, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/user/engineer/create`,
        method: "POST",
        body: userDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name DeleteEngineerUsingPost
     * @summary deleteEngineer
     * @request POST:/user/engineer/delete
     */
    deleteEngineerUsingPost: (userID: BaseDto, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/user/engineer/delete`,
        method: "POST",
        body: userID,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name UpdateEngineerUsingPost
     * @summary updateEngineer
     * @request POST:/user/engineer/update
     */
    updateEngineerUsingPost: (userDto: UserDto, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/user/engineer/update`,
        method: "POST",
        body: userDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name LoginUsingPost
     * @summary login
     * @request POST:/user/login
     */
    loginUsingPost: (userLoginDto: UserLoginDto, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/user/login`,
        method: "POST",
        body: userLoginDto,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name FindAllManagersUsingGet
     * @summary findAllManagers
     * @request GET:/user/manager/
     */
    findAllManagersUsingGet: (params: RequestParams = {}) =>
      this.request<UserDto[], void>({
        path: `/user/manager/`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags user-controller
     * @name FindUserByIdUsingGet
     * @summary findUserById
     * @request GET:/user/{id}
     */
    findUserByIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/user/${id}`,
        method: "GET",
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
}
