import axios from "axios";
import { CarPartCreateDto, CarPartDto } from "../model/CarPart";
import { API_URL } from "../utils/envConstants";

export const getAvailableCarParts = async () => {
  const response = await axios.get(`${API_URL}/carPart/free`);
  return response.data as CarPartDto[];
};

export const getCarPart = async (id: number) => {
  const response = await axios.get(`${API_URL}/carPart/${id}`);
  return response.data as CarPartDto;
};

export const getCarParts = async () => {
  const response = await axios.get(`${API_URL}/carPart/`);
  return response.data as CarPartDto[];
};

export const createCarPart = async (carPart: CarPartCreateDto) => {
  const response = await axios.post(`${API_URL}/carPart/create`, carPart, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data as CarPartDto;
};

export const deleteCarPart = async (carPart: CarPartDto) => {
  const response = await axios.post(
    `${API_URL}/carPart/delete`,
    { id: carPart.id },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data as CarPartDto;
};
