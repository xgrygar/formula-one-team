import axios from "axios";
import { API_URL } from "../utils/envConstants";
import { TeamDto } from "../model/Team";

//Author: Roman Duris
//Project: Formula One Team

export const getTeam = async () => {
    const response = await axios.get(`${API_URL}/team/`);
    return (response.data as TeamDto[])[0];
};
