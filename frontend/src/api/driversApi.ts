import axios from "axios";
import { DriverCreateDto, DriverDto } from "../model/Driver";
import { API_URL } from "../utils/envConstants";

export const getDrivers = async () => {
  const response = await axios.get(`${API_URL}/driver/`);
  return response.data as DriverDto[];
};

export const getDriver = async (id: number) => {
  const response = await axios.get(`${API_URL}/driver/${id}`);
  return response.data as DriverDto;
};

export const updateDriver = async (driver: DriverDto) => {
  const response = await axios.post(`${API_URL}/driver/update`, driver, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data as DriverDto;
};

export const createDriver = async (driver: DriverCreateDto) => {
  const response = await axios.post(`${API_URL}/driver/create`, driver, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data as DriverDto;
};

export const deleteDriver = async (driver: DriverDto) => {
  const response = await axios.post(
    `${API_URL}/driver/delete`,
    { id: driver.id },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data as DriverDto;
};
