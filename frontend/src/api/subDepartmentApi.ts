import axios from "axios";
import { SubDepartmentDto } from "../model/SubDepartment";
import { API_URL } from "../utils/envConstants";

export const getSubDepartments = async () => {
  const response = await axios.get(`${API_URL}/subDept/`);
  return response.data as SubDepartmentDto[];
};

export const getSubDepartment = async (id: number) => {
  const response = await axios.get(`${API_URL}/subDept/${id}`);
  return response.data as SubDepartmentDto;
};

export const updateSubDepartment = async (subDepartment: SubDepartmentDto) => {
  const response = await axios.post(
    `${API_URL}/subDept/update`,
    subDepartment,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data as SubDepartmentDto;
};
