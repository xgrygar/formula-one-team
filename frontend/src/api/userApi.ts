import { UserDto, UserLoginDto } from "../model/User";

import axios from "axios";
import { API_URL } from "../utils/envConstants";
import { sha256 } from "js-sha256";

export const login = async (user: UserLoginDto) => {
  //   const req = `${API_URL}/user/login/${btoa(JSON.stringify(user))}`;
  const req = `${API_URL}/user/login`;
  const newUser = { username: user.username, password: sha256(user.password) };
  const response = await axios.post(req, newUser, {
    headers: {
      "Content-Type": "application/json",
    },
  });

  return response.data as UserDto;
};

export const getAllUsers = async () => {
  const response = await axios.get(`${API_URL}/user/`);
  return response.data;
};
