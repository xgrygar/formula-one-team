import axios from "axios";
import { CarDto } from "../model/Car";
import { API_URL } from "../utils/envConstants";

/**
 * @author Jakub Urban
 * @project Formula One Team
 */
export const getCars = async () => {
    const response = await axios.get(`${API_URL}/car/`);
    return response.data as CarDto[];
};

export const getCar = async (id: number) => {
    const response = await axios.get(`${API_URL}/car/${id}`);
    return response.data as CarDto;
};

export const updateCar = async (car: CarDto) => {
    const response = await axios.post(`${API_URL}/car/update`, car, {
        headers: {
            "Content-Type": "application/json",
        },
    });
    return response.data as CarDto;
};