import axios from "axios";
import { sha256 } from "js-sha256";
import { UserCreateDto, UserDto } from "../model/User";
import { API_URL } from "../utils/envConstants";

export const getEngineers = async () => {
  const response = await axios.get(`${API_URL}/user/engineer/`);
  return response.data as UserDto[];
};

export const getEngineer = async (id: number) => {
  const response = await axios.get(`${API_URL}/user/${id}`);
  return response.data as UserDto;
};

export const createEngineer = async (user: UserCreateDto) => {
  const newUser = { ...user, password: sha256(user.password) };
  const response = await axios.post(
    `${API_URL}/user/engineer/create`,
    newUser,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data as UserDto;
};

export const updateEngineer = async (user: UserDto) => {
  const response = await axios.post(`${API_URL}/user/engineer/update`, user, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data as UserDto;
};

export const deleteEngineer = async (engineer: UserDto) => {
  const response = await axios.post(
    `${API_URL}/user/engineer/delete`,
    { id: engineer.id },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data as UserDto;
};
