import { SubDepartmentDto } from "../model/SubDepartment";

export const FRONT_WING = "FRONT_WING";
export const REAR_WING = "REAR_WING";
export const SUSPENSION = "SUSPENSION";
export const ENGINE = "ENGINE";

export const MANAGER = "MANAGER";
export const ENGINEER = "ENGINEER";

export const ENGINE_DEPARTMENT = "EngineDepartment";
export const SUSPENSION_DEPARTMENT = "SuspensionDepartment";
export const AERODYNAMICS_DEPARTMENT = "AeroDynamicsDepartment";

export const getCarPartTypesForSubDepartment = (
  subDepartment: SubDepartmentDto
) => {
  if (subDepartment.departmentType === ENGINE_DEPARTMENT) {
    return [ENGINE];
  }
  if (subDepartment.departmentType === SUSPENSION_DEPARTMENT) {
    return [SUSPENSION];
  }
  if (subDepartment.departmentType === AERODYNAMICS_DEPARTMENT) {
    return [FRONT_WING, REAR_WING];
  }
  return [];
};
