# PA165 - Formula One Manager

*PA165 course at MUNI Spring 2022.*

## About Project

- **Name**: Formula One Team
- **Technologies**: Java, React
- **Developers**:
    - Ján Popeláš - _Project manager, Java developer_ @xpopelas
    - Adam Grygar - _SW architect, FE developer_ @xgrygar
    - Roman Ďuriš - _Java developer_ @xduris1
    - Jakub Urban - _Tester_ @xurban4
- **Assigment**:
    - A Formula 1 team is in need to manage its participation to the world championship. The team has two cars
      participating to each race, each one driven by one driver, with other test drivers available to develop the car
      further. The manager of the team can set the driver for each car and manage a list of other (test) drivers that
      are under contract and he can also set as main drivers if unhappy about results. Each driver has a name, surname,
      nationality, and set of characteristics (e.g. driving on the wet, aggressiveness, etc..) that the manager can
      visualize. The car is composed of several components (engine, suspensions), that the different departments of the
      team (e.g. engine, aerodynamics) evolve during time. The manager can pick from a list of components and assemble
      the cars that will participate to the next race. Engineers from other departments can login to the system and put
      newer components available. The manager is notified that those components are available and that they can be used
      to improve the car.

## Build and setup

### 1. Backend Setup

1. Go into the root directory (`formula-one-team`).
2. Run `mvn clean install`
3. After that is done, go into `module-rest` folder
4. Run `mvn cargo:run`
5. Now the BE REST API is running. The requests can be sent to `http://localhost:8080/pa165/rest`

### 2. Frontend Setup

1. Make sure that you have [node LTS](https://nodejs.org/en/download/) installed
2. Go to **formula-one-team/frontend**
3. Run 
    - `npm install --global yarn`
    - `yarn install`
    - `yarn start`
4. Make sure that the BE is running
5. Open browser website: [http://localhost:3000](http://localhost:3000)

### 3. Swagger Installation Instructions [OPTIONAL]
To make the testing of API easier (and seeing all API endpoints in one place), we have integrated Swagger.
Swagger allows you to see all of the available API in one webpage. Follow next steps to setup Swagger:

1. In **formula-one-team** run ```mvn clean install```
2. In **formula-one-team/module-rest** run ```mvn cargo:run```
3. Run ```npm install swagger-axios-codegen```
4. In **formula-one-team/frontend** run ```npx swagger-typescript-api -p http://localhost:8080/pa165/rest/v2/api-docs -o ./src -n api-docs.ts```
5. In **formula-one-team/frontend** run ```npm run build```
6. You can now see the docs at [http://localhost:8080/pa165/rest/swagger-ui.html](http://localhost:8080/pa165/rest/swagger-ui.html)

## Usage
- The Backend can be accessed from [http://localhost:8080/pa165/rest](http://localhost:8080/pa165/rest)
- The Frontend can be accessed from [http://localhost:3000](http://localhost:3000)
- The Swagger UI for REST API testing can be accessed on the address: [http://localhost:8080/pa165/rest/swagger-ui.html](http://localhost:8080/pa165/rest/swagger-ui.html)

### Login
- To login as Manager, type in the following credentials:
```
username: chris.horner
password: password
```

- To login as Engineer, you can choose any of the following usernames: `cory.jlucero`, `radek.feri`, `rene.trommler`.

Password is `password` for all of them.

## Project Description

Formula One Manager is a simple information system for managing formula one team.

### Roles

System has two authorization roles - **Manager** and **Engineer**.

- Manager is a role for organization cars, drivers, departments, and engineers. Manager also assemble the car from
  components.
- Engineer is assigned to specific department. Engineers can add new components into system. Manager is then notified.

## Diagrams

![Class Diagram](diagrams/Formula%20One%20Team%20-%20class%20diagram.png)
---

![UseCase Diagram](diagrams/Formula%20One%20Team%20-%20use%20case%20diagram.drawio.png)


