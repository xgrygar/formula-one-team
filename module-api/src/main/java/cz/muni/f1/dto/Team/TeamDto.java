package cz.muni.f1.dto.Team;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.dto.User.UserDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class TeamDto extends BaseDto {
    private UserDto manager;
    private List<DriverDto> drivers;
}
