package cz.muni.f1.dto.Driver;

import cz.muni.f1.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class DriverDto extends BaseDto {
    private String name;
    private String surname;
    private String nationality;
    private int racePerformance;
    private int wetPerformance;
    private int agresiveness;
    private int racePace;
    private int qualificationPace;
}
