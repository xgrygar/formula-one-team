package cz.muni.f1.dto.CarPart;

import cz.muni.f1.enums.PartType;
import lombok.Getter;
import lombok.Setter;


/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@Getter
@Setter
public class CarPartCreateDto {
    private String partName;
    private String partInfo;
    private PartType partType;
}
