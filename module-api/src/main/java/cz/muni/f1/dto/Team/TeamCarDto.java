package cz.muni.f1.dto.Team;

import lombok.Getter;
import lombok.Setter;

/**
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Getter
@Setter
public class TeamCarDto {
    private Long teamId;
    private Long carId;
}
