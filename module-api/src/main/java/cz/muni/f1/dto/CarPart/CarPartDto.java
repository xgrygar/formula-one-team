package cz.muni.f1.dto.CarPart;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.enums.PartType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class CarPartDto extends BaseDto {
    private String partName;
    private String partInfo;
    private PartType partType;
}
