package cz.muni.f1.dto.User;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 * @project Formula One Team
 */
@Getter
@Setter
public final class UserLoginDto {
    private String username;
    private String password;
}
