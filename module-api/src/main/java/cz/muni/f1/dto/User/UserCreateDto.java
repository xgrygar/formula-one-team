package cz.muni.f1.dto.User;

import cz.muni.f1.enums.UserType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreateDto {
    private String name;
    private String surname;
    private String nationality;
    private String username;
    private String password;
    private UserType role;
}
