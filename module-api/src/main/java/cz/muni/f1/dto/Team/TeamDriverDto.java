package cz.muni.f1.dto.Team;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@Getter
@Setter
public class TeamDriverDto {
    private Long teamID;
    private Long driverID;
}
