package cz.muni.f1.dto.EngineeringDepartment;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentDto;
import cz.muni.f1.dto.User.UserDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class EngineeringDepartmentDto extends BaseDto {
    private List<UserDto> engineers;
    private SubDepartmentDto suspensionDepartment;
    private SubDepartmentDto aeroDepartment;
    private SubDepartmentDto engineDepartment;
}
