package cz.muni.f1.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 * General DTO to serve as a base class for other DTOs.
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class BaseDto {
    protected Long id;
}
