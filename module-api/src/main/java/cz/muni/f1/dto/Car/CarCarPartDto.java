package cz.muni.f1.dto.Car;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarCarPartDto {
    private Long carId;
    private Long carPartId;
}
