package cz.muni.f1.dto.SubDepartment;

import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.enums.DepartmentType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubDepartmentCreateDto {
    private List<UserDto> engineers;
    private List<CarPart> carParts;
    private DepartmentType departmentType;
}
