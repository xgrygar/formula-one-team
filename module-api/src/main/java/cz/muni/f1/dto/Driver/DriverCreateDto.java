package cz.muni.f1.dto.Driver;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverCreateDto {
    private String name;
    private String surname;
    private String nationality;
    private int racePerformance;
    private int wetPerformance;
    private int agresiveness;
    private int racePace;
    private int qualificationPace;
}
