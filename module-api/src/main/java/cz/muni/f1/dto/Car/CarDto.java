package cz.muni.f1.dto.Car;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.dto.Driver.DriverDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class CarDto extends BaseDto {
    private DriverDto driver;
    private CarPartDto engine;
    private CarPartDto suspension;
    private CarPartDto frontWing;
    private CarPartDto rearWing;
}
