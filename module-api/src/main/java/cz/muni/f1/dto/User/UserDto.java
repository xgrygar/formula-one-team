package cz.muni.f1.dto.User;

import cz.muni.f1.dto.BaseDto;
import cz.muni.f1.enums.UserType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adam Grygar
 */
@Getter
@Setter
public final class UserDto extends BaseDto {
    private String name;
    private String surname;
    private String nationality;
    private String username;
    private String password;
    private UserType role;
}
