package cz.muni.f1.facade;

import cz.muni.f1.dto.User.UserCreateDto;
import cz.muni.f1.dto.User.UserDto;

import java.util.List;

/**
 * @author Adam Grygar
 */
public interface UserFacade {
    /**
     * Finds all users
     *
     * @return list of all users
     */
    List<UserDto> findAllUsers();

    /**
     * find specific user
     *
     * @param id id of desired user
     * @return desired user
     */
    UserDto getUserById(Long id);

    /**
     * finds all managers
     *
     * @return list of all managers
     */
    List<UserDto> findAllManagers();

    /**
     * finds all engineers
     *
     * @return list of all enginerrs
     */
    List<UserDto> findAllEngineers();

    /**
     * logs a user
     *
     * @param username users useram
     * @param password users password
     * @return dto of a logged user
     */
    UserDto logIn(String username, String password);

    /**
     * udpates user
     *
     * @param user dto with new values
     * @return updated user dto
     */
    UserDto updateUser(UserDto user);

    /**
     * creates a new user
     *
     * @param user dto with new values
     * @return created user
     */
    UserDto createUser(UserDto user);

    /**
     * create user used in rest without ID
     *
     * @param userCreateDto user dto withou ID
     * @return created user dto with ID
     */
    UserDto createUserRest(UserCreateDto userCreateDto);

    /**
     * deletes user
     *
     * @param user user to be deleted
     */
    void deleteUser(UserDto user);

    /**
     * deletes engeneer
     *
     * @param user engineer to be deleted
     */
    void deleteEngineer(UserDto user);
}
