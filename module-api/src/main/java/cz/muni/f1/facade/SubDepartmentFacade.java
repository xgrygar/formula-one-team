package cz.muni.f1.facade;

import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentCreateDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentDto;
import cz.muni.f1.dto.User.UserDto;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Roman Duris - javaddocs
 */
public interface SubDepartmentFacade {
    /**
     * finds all subedepts.
     * @return list of all subdepts
     */
    List<SubDepartmentDto> findAllSubDepartments();

    /**
     * return specififc subdept.
     * @param id id of desired subdepartment
     * @return desired subdept. dto
     */
    SubDepartmentDto getSubDepartmentById(Long id);

    /**
     * adds engineer to subdepts.
     * @param SUbDepartmentID ID of edited subdept.
     * @param engineer engeneer to add
     */
    void addEngineer(Long SUbDepartmentID, UserDto engineer);

    /**
     *remove engineer form subdept.
     * @param SUbDepartmentID ID of edited subdept.
     * @param engineer engineer to remove
     */
    void removeEngineer(Long SUbDepartmentID, UserDto engineer);

    /**
     * adds carparts to subdept
     * @param SubDepartmentID id of edited subdept.
     * @param carPart carpart to add
     */
    void addCarpart(Long SubDepartmentID, CarPartDto carPart);

    /**
     * creates a subdepartment from dto without ID
     * @param subDepartmentDto rest dto without ID
     * @return subdept dto with ID
     */
    SubDepartmentDto createSubDepartment(SubDepartmentCreateDto subDepartmentDto);

    /**
     * removes subdepts
     * @param id id of removed subdept.
     * @return if subdept has been removed
     */
    Boolean deleteSubDepartment(Long id);

    /**
     * upadtes subdept with values from new subdept dto
     * @param subDepartmentDto new subdept dto
     * @return updated dto
     */
    SubDepartmentDto updateSubDepartment(SubDepartmentDto subDepartmentDto);
}
