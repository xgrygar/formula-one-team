package cz.muni.f1.facade;

import cz.muni.f1.dto.Driver.DriverCreateDto;
import cz.muni.f1.dto.Driver.DriverDto;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Jakub Urban <485632@mail.muni.cz>
 * @author Roman Duris - javaddocs
 * @project Formula One Team
 */
public interface DriverFacade {
    /**
     * Finds all drivers
     * @return list of al drivers
     */
    List<DriverDto> findAllDrivers();

    /**
     * finds specific driver
     * @param id id of the driver
     * @return driver with the given id
     */
    DriverDto getDriverById(Long id);

    /**
     * updates the driver with new values
     * @param driver dto with new values
     * @return updated driver dto
     */
    DriverDto updateDriver(DriverDto driver);

    /**
     * creates driver
     * @param driver dto with new driver valuess
     * @return new driver dto
     */
    DriverDto createDriver(DriverDto driver);

    /**
     * creates driver from dto without ID
     * @param driver driver dto withou ID
     * @return new driver dto
     */
    DriverDto createDriverRest(DriverCreateDto driver);

    /**
     * removes driver
     * @param driver driver to be removed
     */
    void deleteDriver(DriverDto driver);

}
