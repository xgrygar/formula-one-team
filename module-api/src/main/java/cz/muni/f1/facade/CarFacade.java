package cz.muni.f1.facade;

import cz.muni.f1.dto.Car.CarDto;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Roman Duris - javadocs
 */
public interface CarFacade {
    /**
     * Finds all cars
     *
     * @return List of All cars
     */
    List<CarDto> findAllCars();

    /**
     * Finds car with the given ID
     *
     * @param id id of a car to be found
     * @return car with the given ID
     */
    CarDto getCarById(Long id);

    /**
     * Changes driver of the car
     *
     * @param driverID replacement driver ID
     * @param carID    car ID where driver should be replaced
     */
    void changeDriver(Long driverID, Long carID);

    /**
     * Changes engine of the car
     *
     * @param engineID replacement engine ID
     * @param carID    car ID where engine should be replaced
     */
    void changeEngine(Long engineID, Long carID);

    /**
     * Changes front wing of the car
     *
     * @param frontWingID replacement front wing ID
     * @param carID       car ID where front wing should be replaced
     */
    void changeFrontWing(Long frontWingID, Long carID);

    /**
     * Changes rear wing of the car
     *
     * @param rearWingID replacement rear wing ID
     * @param carID      car ID where rear wing should be replaced
     */
    void changeRearWing(Long rearWingID, Long carID);

    /**
     * Changes suspension of the car
     *
     * @param suspensionID replacement suspension ID
     * @param carID        car ID where suspension should be replaced
     */
    void changeSuspension(Long suspensionID, Long carID);

    /**
     * Updates the car from the values from Car Dto
     *
     * @param carDto car object with new values
     */
    void updateCar(CarDto carDto);
}
