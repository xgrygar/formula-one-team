package cz.muni.f1.facade;

import cz.muni.f1.dto.Car.CarDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.dto.Team.TeamDto;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Roman Duris - javaddocs
 */
public interface TeamFacade {
    /**
     * finds all teams
     *
     * @return list of all teams
     */
    List<TeamDto> findAllTeams();

    /**
     * find one specific team
     *
     * @param id id of desired team
     * @return desired team
     */
    TeamDto getTeamById(Long id);

    /**
     * adds car to the team
     *
     * @param teamID edited team ID
     * @param carId  Id of a car to add
     * @return added car dto
     */
    CarDto addCar(Long teamID, Long carId);

    /**
     * removes car from team
     *
     * @param teamID edited team ID
     * @param carId  ID of a car to remove
     * @return if car was removed
     */
    Boolean removeCar(Long teamID, Long carId);

    /**
     * adds driver to the team
     *
     * @param teamID   edited team id
     * @param driverID id of a driver to add
     * @return added driver dto
     */
    DriverDto addDriver(Long teamID, Long driverID);

    /**
     * removes driver from the team
     *
     * @param teamID   edited team id
     * @param driverID id of a driver to remove
     * @return if driver was removed
     */
    Boolean removeDriver(Long teamID, Long driverID);

}
