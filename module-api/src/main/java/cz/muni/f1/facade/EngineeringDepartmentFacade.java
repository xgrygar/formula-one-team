package cz.muni.f1.facade;

import cz.muni.f1.dto.EngineeringDepartment.EngineeringDepartmentDto;
import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.entity.EngineeringDepartment;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Roman Duris - javaddocs
 */
public interface EngineeringDepartmentFacade {
    /**
     * finds all engineering depts.
     * @return list of all engineering depts.
     */
    List<EngineeringDepartmentDto> findAllEngineeringDepartments();

    /**
     * finds specific eng. dept.
     * @param id id of the dept.
     * @return specific dept.
     */
    EngineeringDepartmentDto getEngineeringDepartmentById(Long id);

    /**
     * Creates and adds engineer to department
     *
     * @param engineer new engineer
     */
    void addEngineer(EngineeringDepartment engineeringDepartment, UserDto engineer);

    /**
     * removes engineer from the department
     * @param engineeringDepartment department to be edited
     * @param engineer engineer to be removed
     */
    void removeEngineer(EngineeringDepartment engineeringDepartment, UserDto engineer);

    /**
     * changes subdepartments for the given engineering department
     * @param DepartmentID edited department
     * @param subDepartmentID subdepartment to be changed
     * @throws Exception if the subdepartment does not exist
     */
    void changeSubDepartment(Long DepartmentID, Long subDepartmentID) throws Exception;

    /**
     * creates new eng. dept from engineering dept. dto
     * @param engineeringDepartmentDto dto with new values
     * @return new engineering department
     */
    EngineeringDepartmentDto createEngineeringDepartment(EngineeringDepartmentDto engineeringDepartmentDto);

    /**
     * updates engineering department with new values
     * @param engineeringDepartmentDto dto with new values
     * @return updated engineering department
     */
    EngineeringDepartmentDto updateEngineeringDepartment(EngineeringDepartmentDto engineeringDepartmentDto);

    /**
     * removes engineering department
     * @param id id of eng. dept. to be removed
     * @return if eng. dept. has been removed
     */
    Boolean deleteEngineeringDepartment(Long id);
}
