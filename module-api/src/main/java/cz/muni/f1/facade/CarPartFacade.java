package cz.muni.f1.facade;

import cz.muni.f1.dto.CarPart.CarPartCreateDto;
import cz.muni.f1.dto.CarPart.CarPartDto;

import java.util.List;

/**
 * @author Adam Grygar
 * @author Roman Duris - javaddocs
 */
public interface CarPartFacade {
    /**
     * Finds all Car Parts
     *
     * @return List of all car Parts
     */
    List<CarPartDto> findAllCarParts();

    /**
     * Finds corresponding Car Part to the given ID
     *
     * @param id ID of the desired Car Part
     * @return desired Car Part
     */
    CarPartDto getCarPartById(Long id);

    /**
     * Returns all Car Parts that are not assiged to a Car
     *
     * @return List of free carparts
     */
    List<CarPartDto> getAllFreeCarParts();

    /**
     * Creates Car Part
     *
     * @param carPartDto Dto with new Car Part values
     */
    void create(CarPartDto carPartDto);

    /**
     * Create Car Part used in Rest Layer. Works without ID.
     *
     * @param carPartDto Dto object Containing CarPart without ID
     * @return Crated Car Part with ID
     */
    CarPartDto createRest(CarPartCreateDto carPartDto);

    /**
     * Updates Car Part according to Dto
     *
     * @param carPartDto updated Car Part
     */
    void update(CarPartDto carPartDto);

    /**
     * removes CarPart
     *
     * @param id id of Car Part to be removed
     */
    void remove(long id);
}
