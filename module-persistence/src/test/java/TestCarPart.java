import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.enums.PartType;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Car Part Entity and Dao
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestCarPart extends AbstractTestNGSpringContextTests {

    @Autowired
    private CarPartDAO carPartDao;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from CarPart").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private CarPart findById(long id) {
        EntityManager em = null;
        CarPart carPart;
        try {
            em = emf.createEntityManager();
            carPart = em.find(CarPart.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return carPart;
    }

    private List<CarPart> findAll() {
        List<CarPart> carParts;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            carParts = em.createQuery("select cp from CarPart cp", CarPart.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return carParts;
    }

    private CarPart createCarPartEntity(String partName, JSONObject partInfo, PartType partType) {
        CarPart carPart = new CarPart();
        carPart.setPartName(partName);
        carPart.setPartInfo(partInfo.toString());
        carPart.setPartType(partType);
        return carPart;
    }

    private CarPart createCarPart(String partName, JSONObject partInfo, PartType partType) {
        CarPart carPart = createCarPartEntity(partName, partInfo, partType);
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(carPart);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return carPart;
    }

    @Test
    public void testCreate() {
        CarPart carPart = createCarPartEntity("V8", new JSONObject(), PartType.ENGINE);
        carPartDao.create(carPart);
        assertThat(carPart.getId())
                .isNotNull();
        assertThat(this.findById(carPart.getId()))
                .isNotSameAs(carPart)
                .usingRecursiveComparison()
                .isEqualTo(carPart);

    }

    @Test
    public void testFindAll() {
        CarPart carPart = createCarPart("V8", new JSONObject(), PartType.ENGINE);
        CarPart carPart2 = createCarPart("v45", new JSONObject(), PartType.FRONT_WING);
        List<CarPart> carParts = this.findAll();
        assertThat(carParts)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(carPart, carPart2);
    }

    @Test
    public void testFindById() {
        CarPart carPart = createCarPart("V8", new JSONObject(), PartType.ENGINE);
        createCarPart("v45", new JSONObject(), PartType.FRONT_WING);
        assertThat(carPartDao.findById(carPart.getId()))
                .isNotSameAs(carPart)
                .usingRecursiveComparison()
                .isEqualTo(carPart);
    }

    @Test
    public void testFindByPartName() {
        CarPart carPart = createCarPart("V8", new JSONObject(), PartType.ENGINE);
        createCarPart("v45", new JSONObject(), PartType.FRONT_WING);
        assertThat(carPartDao.findByName(carPart.getPartName()))
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(carPart);
    }

    @Test
    public void testUpdate() {
        CarPart carPart = createCarPart("V8", new JSONObject(), PartType.ENGINE);
        carPart.setPartName("V6");
        carPartDao.update(carPart);
        assertThat(this.findById(carPart.getId()))
                .isNotSameAs(carPart)
                .usingRecursiveComparison()
                .isEqualTo(carPart);
    }

    @Test
    public void testRemove() {
        CarPart carPart = createCarPart("V8", new JSONObject(), PartType.ENGINE);
        CarPart carPart2 = createCarPart("v45", new JSONObject(), PartType.FRONT_WING);
        carPartDao.remove(carPart2);
        List<CarPart> carParts = this.findAll();
        assertThat(carParts)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(carPart);
    }
}
