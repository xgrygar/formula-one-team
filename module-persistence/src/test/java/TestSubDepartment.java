import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for SubDepartment Entity and Dao
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestSubDepartment extends AbstractTestNGSpringContextTests {

    @Autowired
    private SubDepartmentDAO subDepartmentDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from SubDepartment").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private SubDepartment findById(long id) {
        EntityManager em = null;
        SubDepartment subDepartment;
        try {
            em = emf.createEntityManager();
            subDepartment = em.find(SubDepartment.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return subDepartment;
    }

    private List<SubDepartment> findAll() {
        List<SubDepartment> subDepartments;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            subDepartments = em.createQuery("select p from SubDepartment p", SubDepartment.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return subDepartments;
    }

    private SubDepartment createSubDepartmentEntity(Set<User> engineers, Set<CarPart> carParts, DepartmentType departmentType) {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setEngineers(engineers);
        subDepartment.setCarParts(carParts);
        subDepartment.setDepartmentType(departmentType);
        return subDepartment;
    }

    private SubDepartment createSubDepartment(Set<User> engineers, Set<CarPart> carParts, DepartmentType departmentType) {
        SubDepartment subDepartment = createSubDepartmentEntity(engineers, carParts, departmentType);
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(subDepartment);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return subDepartment;
    }

    @Test
    public void testCreate() {
        SubDepartment subDepartment = createSubDepartmentEntity(new HashSet<>(), new HashSet<>(), DepartmentType.EngineDepartment);
        subDepartmentDAO.create(subDepartment);
        assertThat(subDepartment.getId())
                .isNotNull();
        assertThat(this.findById(subDepartment.getId()))
                .isNotSameAs(subDepartment)
                .usingRecursiveComparison()
                .isEqualTo(subDepartment);

    }

    @Test
    public void testFindAll() {
        SubDepartment subDepartment = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.EngineDepartment);
        SubDepartment subDepartment2 = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.SuspensionDepartment);
        List<SubDepartment> subDepartments = this.findAll();
        assertThat(subDepartments)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(subDepartment, subDepartment2);
    }

    @Test
    public void testFindById() {
        SubDepartment subDepartment = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.EngineDepartment);
        createSubDepartmentEntity(new HashSet<>(), new HashSet<>(), DepartmentType.SuspensionDepartment);
        assertThat(subDepartmentDAO.findById(subDepartment.getId()))
                .isNotSameAs(subDepartment)
                .usingRecursiveComparison()
                .isEqualTo(subDepartment);
    }


    @Test
    public void testUpdate() {
        SubDepartment subDepartment = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.EngineDepartment);
        subDepartment.setDepartmentType(DepartmentType.AeroDynamicsDepartment);
        subDepartmentDAO.update(subDepartment);
        assertThat(this.findById(subDepartment.getId()))
                .isNotSameAs(subDepartment)
                .usingRecursiveComparison()
                .isEqualTo(subDepartment);
    }

    @Test
    public void testRemove() {
        SubDepartment subDepartment = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.EngineDepartment);
        SubDepartment subDepartment2 = createSubDepartment(new HashSet<>(), new HashSet<>(), DepartmentType.SuspensionDepartment);
        subDepartmentDAO.remove(subDepartment2);
        List<SubDepartment> subDepartments = this.findAll();
        assertThat(subDepartments)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(subDepartment);
    }
}
