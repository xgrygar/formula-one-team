import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.TeamDAO;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.Team;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Team Entity and Dao
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestTeam extends AbstractTestNGSpringContextTests {

    @Autowired
    private TeamDAO teamDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("DELETE FROM Team").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private void persistEntity(Object entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Team findById(long id) {
        EntityManager em = null;
        Team team;
        try {
            em = emf.createEntityManager();
            team = em.find(Team.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return team;
    }

    private List<Team> findAll() {
        List<Team> teams;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            teams = em.createQuery("select t from Team t", Team.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return teams;
    }

    private Driver createDriver(String name, String surname, String nationality, int qualificationPace, int racePace, int agresiveness, int wetPerformance) {
        var driver = new Driver();
        driver.setName(name);
        driver.setSurname(surname);
        driver.setNationality(nationality);
        driver.setQualificationPace(qualificationPace);
        driver.setAgresiveness(agresiveness);
        driver.setRacePace(racePace);
        driver.setWetPerformance(wetPerformance);
        this.persistEntity(driver);
        return driver;
    }

    private Set<Driver> createDriversSet() {
        Set<Driver> drivers = new HashSet<>();
        drivers.add(createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9));
        drivers.add(createDriver("Lewis", "Hamilton", "BRI", 8, 9, 5, 7));
        return drivers;

    }

    private User createManager() {
        User manager = new User();
        manager.setName("John");
        manager.setSurname("Doe");
        manager.setNationality("Czech");
        manager.setPassword("password1234");
        manager.setRole(UserType.MANAGER);
        manager.setUsername("manager");
        persistEntity(manager);
        return manager;
    }


    private Team createTeamEntity(Set<Driver> drivers, User manager) {
        Team team = new Team();
        team.setDrivers(drivers);
        team.setManager(manager);
        return team;
    }

    private Team createTeam(Set<Driver> drivers, User manager) {
        Team team = createTeamEntity(drivers, manager);
        persistEntity(team);
        return team;
    }

    @Test
    public void testCreate() {
        Team team = createTeamEntity(createDriversSet(), createManager());
        teamDAO.create(team);
        assertThat(team.getId())
                .isNotNull();
        assertThat(this.findById(team.getId()))
                .isNotSameAs(team)
                .usingRecursiveComparison()
                .isEqualTo(team);

    }

    @Test
    public void testFindAll() {
        Team team = createTeam(createDriversSet(), createManager());
        Team team2 = createTeam(createDriversSet(), createManager());
        List<Team> teams = teamDAO.findAll();
        assertThat(teams)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(team, team2);
    }

    @Test
    public void testFindById() {
        Team team = createTeam(createDriversSet(), createManager());
        createTeam(createDriversSet(), createManager());
        assertThat(teamDAO.findById(team.getId()))
                .isNotSameAs(team)
                .usingRecursiveComparison()
                .isEqualTo(team);
    }

    @Test
    public void testUpdate() {
        Team team = createTeam(createDriversSet(), createManager());
        User manager2 = createManager();
        manager2.setUsername("bestmanager");
        team.setManager(manager2);
        teamDAO.update(team);
        assertThat(this.findById(team.getId()))
                .isNotSameAs(team)
                .usingRecursiveComparison()
                .isEqualTo(team);
    }

    @Test
    public void testRemove() {
        Team team = createTeam(createDriversSet(), createManager());
        Team team2 = createTeam(createDriversSet(), createManager());
        teamDAO.remove(team2);
        List<Team> teams = this.findAll();
        assertThat(teams)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(team);

    }
}
