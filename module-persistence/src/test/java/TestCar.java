import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.CarDAO;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.enums.PartType;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Car Entity and Dao
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestCar extends AbstractTestNGSpringContextTests {

    @Autowired
    private CarDAO carDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from Car").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Car findById(long id) {
        EntityManager em = null;
        Car Car;
        try {
            em = emf.createEntityManager();
            Car = em.find(Car.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return Car;
    }

    private List<Car> findAll() {
        List<Car> cars;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            cars = em.createQuery("select c from Car c", Car.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return cars;
    }

    private void persistEntity(Object entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private CarPart createEngine() {
        CarPart carPart = new CarPart();
        carPart.setPartName("Engine");
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartType(PartType.ENGINE);
        this.persistEntity(carPart);
        return carPart;
    }

    private CarPart createSuspension() {
        CarPart carPart = new CarPart();
        carPart.setPartName("Suspension");
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartType(PartType.SUSPENSION);
        this.persistEntity(carPart);
        return carPart;
    }

    private CarPart createFrontWing() {
        CarPart carPart = new CarPart();
        carPart.setPartName("FrontWing");
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartType(PartType.FRONT_WING);
        this.persistEntity(carPart);
        return carPart;
    }

    private CarPart createRearWing() {
        CarPart carPart = new CarPart();
        carPart.setPartName("RearWing");
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartType(PartType.REAR_WING);
        this.persistEntity(carPart);
        return carPart;
    }

    private Driver createDriverOne() {
        var driver = new Driver();
        driver.setName("Valtteri");
        driver.setSurname("Bottas");
        driver.setNationality("FIN");
        driver.setQualificationPace(7);
        driver.setAgresiveness(5);
        driver.setRacePace(2);
        driver.setWetPerformance(9);
        this.persistEntity(driver);
        return driver;
    }

    private Driver createDriverTwo() {
        var driver = new Driver();
        driver.setName("Lewis");
        driver.setSurname("Hamilton");
        driver.setNationality("BRI");
        driver.setQualificationPace(2);
        driver.setAgresiveness(9);
        driver.setRacePace(5);
        driver.setWetPerformance(7);
        this.persistEntity(driver);
        return driver;
    }

    private Car createCarEntity(Driver driver, CarPart engine, CarPart suspension, CarPart frontWing, CarPart rearWing) {
        Car car = new Car();
        car.setDriver(driver);
        car.setEngine(engine);
        car.setSuspension(suspension);
        car.setFrontWing(frontWing);
        car.setRearWing(rearWing);
        return car;
    }

    private Car createCar(Driver driver, CarPart engine, CarPart suspension, CarPart frontWing, CarPart rearWing) {
        Car car = createCarEntity(driver, engine, suspension, frontWing, rearWing);
        this.persistEntity(car);
        return car;
    }

    @Test
    public void testCreate() {
        Car Car = createCarEntity(createDriverOne(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        carDAO.create(Car);
        assertThat(Car.getId())
                .isNotNull();
        assertThat(this.findById(Car.getId()))
                .isNotSameAs(Car)
                .usingRecursiveComparison()
                .isEqualTo(Car);
    }

    @Test
    public void testFindAll() {
        Car car = createCar(createDriverOne(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        Car car2 = createCar(createDriverTwo(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        List<Car> cars = this.findAll();
        assertThat(cars)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(car, car2);
    }

    @Test
    public void testFindById() {
        Car car = createCar(createDriverOne(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        createCarEntity(createDriverTwo(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        assertThat(carDAO.findById(car.getId()))
                .isNotSameAs(car)
                .usingRecursiveComparison()
                .isEqualTo(car);
    }

    @Test
    public void testUpdate() {
        Car car = createCar(createDriverOne(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        car.setDriver(createDriverTwo());
        carDAO.update(car);
        assertThat(this.findById(car.getId()))
                .isNotSameAs(car)
                .usingRecursiveComparison()
                .isEqualTo(car);
    }

    @Test
    public void testRemove() {
        Car car = createCar(createDriverOne(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        Car car2 = createCar(createDriverTwo(), createEngine(), createSuspension(), createFrontWing(), createRearWing());
        carDAO.remove(car);
        List<Car> cars = this.findAll();
        assertThat(cars)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(car2);

    }
}
