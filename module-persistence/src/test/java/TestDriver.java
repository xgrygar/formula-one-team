import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.entity.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Driver Entity and Dao
 *
 * @author Adam Grygar <485469@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestDriver extends AbstractTestNGSpringContextTests {

    @Autowired
    private DriverDAO driverDao;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from Driver").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Driver findById(long id) {
        EntityManager em = null;
        Driver driver;
        try {
            em = emf.createEntityManager();
            driver = em.find(Driver.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return driver;
    }

    private List<Driver> findAll() {
        List<Driver> drivers;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            drivers = em.createQuery("select d from Driver d", Driver.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return drivers;
    }

    private Driver createDriverEntity(String name, String surname, String nationality, int qualificationPace, int racePace, int agresiveness, int wetPerformance) {
        var driver = new Driver();
        driver.setName(name);
        driver.setSurname(surname);
        driver.setNationality(nationality);
        driver.setQualificationPace(qualificationPace);
        driver.setAgresiveness(agresiveness);
        driver.setRacePace(racePace);
        driver.setWetPerformance(wetPerformance);
        return driver;
    }

    private Driver createDriver(String name, String surname, String nationality, int qualificationPace, int racePace, int agresiveness, int wetPerformance) {
        Driver driver = createDriverEntity(name, surname, nationality, qualificationPace, racePace, agresiveness, wetPerformance);
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(driver);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return driver;
    }

    @Test
    public void testCreate() {
        Driver driver = createDriverEntity("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        driverDao.create(driver);
        assertThat(driver.getId())
                .isNotNull();
        assertThat(this.findById(driver.getId()))
                .isNotSameAs(driver)
                .usingRecursiveComparison()
                .isEqualTo(driver);
    }

    @Test
    public void testFindAll() {
        Driver driver = createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        Driver driver2 = createDriver("Lewis", "Hamilton", "BRI", 2, 9, 5, 7);
        List<Driver> drivers = this.findAll();
        assertThat(drivers)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(driver, driver2);
    }

    @Test
    public void testFindById() {
        Driver driver = createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        createDriver("Lewis", "Hamilton", "BRI", 2, 9, 5, 7);
        assertThat(driverDao.findById(driver.getId()))
                .isNotSameAs(driver)
                .usingRecursiveComparison()
                .isEqualTo(driver);
    }

    @Test
    public void testFindByName() {
        Driver driver = createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        createDriver("Lewis", "Hamilton", "BRI", 2, 9, 5, 7);
        assertThat(driverDao.findByName(driver.getName()))
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(driver);
    }

    @Test
    public void testUpdate() {
        Driver driver = createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        driver.setNationality("Canada");
        driverDao.update(driver);
        assertThat(this.findById(driver.getId()))
                .isNotSameAs(driver)
                .usingRecursiveComparison()
                .isEqualTo(driver);
    }

    @Test
    public void testRemove() {
        Driver driver = createDriver("Valtteri", "Bottas", "FIN", 7, 5, 2, 9);
        Driver driver2 = createDriver("Lewis", "Hamilton", "BRI", 2, 9, 5, 7);
        driverDao.remove(driver);
        List<Driver> drivers = this.findAll();
        assertThat(drivers)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(driver2);

    }
}
