import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.EngineeringDepartmentDAO;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for EngineeringDepartment Entity and Dao
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestEngineeringDepartment extends AbstractTestNGSpringContextTests {

    @Autowired
    private EngineeringDepartmentDAO engineeringDepartmentDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from EngineeringDepartment").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private EngineeringDepartment findById(long id) {
        EntityManager em = null;
        EngineeringDepartment engineeringDepartment;
        try {
            em = emf.createEntityManager();
            engineeringDepartment = em.find(EngineeringDepartment.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return engineeringDepartment;
    }

    private List<EngineeringDepartment> findAll() {
        List<EngineeringDepartment> engineeringDepartments;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            engineeringDepartments = em.createQuery("select p from EngineeringDepartment p", EngineeringDepartment.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return engineeringDepartments;
    }

    private void persistEntity(Object entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private SubDepartment createSuspensionDepartment() {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setEngineers(new HashSet<>());
        subDepartment.setCarParts(new HashSet<>());
        subDepartment.setDepartmentType(DepartmentType.SuspensionDepartment);
        this.persistEntity(subDepartment);
        return subDepartment;
    }

    private SubDepartment createAeroDepartment() {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setEngineers(new HashSet<>());
        subDepartment.setCarParts(new HashSet<>());
        subDepartment.setDepartmentType(DepartmentType.AeroDynamicsDepartment);
        this.persistEntity(subDepartment);
        return subDepartment;
    }

    private SubDepartment createEngineDepartment() {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setEngineers(new HashSet<>());
        subDepartment.setCarParts(new HashSet<>());
        subDepartment.setDepartmentType(DepartmentType.EngineDepartment);
        this.persistEntity(subDepartment);
        return subDepartment;
    }

    private EngineeringDepartment createEngineeringDepartmentEntity(Set<User> engineers, SubDepartment suspensionDepartment, SubDepartment aeroDepartment, SubDepartment engineDepartment) {
        EngineeringDepartment engineeringDepartment = new EngineeringDepartment();
        engineeringDepartment.setEngineers(engineers);
        engineeringDepartment.setSuspensionDepartment(suspensionDepartment);
        engineeringDepartment.setEngineDepartment(aeroDepartment);
        engineeringDepartment.setEngineDepartment(engineDepartment);
        return engineeringDepartment;
    }

    private EngineeringDepartment createEngineeringDepartment(Set<User> engineers, SubDepartment suspensionDepartment, SubDepartment aeroDepartment, SubDepartment engineDepartment) {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartmentEntity(engineers, suspensionDepartment, aeroDepartment, engineDepartment);
        this.persistEntity(engineeringDepartment);
        return engineeringDepartment;
    }

    @Test
    public void testCreate() {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartmentEntity(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        engineeringDepartmentDAO.create(engineeringDepartment);
        assertThat(engineeringDepartment.getId())
                .isNotNull();
        assertThat(this.findById(engineeringDepartment.getId()))
                .isNotSameAs(engineeringDepartment)
                .usingRecursiveComparison()
                .isEqualTo(engineeringDepartment);

    }

    @Test
    public void testFindAll() {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        EngineeringDepartment engineeringDepartment2 = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        List<EngineeringDepartment> engineeringDepartments = this.findAll();
        assertThat(engineeringDepartments)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(engineeringDepartment, engineeringDepartment2);
    }

    @Test
    public void testFindById() {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        assertThat(engineeringDepartmentDAO.findById(engineeringDepartment.getId()))
                .isNotSameAs(engineeringDepartment)
                .usingRecursiveComparison()
                .isEqualTo(engineeringDepartment);
    }


    @Test
    public void testUpdate() {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        engineeringDepartment.setAeroDepartment(createEngineDepartment());
        engineeringDepartmentDAO.update(engineeringDepartment);
        assertThat(this.findById(engineeringDepartment.getId()))
                .isNotSameAs(engineeringDepartment)
                .usingRecursiveComparison()
                .isEqualTo(engineeringDepartment);
    }

    @Test
    public void testRemove() {
        EngineeringDepartment engineeringDepartment = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        EngineeringDepartment engineeringDepartment2 = createEngineeringDepartment(new HashSet<>(), createSuspensionDepartment(), createAeroDepartment(), createEngineDepartment());
        engineeringDepartmentDAO.remove(engineeringDepartment2);
        List<EngineeringDepartment> engineeringDepartments = this.findAll();
        assertThat(engineeringDepartments)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(engineeringDepartment);
    }
}
