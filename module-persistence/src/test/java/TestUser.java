import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for User Entity and DAO.
 *
 * @author Ján Popeláš <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */

@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestUser extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserDAO userDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from User").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private void persistEntity(Object entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private User findById(long id) {
        EntityManager em = null;
        User user;
        try {
            em = emf.createEntityManager();
            user = em.find(User.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return user;
    }

    private List<User> findAll() {
        List<User> users;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            users = em.createQuery("select u from User u", User.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return users;
    }


    private User createUserEntity(String name, String surname, String nationality, String username, String password, UserType role) {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setNationality(nationality);
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

    private User sampleCorrectUser1() {
        User user = createUserEntity("John", "Doe", "USA", "john.doe", "password", UserType.MANAGER);
        this.persistEntity(user);
        return user;
    }

    private User sampleCorrectUser2() {
        User user = createUserEntity("Mike", "Foe", "Canada", "mike.foe", "password2", UserType.ENGINEER);
        this.persistEntity(user);
        return user;
    }

    @Test
    public void testCreate() {
        User user = createUserEntity("John", "Doe", "USA", "john.doe", "password", UserType.MANAGER);
        userDAO.create(user);
        assertThat(user.getId()).isNotNull();
        assertThat(this.findById(user.getId()))
                .isNotSameAs(user)
                .usingRecursiveComparison()
                .isEqualTo(user);

    }

    @Test
    public void testFindAll() {
        User user1 = sampleCorrectUser1();
        User user2 = sampleCorrectUser2();
        List<User> users = userDAO.findAll();
        assertThat(users)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(user1, user2);
    }

    @Test
    public void testFindById() {
        sampleCorrectUser1();
        User user2 = sampleCorrectUser2();
        assertThat(userDAO.findById(user2.getId()))
                .isNotSameAs(user2)
                .usingRecursiveComparison()
                .isEqualTo(user2);
    }

    @Test
    public void testFindByName() {
        sampleCorrectUser1();
        User user2 = sampleCorrectUser2();
        assertThat(userDAO.findByName(user2.getName()))
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(user2);
    }

    @Test
    public void testUpdate() {
        User user = sampleCorrectUser1();
        user.setName(user.getName() + "ette");
        userDAO.update(user);
        assertThat(userDAO.findById(user.getId()))
                .isNotSameAs(user)
                .usingRecursiveComparison()
                .isEqualTo(user);
        assertThat(this.findById(user.getId()))
                .isNotSameAs(user)
                .usingRecursiveComparison()
                .isEqualTo(user);
    }

    @Test
    public void testRemove() {
        User user1 = sampleCorrectUser1();
        User user2 = sampleCorrectUser2();
        assertThat(this.findAll())
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(user1, user2);
        userDAO.remove(user1);
        assertThat(this.findAll())
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .contains(user2);
    }
}
