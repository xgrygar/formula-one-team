import cz.muni.f1.PersistenceApplicationContext;
import cz.muni.f1.dao.PersonDAO;
import cz.muni.f1.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for Person Entity and Dao
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = PersistenceApplicationContext.class)
public class TestPerson extends AbstractTestNGSpringContextTests {

    @Autowired
    private PersonDAO personDAO;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @AfterMethod
    public void afterMethod() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.createQuery("delete from Person").executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Person findById(long id) {
        EntityManager em = null;
        Person person;
        try {
            em = emf.createEntityManager();
            person = em.find(Person.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return person;
    }

    private List<Person> findAll() {
        List<Person> persons;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            persons = em.createQuery("select p from Person p", Person.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return persons;
    }

    private Person createPersonEntity(String name, String surname, String nationality) {
        Person person = new Person();
        person.setName(name);
        person.setSurname(surname);
        person.setNationality(nationality);
        return person;
    }

    private Person createPerson(String name, String surname, String nationality) {
        Person person = createPersonEntity(name, surname, nationality);
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(person);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return person;
    }

    @Test
    public void testCreate() {
        Person person = createPersonEntity("John", "Doe", "USA");
        personDAO.create(person);
        assertThat(person.getId())
                .isNotNull();
        assertThat(this.findById(person.getId()))
                .isNotSameAs(person)
                .usingRecursiveComparison()
                .isEqualTo(person);

    }

    @Test
    public void testFindAll() {
        Person person = createPerson("John", "Doe", "USA");
        Person person2 = createPerson("Mike", "Foe", "Canada");
        List<Person> persons = this.findAll();
        assertThat(persons)
                .isNotEmpty()
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(person, person2);
    }

    @Test
    public void testFindById() {
        Person person = createPerson("John", "Doe", "USA");
        createPerson("Mike", "Foe", "Canada");
        assertThat(personDAO.findById(person.getId()))
                .isNotSameAs(person)
                .usingRecursiveComparison()
                .isEqualTo(person);
    }

    @Test
    public void testFindByName() {
        Person person = createPerson("John", "Doe", "USA");
        createPerson("Mike", "Foe", "Canada");
        assertThat(personDAO.findByName(person.getName()))
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(person);
    }

    @Test
    public void testUpdate() {
        Person person = createPerson("John", "Doe", "USA");
        person.setNationality("Canada");
        personDAO.update(person);
        assertThat(this.findById(person.getId()))
                .isNotSameAs(person)
                .usingRecursiveComparison()
                .isEqualTo(person);
    }

    @Test
    public void testRemove() {
        Person person = createPerson("John", "Doe", "USA");
        Person person2 = createPerson("Mike", "Foe", "Canada");
        personDAO.remove(person2);
        List<Person> persons = this.findAll();
        assertThat(persons)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(person);
    }
}
