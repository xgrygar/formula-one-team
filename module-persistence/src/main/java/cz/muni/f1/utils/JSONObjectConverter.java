package cz.muni.f1.utils;

import org.json.JSONObject;

import javax.persistence.AttributeConverter;

/**
 * Convertor for DB storage of JSONObject.
 *
 * @author  Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
public class JSONObjectConverter implements AttributeConverter<JSONObject, String> {
    @Override
    public String convertToDatabaseColumn(JSONObject attribute) {
        return attribute.toString();
    }

    @Override
    public JSONObject convertToEntityAttribute(String dbData) {
        return new JSONObject(dbData);
    }
}
