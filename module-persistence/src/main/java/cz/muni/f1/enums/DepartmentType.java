package cz.muni.f1.enums;

/**
 * Enum used for SubDepartment entity.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
public enum DepartmentType {
    EngineDepartment,
    SuspensionDepartment,
    AeroDynamicsDepartment
}
