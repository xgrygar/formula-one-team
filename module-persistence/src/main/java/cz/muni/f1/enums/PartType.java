package cz.muni.f1.enums;

/**
 * Enum used for CarPart entity.
 *
 * @author  Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
public enum PartType {
    ENGINE,
    SUSPENSION,
    REAR_WING,
    FRONT_WING
}
