package cz.muni.f1.enums;


/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
public enum UserType {
    MANAGER,
    ENGINEER
}
