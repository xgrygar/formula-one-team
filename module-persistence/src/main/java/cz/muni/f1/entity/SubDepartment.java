package cz.muni.f1.entity;

import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.PartType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Entity for SubDepartment.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Entity
@Getter
@Setter
public class SubDepartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "FK_ENGINEER")
    private Set<User> engineers = new HashSet<>();

    @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "FK_CARPART")
    private Set<CarPart> carParts = new HashSet<>();

    @NotNull
    private DepartmentType departmentType;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    CarPart getCarPart(String partName, PartType partType) {
        for (var carPart : carParts) {
            if (carPart.getPartName().equals(partName) && carPart.getPartType() == partType) {
                return carPart;
            }
        }
        return null;
    }

    Set<CarPart> getAllCarParts(PartType partType) {
        // Don't know equivalent of LINQ in Java sry
        var result = new HashSet<CarPart>();
        for (var carPart : carParts) {
            if (carPart.getPartType() == partType) {
                result.add(carPart);
            }
        }
        return result;
    }

    Set<CarPart> getAllCarParts() {
        return carParts;
    }

    public boolean addCarPart(CarPart carPart) {
        return carParts.add(carPart);
    }

    public boolean addEngineer(User engineer) {
        return engineers.add(engineer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubDepartment)) return false;
        SubDepartment that = (SubDepartment) o;
        return Objects.equals(getEngineers(), that.getEngineers()) && getDepartmentType() == that.getDepartmentType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getEngineers(), getDepartmentType());
    }
}
