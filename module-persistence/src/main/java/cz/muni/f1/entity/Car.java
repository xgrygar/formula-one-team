package cz.muni.f1.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

/**
 * Entity for Car.
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@Entity
@Getter
@Setter
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Driver driver;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private CarPart engine;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private CarPart suspension;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private CarPart frontWing;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private CarPart rearWing;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (!Objects.equals(id, car.id)) return false;
        if (!driver.equals(car.driver)) return false;
        if (!engine.equals(car.engine)) return false;
        if (!suspension.equals(car.suspension)) return false;
        if (!frontWing.equals(car.frontWing)) return false;
        return rearWing.equals(car.rearWing);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        if (driver != null) {
            result = 31 * result + driver.hashCode();
        }
        if (engine != null) {
            result = 31 * result + engine.hashCode();
        }
        if (suspension != null) {
            result = 31 * result + suspension.hashCode();
        }
        if (frontWing != null) {
            result = 31 * result + frontWing.hashCode();
        }
        if (rearWing != null) {
            result = 31 * result + rearWing.hashCode();
        }
        return result;
    }
}
