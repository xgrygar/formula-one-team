package cz.muni.f1.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Base Team Entity
 *
 * @author  Adam Grygar <485469@mail.muni.cz>
 * @project Formula One Team
 */
@Entity
@Getter
@Setter
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(name = "Manager")
    private User manager;

    @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "FK_DRIVER")
    private Set<Driver> drivers = new HashSet<>();

    @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "FK_CAR")
    private Set<Car> cars = new HashSet<>();

    @OneToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(name = "FK_ENG_DEPT")
    private EngineeringDepartment engineeringDepartment;

    public boolean addDriver(Driver driver) {
        return drivers.add(driver);
    }

    public boolean addCar(Car car) {
        return cars.add(car);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return getDrivers().equals(team.getDrivers())
               && getManager().equals(team.getManager());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDrivers(), getManager());
    }
}
