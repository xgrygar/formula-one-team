package cz.muni.f1.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Base Driver Entity
 *
 * @author Ján Popeláš <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@Entity
@Getter
@Setter
public class Driver extends Person {

    @NotNull
    private int racePerformance = 0;

    @NotNull
    private int wetPerformance = 0;

    @NotNull
    private int agresiveness = 0;

    @NotNull
    private int racePace = 0;

    @NotNull
    private int qualificationPace = 0;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (!(o instanceof Driver)) return false;

        Driver driver = (Driver) o;

        if (racePerformance != driver.racePerformance) return false;
        if (wetPerformance != driver.wetPerformance) return false;
        if (agresiveness != driver.agresiveness) return false;
        if (racePace != driver.racePace) return false;
        return qualificationPace == driver.qualificationPace;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + racePerformance;
        result = 31 * result + wetPerformance;
        result = 31 * result + agresiveness;
        result = 31 * result + racePace;
        result = 31 * result + qualificationPace;
        return result;
    }
}
