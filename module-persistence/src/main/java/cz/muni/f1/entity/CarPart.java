package cz.muni.f1.entity;

import cz.muni.f1.enums.PartType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Entity for CarPart.
 *
 * @author  Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@Entity
@Getter
@Setter
public class CarPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String partName;

    private String partInfo;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PartType partType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarPart)) return false;

        CarPart carPart = (CarPart) o;
        if (!Objects.equals(partName, carPart.partName)) return false;
        if (!Objects.equals(id, carPart.id)) return false;
        return partType == carPart.partType;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + partInfo.hashCode();
        result = 31 * result + partType.hashCode();
        return result;
    }
}
