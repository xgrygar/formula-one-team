package cz.muni.f1.entity;

import cz.muni.f1.enums.PartType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Entity
@Getter
@Setter
public class EngineeringDepartment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Set<User> engineers = new HashSet<>();

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private SubDepartment suspensionDepartment;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private SubDepartment aeroDepartment;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private SubDepartment engineDepartment;

    CarPart getCarPart(String partName, PartType partType) {
        switch (partType) {
            case ENGINE:
                return engineDepartment.getCarPart(partName, partType);
            case FRONT_WING:
            case REAR_WING:
                return aeroDepartment.getCarPart(partName, partType);
            case SUSPENSION:
                return suspensionDepartment.getCarPart(partName, partType);
            default:
                return null;
        }
    }

    Set<CarPart> getAllCarParts(PartType partType) {
        switch (partType) {
            case ENGINE:
                return engineDepartment.getAllCarParts(partType);
            case FRONT_WING:
            case REAR_WING:
                return aeroDepartment.getAllCarParts(partType);
            case SUSPENSION:
                return suspensionDepartment.getAllCarParts(partType);
            default:
                return null;
        }
    }

    Set<CarPart> getAllCarParts() {
        var allCarParts = new HashSet<CarPart>();
        allCarParts.addAll(engineDepartment.getAllCarParts());
        allCarParts.addAll(aeroDepartment.getAllCarParts());
        allCarParts.addAll(suspensionDepartment.getAllCarParts());
        return allCarParts;
    }

    public void addEngineer(User engineer) {
        engineers.add(engineer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EngineeringDepartment)) return false;
        EngineeringDepartment that = (EngineeringDepartment) o;
        return Objects.equals(engineers, that.engineers)
                && Objects.equals(suspensionDepartment, that.suspensionDepartment)
                && Objects.equals(aeroDepartment, that.aeroDepartment)
                && Objects.equals(engineDepartment, that.engineDepartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, engineers, suspensionDepartment, aeroDepartment, engineDepartment);
    }
}
