package cz.muni.f1.dao;


import cz.muni.f1.entity.SubDepartment;

import java.util.List;

/**
 * Basic interface for SubDepartmentDAO.
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
public interface SubDepartmentDAO {

    /**
     * create sub dept
     */
    void create(SubDepartment subDepartment);

    /**
     * find all sub depts
     */
    List<SubDepartment> findAll();

    /**
     * find one sub dept
     */
    SubDepartment findById(Long id);

    /**
     * update sub dept
     */
    void update(SubDepartment subDepartment);

    /**
     * remove subdept
     */
    void remove(SubDepartment subDepartment);

}
