package cz.muni.f1.dao;

import cz.muni.f1.entity.Car;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Implementation of CarDAO.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Repository
@Transactional
public class CarDaoImpl implements CarDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Car can't be null");
        }

        em.persist(car);
    }

    @Override
    public List<Car> findAll() {
        return em.createQuery("select c from Car c", Car.class).getResultList();
    }

    @Override
    public Car findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can't be null");
        }
        var result = em.find(Car.class, id);

        if (result == null) {
            throw new DataAccessException("Car could not be found for ID " + id, 1);
        }

        return result;
    }

    @Override
    public void update(Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Car can't be null");
        }

        if (car.getId() == null) {
            throw new DataAccessException("Can't update car that has no ID", 1);
        }
        em.merge(car);
    }

    @Override
    public void remove(Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Car can't be null");
        }

        if (car.getId() == null) {
            throw new DataAccessException("Can't update car that has no ID", 1);
        }
        em.remove(em.contains(car) ? car : em.merge(car));
    }
}
