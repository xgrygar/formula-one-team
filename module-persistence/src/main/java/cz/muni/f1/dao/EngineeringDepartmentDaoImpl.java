package cz.muni.f1.dao;

import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Implementation of EngineeringDepartmentDAO.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Repository
@Transactional
public class EngineeringDepartmentDaoImpl implements EngineeringDepartmentDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(EngineeringDepartment engineeringDepartment) {
        if (engineeringDepartment == null) {
            throw new IllegalArgumentException("EngineeringDepartment can't be null");
        }

        em.persist(engineeringDepartment);
    }

    @Override
    public List<EngineeringDepartment> findAll() {
        return em.createQuery("select e from EngineeringDepartment e", EngineeringDepartment.class).getResultList();
    }

    @Override
    public EngineeringDepartment findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID can't be null");
        }

        return em.find(EngineeringDepartment.class, id);
    }

    @Override
    public void update(EngineeringDepartment engineeringDepartment) {
        if (engineeringDepartment == null) {
            throw new IllegalArgumentException("EngineeringDepartment can't be null");
        }

        if (engineeringDepartment.getId() == null) {
            throw new DataAccessException("Can't update EngineeringDepartment without ID", 1);
        }

        em.merge(engineeringDepartment);
    }

    @Override
    public void remove(EngineeringDepartment engineeringDepartment) {
        if (engineeringDepartment == null) {
            throw new IllegalArgumentException("EngineeringDepartment can't be null");
        }

        if (engineeringDepartment.getId() == null) {
            throw new DataAccessException("Can't remove EngineeringDepartment without ID", 1);
        }

        em.remove(em.contains(engineeringDepartment) ? engineeringDepartment : em.merge(engineeringDepartment));
    }
}
