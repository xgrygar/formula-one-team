package cz.muni.f1.dao;

import cz.muni.f1.entity.Person;

import java.util.List;

/**
 * Basic interface for PersonDAO.
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
public interface PersonDAO {
    /**
     * create person
     */
    void create(Person p);

    /**
     * find all persons
     */
    List<Person> findAll();

    /**
     * find one person
     */
    Person findById(Long id);

    /**
     * find one person by name
     */
    List<Person> findByName(String name);

    /**
     * update person
     */
    void update(Person p);

    /**
     * remove person
     */
    void remove(Person p);

}
