package cz.muni.f1.dao;

import cz.muni.f1.entity.Driver;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Base implementation of DriverDAO interface.
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@Repository
@Transactional
public class DriverDaoImpl implements DriverDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(Driver driver) {
        if (driver == null) {
            throw new IllegalArgumentException("Driver can't be null");
        }

        em.persist(driver);
    }

    @Override
    public List<Driver> findAll() {
        return em.createQuery("select d from Driver d", Driver.class).getResultList();
    }

    @Override
    public Driver findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can't be null");
        }

        return em.find(Driver.class, id);
    }

    @Override
    public List<Driver> findByName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can't be null");
        }

        return em
                .createQuery("select d from Driver d WHERE d.name = :name", Driver.class)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public void update(Driver driver) {
        if (driver == null) {
            throw new IllegalArgumentException("Driver can't be null");
        }

        if (driver.getId() == null) {
            throw new DataAccessException("Can't update driver without ID", 1);
        }

        em.merge(driver);
    }

    @Override
    public void remove(Driver driver) {
        if (driver == null) {
            throw new IllegalArgumentException("Driver can't be null");
        }

        if (driver.getId() == null) {
            throw new DataAccessException("Can't remove driver without ID", 1);
        }

        em.remove(em.contains(driver) ? driver : em.merge(driver));
    }
}
