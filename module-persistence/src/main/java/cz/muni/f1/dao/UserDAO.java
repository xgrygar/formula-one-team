package cz.muni.f1.dao;

import cz.muni.f1.entity.User;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface UserDAO {
    /**
     * create user
     */
    void create(User u);

    /**
     * find all users
     */
    List<User> findAll();

    /**
     * find one user
     */
    User findById(Long id);

    /**
     * find one user by name
     */
    List<User> findByName(String name);

    /**
     * update user
     */
    void update(User u);

    /**
     * remove user
     */
    void remove(User u);

}
