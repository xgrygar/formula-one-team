package cz.muni.f1.dao;

import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Implementation of SubDepartmentDAO.
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Repository
@Transactional
public class SubDepartmentDaoImpl implements SubDepartmentDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(SubDepartment subDepartment) {
        if (subDepartment == null) {
            throw new IllegalArgumentException("SubDepartment can't be null");
        }

        em.persist(subDepartment);
    }

    @Override
    public List<SubDepartment> findAll() {
        return em.createQuery("select s from SubDepartment s", SubDepartment.class).getResultList();
    }

    @Override
    public SubDepartment findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID can't be null");
        }

        return em.find(SubDepartment.class, id);
    }

    @Override
    public void update(SubDepartment subDepartment) {
        if (subDepartment == null) {
            throw new IllegalArgumentException("SubDepartment can't be null");
        }

        if (subDepartment.getId() == null) {
            throw new DataAccessException("Can't update SubDepartment with ID null", 1);
        }

        em.merge(subDepartment);
    }

    @Override
    public void remove(SubDepartment subDepartment) {
        if (subDepartment == null) {
            throw new IllegalArgumentException("SubDepartment can't be null");
        }

        if (subDepartment.getId() == null) {
            throw new DataAccessException("Can't remove SubDepartment with ID null", 1);
        }

        em.remove(em.contains(subDepartment) ? subDepartment : em.merge(subDepartment));
    }

}
