package cz.muni.f1.dao;

import cz.muni.f1.entity.CarPart;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Implementation of CarPartDAO.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Repository
@Transactional
public class CarPartDaoImpl implements CarPartDAO{

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(CarPart carPart) {
        if (carPart == null) {
            throw new IllegalArgumentException("CarPart can't be null");
        }

        em.persist(carPart);
    }

    @Override
    public List<CarPart> findAll() {
        return em.createQuery("select d from CarPart d", CarPart.class).getResultList();
    }

    @Override
    public CarPart findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can't be null");
        }

        return em.find(CarPart.class, id);
    }

    @Override
    public List<CarPart> findByName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can't be null");
        }

        return em
                .createQuery("select d from CarPart d WHERE d.partName = :name", CarPart.class)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public void update(CarPart carPart) {
        if (carPart == null) {
            throw new IllegalArgumentException("CarPart can't be null");
        }

        if (carPart.getId() == null) {
            throw new DataAccessException("Can't update CarPart without ID", 1);
        }

        em.merge(carPart);
    }

    @Override
    public void remove(CarPart carPart) {
        if (carPart == null) {
            throw new IllegalArgumentException("CarPart can't be null");
        }

        if (carPart.getId() == null) {
            throw new DataAccessException("Can't remove CarPart without ID", 1);
        }

        em.remove(em.contains(carPart) ? carPart : em.merge(carPart));
    }
}
