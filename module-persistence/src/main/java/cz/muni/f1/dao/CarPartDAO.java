package cz.muni.f1.dao;

import cz.muni.f1.entity.CarPart;

import java.util.List;

/**
 * Basic interface of CarPartDAO.
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
public interface CarPartDAO {
    /**
     * creates carpart
     */
    void create(CarPart carPart);

    /**
     * fnds all carparts
     */
    List<CarPart> findAll();

    /**
     * find specific carpart
     */
    CarPart findById(Long id);

    /**
     * finds carpart by name
     */
    List<CarPart> findByName(String name);

    /**
     * update carpart
     */
    void update(CarPart carPart);

    /**
     * removes carpart
     */
    void remove(CarPart carPart);
}
