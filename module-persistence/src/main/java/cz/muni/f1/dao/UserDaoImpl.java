package cz.muni.f1.dao;

import cz.muni.f1.entity.User;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Base implementation of UserDAO.
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(User u) {
        if (u == null) {
            throw new IllegalArgumentException("User can't be null");
        }

        em.persist(u);
    }

    @Override
    public List<User> findAll() {
        return em.createQuery("select u from User u", User.class).getResultList();
    }

    @Override
    public User findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID can't be null");
        }

        return em.find(User.class, id);
    }

    @Override
    public List<User> findByName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can't be null");
        }

        return em.createQuery("select u from User u where name = :param", User.class).setParameter("param", name).getResultList();
    }

    @Override
    public void update(User u) {
        if (u == null) {
            throw new IllegalArgumentException("User can't be null");
        }

        if (u.getId() == null) {
            throw new DataAccessException("Can't update User without ID", 1);
        }

        em.merge(u);
    }

    @Override
    public void remove(User u) {
        if (u == null) {
            throw new IllegalArgumentException("User can't be null");
        }

        if (u.getId() == null) {
            throw new DataAccessException("Can't remove User without ID", 1);
        }

        em.remove(em.contains(u) ? u : em.merge(u));
    }
}
