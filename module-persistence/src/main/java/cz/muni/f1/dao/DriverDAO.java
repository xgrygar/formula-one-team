package cz.muni.f1.dao;

import cz.muni.f1.entity.Driver;

import java.util.List;

/**
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
public interface DriverDAO {
    /**
     * creates driver
     */
    void create(Driver driver);

    /**
     * finds all drivers
     */
    List<Driver> findAll();

    /**
     * finds one driver
     */
    Driver findById(Long id);

    /**
     * finds one driver by name
     */
    List<Driver> findByName(String name);

    /**
     * updates driver
     */
    void update(Driver driver);

    /**
     * removes driver
     */
    void remove(Driver driver);
}
