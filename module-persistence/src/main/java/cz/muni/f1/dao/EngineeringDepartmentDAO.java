package cz.muni.f1.dao;


import cz.muni.f1.entity.EngineeringDepartment;

import java.util.List;

/**
 * Basic interface for EngineeringDepartmentDAO.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
public interface EngineeringDepartmentDAO {
    /**
     * creates eng dept.
     */
    void create(EngineeringDepartment engineeringDepartment);

    /**
     * find all eng. depts
     */
    List<EngineeringDepartment> findAll();

    /**
     * find one eng. dept.
     */
    EngineeringDepartment findById(Long id);

    /**
     * update eng dept
     */
    void update(EngineeringDepartment engineeringDepartment);

    /**
     * remove eng dept
     */
    void remove(EngineeringDepartment engineeringDepartment);

}
