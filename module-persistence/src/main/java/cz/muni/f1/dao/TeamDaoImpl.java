package cz.muni.f1.dao;

import cz.muni.f1.entity.Team;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Implementation of TeamDAO.
 *
 * @author  Adam Grygar <485469@mail.muni.cz>
 * @project Formula One Team
 */
@Transactional
@Repository
public class TeamDaoImpl implements TeamDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(Team team) {
        if (team == null) {
            throw new IllegalArgumentException("Team can't be null");
        }

        em.persist(team);
    }

    @Override
    public List<Team> findAll() {
        return em.createQuery("select t from Team t", Team.class).getResultList();
    }

    @Override
    public Team findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID can't be null");
        }

        return em.find(Team.class, id);
    }

    @Override
    public void update(Team team) {
        if (team == null) {
            throw new IllegalArgumentException("Team can't be null");
        }

        if (team.getId() == null) {
            throw new DataAccessException("Can't update Team without ID", 1);
        }

        em.merge(team);
    }

    @Override
    public void remove(Team team) {
        if (team == null) {
            throw new IllegalArgumentException("Team can't be null");
        }

        if (team.getId() == null) {
            throw new DataAccessException("Can't remove Team without ID", 1);
        }

        em.remove(em.contains(team) ? team : em.merge(team));
    }
}