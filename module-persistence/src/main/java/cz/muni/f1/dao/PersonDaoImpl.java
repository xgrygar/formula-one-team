package cz.muni.f1.dao;

import cz.muni.f1.entity.Person;
import cz.muni.f1.exception.DataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Base implementation of PersonDAO.
 *
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@Transactional
@Repository
public class PersonDaoImpl implements PersonDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(Person p) {
        if (p == null) {
            throw new IllegalArgumentException("Person can't be null");
        }

        em.persist(p);
    }

    @Override
    public List<Person> findAll() {
        return em.createQuery("select p from Person p", Person.class).getResultList();
    }

    @Override
    public Person findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID can't be null");
        }

        return em.find(Person.class, id);
    }

    @Override
    public List<Person> findByName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can't be null");
        }

        return em.createQuery("select p from Person p where name = :param", Person.class).setParameter("param", name).getResultList();
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            throw new IllegalArgumentException("Person can't be null");
        }

        if (p.getId() == null) {
            throw new DataAccessException("Can't update Person without ID", 1);
        }

        em.merge(p);
    }

    @Override
    public void remove(Person p) {
        if (p == null) {
            throw new IllegalArgumentException("Person can't be null");
        }

        if (p.getId() == null) {
            throw new DataAccessException("Can't remove Person without ID", 1);
        }

        em.remove(em.contains(p) ? p : em.merge(p));
    }
}


