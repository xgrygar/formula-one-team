package cz.muni.f1.dao;

import cz.muni.f1.entity.Team;

import java.util.List;

/**
 * Basic interface for TeamDAO.
 *
 * @author Adam Grygar <485469@mail.muni.cz>
 * @project Formula One Team
 */
public interface TeamDAO {
    /**
     * create team
     */
    void create(Team team);

    /**
     * find all teams
     */
    List<Team> findAll();

    /**
     * find one team
     */
    Team findById(Long id);

    /**
     * update team
     */
    void update(Team team);

    /**
     * remove team
     */
    void remove(Team team);
}
