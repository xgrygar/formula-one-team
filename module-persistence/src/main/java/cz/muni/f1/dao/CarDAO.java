package cz.muni.f1.dao;


import cz.muni.f1.entity.Car;

import java.util.List;

/**
 * Basic interface for CarDAO.
 *
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
public interface CarDAO {
    /**
     * creates car
     */
    void create(Car car);

    /**
     * finds all cars
     */
    List<Car> findAll();

    /**
     * finds specific car
     */
    Car findById(Long id);

    /**
     * updates a car
     */
    void update(Car car);

    /**
     * removes car
     */
    void remove(Car car);
}
