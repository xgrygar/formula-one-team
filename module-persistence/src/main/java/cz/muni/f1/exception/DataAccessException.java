package cz.muni.f1.exception;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception for unavailable resource.
 *
 * @author  Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "The required resource was not found")
public class DataAccessException extends EmptyResultDataAccessException {

    public DataAccessException(String msg, int expectedSize) {
        super(msg, expectedSize);
    }

    public DataAccessException(int expectedSize) {
        super(expectedSize);
    }
}
