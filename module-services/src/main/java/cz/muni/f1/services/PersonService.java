package cz.muni.f1.services;

import cz.muni.f1.entity.Person;

import java.util.List;

/**
 * Person Service interface for Person entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface PersonService {
    /**
     * create persom
     */
    void create(Person p);

    /**
     * find all persons
     */
    List<Person> findAll();

    /**
     * find one person
     */
    Person findById(Long id);

    /**
     * find one person by name
     */
    List<Person> findByName(String name);

    /**
     * update person
     */
    void update(Person p);

    /**
     * remove person
     */
    void remove(Person p);
}
