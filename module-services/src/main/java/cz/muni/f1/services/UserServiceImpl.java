package cz.muni.f1.services;

import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDAO userDao;

    @Override
    public void create(User user) {
        userDao.create(user);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findById(Long id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void remove(User user) {
        userDao.remove(user);
    }

    @Override
    public List<User> findAllManagers() {
        var users = userDao.findAll();
        return users.stream().filter(x -> x.getRole().equals(UserType.MANAGER)).collect(Collectors.toList());
    }

    @Override
    public List<User> findAllEngineers() {
        var users = userDao.findAll();
        return users.stream().filter(x -> x.getRole().equals(UserType.ENGINEER)).collect(Collectors.toList());
    }
}
