package cz.muni.f1.services;

import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.entity.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class DriverServiceImpl implements DriverService {
    @Autowired
    private DriverDAO driverDao;

    @Override
    public void create(Driver driver) {
        driverDao.create(driver);
    }

    @Override
    public List<Driver> findAll() {
        return driverDao.findAll();
    }

    @Override
    public Driver findById(Long id) {
        return driverDao.findById(id);
    }

    @Override
    public List<Driver> findByName(String name) {
        return driverDao.findByName(name);
    }

    @Override
    public void update(Driver driver) {
        driverDao.update(driver);
    }

    @Override
    public void remove(Driver driver) {
        driverDao.remove(driver);
    }
}
