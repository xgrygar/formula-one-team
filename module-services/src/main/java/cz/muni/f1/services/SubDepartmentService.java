package cz.muni.f1.services;

import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;

import java.util.List;

/**
 * SubDepartment Service interface for SubDepartment entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface SubDepartmentService {
    /**
     * create subdept
     */
    void create(SubDepartment subDepartment);

    /**
     * find all sub depts
     */
    List<SubDepartment> findAll();

    /**
     * find one sub dept
     */
    SubDepartment findById(Long id);

    /**
     * update subdept
     */
    void update(SubDepartment subDepartment);

    /**
     * remove subdept
     */
    void remove(SubDepartment subDepartment);

    /**
     * add engineer to subdept
     */
    void addEngineeer(Long SubDepartmentID, User engineer);

    /**
     * remoce engineer from subdept
     */
    void removeEngineer(Long SubDepartmentID, User engineer);

    /**
     * add car part do sub dept
     */
    void addCarPart(Long SubDepartmentID, CarPart carPart);

}

