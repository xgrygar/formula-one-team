package cz.muni.f1.services;

import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class SubDepartmentServiceImpl implements SubDepartmentService {

    @Autowired
    private SubDepartmentDAO subDepartmentDao;

    @Autowired
    private UserDAO userDao;

    @Autowired
    private CarPartDAO carPartDao;

    @Override
    public void create(SubDepartment subDepartment) {
        for (User engineer : subDepartment.getEngineers()) {
            userDao.create(engineer);
        }
        for (CarPart carPart : subDepartment.getCarParts()) {
            carPartDao.create(carPart);
        }
        subDepartmentDao.create(subDepartment);
    }

    @Override
    public List<SubDepartment> findAll() {
        return subDepartmentDao.findAll();
    }

    @Override
    public SubDepartment findById(Long id) {
        return subDepartmentDao.findById(id);
    }

    @Override
    public void update(SubDepartment subDepartment) {
        subDepartmentDao.update(subDepartment);
    }

    @Override
    public void remove(SubDepartment subDepartment) {
        subDepartmentDao.remove(subDepartment);
    }

    @Override
    public void addEngineeer(Long subDepartmentID, User engineer) {
        SubDepartment subDepartment = subDepartmentDao.findById(subDepartmentID);
        var engineers = subDepartment.getEngineers();
        engineers.add(engineer);
        subDepartment.setEngineers(engineers);
        subDepartmentDao.update(subDepartment);
    }

    @Override
    public void removeEngineer(Long subDepartmentID, User engineer) {
        SubDepartment subDepartment = subDepartmentDao.findById(subDepartmentID);
        var engineers = subDepartment.getEngineers();
        engineers.remove(engineer);
        subDepartment.setEngineers(engineers);
        subDepartmentDao.update(subDepartment);
    }

    @Override
    public void addCarPart(Long subDepartmentID, CarPart carPart) {
        SubDepartment subDepartment = subDepartmentDao.findById(subDepartmentID);
        var carParts = subDepartment.getCarParts();
        carParts.add(carPart);
        subDepartment.setCarParts(carParts);
        subDepartmentDao.update(subDepartment);
    }
}
