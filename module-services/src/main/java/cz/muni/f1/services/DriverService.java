package cz.muni.f1.services;

import cz.muni.f1.entity.Driver;

import java.util.List;

/**
 * Driver Service interface for Driver entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface DriverService {
    /**
     * create driver
     */
    void create(Driver driver);

    /**
     * find all drivers
     */
    List<Driver> findAll();

    /**
     * find one driver
     */
    Driver findById(Long id);

    /**
     * find one driver by name
     */
    List<Driver> findByName(String name);

    /**
     * update driver
     */
    void update(Driver driver);

    /**
     * remove driver
     */
    void remove(Driver driver);
}
