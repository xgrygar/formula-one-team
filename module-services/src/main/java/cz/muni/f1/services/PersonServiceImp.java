package cz.muni.f1.services;

import cz.muni.f1.dao.PersonDAO;
import cz.muni.f1.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class PersonServiceImp implements PersonService {
    @Autowired
    private PersonDAO personDao;

    @Override
    public void create(Person person) {
        personDao.create(person);
    }

    @Override
    public List<Person> findAll() {
        return personDao.findAll();
    }

    @Override
    public Person findById(Long id) {
        return personDao.findById(id);
    }

    @Override
    public List<Person> findByName(String name) {
        return personDao.findByName(name);
    }

    @Override
    public void update(Person person) {
        personDao.update(person);
    }

    @Override
    public void remove(Person person) {
        personDao.remove(person);
    }
}
