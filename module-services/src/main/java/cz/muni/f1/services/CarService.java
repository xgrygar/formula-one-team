package cz.muni.f1.services;

import cz.muni.f1.entity.Car;

import java.util.List;

/**
 * Car Service interface for Car entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface CarService {
    /**
     * create car
     */
    void create(Car car);

    /**
     * find all cars
     */
    List<Car> findAll();

    /**
     * find one car
     */
    Car findById(Long id);

    /**
     * update car
     */
    void update(Car car);

    /**
     * remove car
     */
    void remove(Car car);

    /**
     * change driver of car
     */
    void changeDriver(Long driverID, Long carID);

    /**
     * change engine of a car
     */
    void changeEngine(Long engineID, Long carID);

    /**
     * change front wing of a car
     */
    void changeFrontWing(Long frontWingID, Long carID);

    /**
     * change rear wing of a car
     */
    void changeRearWing(Long rearWingID, Long carID);

    /**
     * change suspension of a car
     */
    void changeSuspension(Long suspensionID, Long carID);


}

