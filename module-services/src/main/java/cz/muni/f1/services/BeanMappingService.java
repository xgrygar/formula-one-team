package cz.muni.f1.services;

import org.dozer.Mapper;

import java.util.Collection;
import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface BeanMappingService {
    /**
     * maps collection of objects to object to class
     *
     * @param objects    collection of objects
     * @param mapToClass class to be mapped to
     * @param <T>        type
     * @return collection of objects mapped to collection of class
     */

    <T> List<T> mapTo(Collection<?> objects, Class<T> mapToClass);

    /**
     * maps single object to class
     *
     * @param u          object
     * @param mapToClass class to be mapped to
     * @param <T>        type
     * @return object mapped to class
     */
    <T> T mapTo(Object u, Class<T> mapToClass);

    /**
     * gets mapper
     *
     * @return mapper
     */
    Mapper getMapper();
}