package cz.muni.f1.services;

import cz.muni.f1.dao.CarDAO;
import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarDAO carDao;

    @Autowired
    private CarPartDAO carPartDao;

    @Autowired
    private DriverDAO driverDao;


    @Override
    public void create(Car car) {
        driverDao.create(car.getDriver());
        carPartDao.create(car.getEngine());
        carPartDao.create(car.getFrontWing());
        carPartDao.create(car.getRearWing());
        carPartDao.create(car.getSuspension());
        carDao.create(car);
    }

    @Override
    public List<Car> findAll() {
        return carDao.findAll();
    }

    @Override
    public Car findById(Long id) {
        return carDao.findById(id);
    }

    @Override
    public void update(Car car) {
        carDao.update(car);
    }

    @Override
    public void remove(Car car) {
        carDao.remove(car);
    }

    @Override
    public void changeDriver(Long driverID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null!");
        }
        Car car = carDao.findById(carID);
        if (driverID == null) {
            car.setDriver(null);
        } else {
            Driver driver = driverDao.findById(driverID);
            car.setDriver(driver);
        }
        carDao.update(car);
    }

    @Override
    public void changeEngine(Long engineID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null!");
        }
        Car car = carDao.findById(carID);
        if (engineID == null) {
            car.setEngine(null);
        } else {
            CarPart engine = carPartDao.findById(engineID);
            car.setEngine(engine);
        }
        carDao.update(car);
    }

    @Override
    public void changeFrontWing(Long frontWingID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null!");
        }
        Car car = carDao.findById(carID);
        if (frontWingID == null) {
            car.setFrontWing(null);
        } else {
            CarPart frontWing = carPartDao.findById(frontWingID);
            car.setFrontWing(frontWing);
        }
        carDao.update(car);
    }

    @Override
    public void changeRearWing(Long rearWingID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null!");
        }
        Car car = carDao.findById(carID);
        if (rearWingID == null) {
            car.setRearWing(null);
        } else {
            CarPart rearWing = carPartDao.findById(rearWingID);
            car.setRearWing(rearWing);
        }
        carDao.update(car);
    }

    @Override
    public void changeSuspension(Long suspensionID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null!");
        }
        Car car = carDao.findById(carID);
        if (suspensionID == null) {
            car.setSuspension(null);
        } else {
            CarPart suspension = carPartDao.findById(suspensionID);
            car.setSuspension(suspension);
        }
        carDao.update(car);
    }
}
