package cz.muni.f1.services;

import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.Team;

import java.util.List;

/**
 * Team Service interface for Team entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface TeamService {
    /**
     * create team
     */
    void create(Team team);

    /**
     * find all teams
     */
    List<Team> findAll();

    /**
     * find one team
     */
    Team findById(Long id);

    /**
     * uddate team
     */
    void update(Team team);

    /**
     * remove team
     */
    void remove(Team team);

    /**
     * add car to team
     */
    void addCar(Team team, Car car);

    /**
     * remove car from team
     */
    void removeCar(Team team, Car car);

    /**
     * add driver to team
     */
    void addDriver(Team team, Driver driver);

    /**
     * remove driver from team
     */
    void removeDriver(Team team, Driver driver);
}
