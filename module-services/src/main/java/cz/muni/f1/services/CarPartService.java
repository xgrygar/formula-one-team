package cz.muni.f1.services;

import cz.muni.f1.entity.CarPart;

import java.util.List;

/**
 * CarPart Service interface for CarPart entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface CarPartService {
    /**
     * creates carpart
     */
    void create(CarPart carPart);

    /**
     * find all carparts
     */
    List<CarPart> findAll();

    /**
     * find one carpart
     */
    CarPart findById(Long id);

    /**
     * find carpars with the same name
     */
    List<CarPart> findByName(String name);

    /**
     * update carpart
     */
    void update(CarPart carPart);

    /**
     * removes carpart
     */
    void remove(CarPart carPart);
}
