package cz.muni.f1.services;

import cz.muni.f1.dao.CarDAO;
import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.dao.TeamDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamDAO teamDao;

    @Autowired
    private CarDAO carDAO;

    @Autowired
    private UserDAO userDao;

    @Autowired
    private DriverDAO driverDao;

    @Override
    public void create(Team team) {
        userDao.create(team.getManager());
        for (Driver driver : team.getDrivers()) {
            driverDao.create(driver);
        }
        teamDao.create(team);
    }

    @Override
    public List<Team> findAll() {
        return teamDao.findAll();
    }

    @Override
    public Team findById(Long id) {
        return teamDao.findById(id);
    }

    @Override
    public void update(Team team) {
        teamDao.update(team);
    }

    @Override
    public void remove(Team team) {
        teamDao.remove(team);
    }

    @Override
    public void addCar(Team team, Car car) {
        if (team == null) {
            throw new IllegalArgumentException("Team is null");
        }
        if (car == null) {
            throw new IllegalArgumentException("Car is null");
        }
        var cars = team.getCars();
        cars.add(car);
        teamDao.update(team);
    }

    @Override
    public void removeCar(Team team, Car car) {
        if (team == null) {
            throw new IllegalArgumentException("Team is null");
        }
        if (car == null) {
            throw new IllegalArgumentException("Car is null");
        }
        var cars = team.getCars();
        cars.remove(car);
        teamDao.update(team);
    }

    @Override
    public void addDriver(Team team, Driver driver) {
        if (team == null) {
            throw new IllegalArgumentException("Team is null");
        }
        if (driver == null) {
            throw new IllegalArgumentException("Driver is null");
        }
        var drivers = team.getDrivers();
        drivers.add(driver);
        teamDao.update(team);
    }

    @Override
    public void removeDriver(Team team, Driver driver) {
        if (team == null) {
            throw new IllegalArgumentException("Team is null");
        }
        if (driver == null) {
            throw new IllegalArgumentException("Driver is null");
        }
        var drivers = team.getDrivers();
        drivers.remove(driver);
        teamDao.update(team);
    }
}
