package cz.muni.f1.services;

import cz.muni.f1.entity.User;

import java.util.List;

/**
 * User Service interface for User entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface UserService {
    /**
     * create user
     */
    void create(User user);

    /**
     * find all users
     */
    List<User> findAll();

    /**
     * find one user
     */
    User findById(Long id);

    /**
     * find user by name
     */
    List<User> findByName(String name);

    /**
     * update user
     */
    void update(User user);

    /**
     * remove user
     */
    void remove(User user);

    /**
     * find all managers
     */
    List<User> findAllManagers();

    /**
     * find all engineers
     */
    List<User> findAllEngineers();
}
