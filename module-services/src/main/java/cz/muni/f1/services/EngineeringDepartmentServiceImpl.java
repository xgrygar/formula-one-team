package cz.muni.f1.services;

import cz.muni.f1.dao.EngineeringDepartmentDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class EngineeringDepartmentServiceImpl implements EngineeringDepartmentService {

    @Autowired
    private EngineeringDepartmentDAO engineeringDepartmentDao;
    @Autowired
    private UserDAO userDao;
    @Autowired
    private SubDepartmentDAO subDepartmentDao;

    @Override
    public void create(EngineeringDepartment engineeringDepartment) {
        for (User engineer : engineeringDepartment.getEngineers()) {
            userDao.create(engineer);
        }
        subDepartmentDao.create(engineeringDepartment.getSuspensionDepartment());
        subDepartmentDao.create(engineeringDepartment.getAeroDepartment());
        subDepartmentDao.create(engineeringDepartment.getEngineDepartment());
        engineeringDepartmentDao.create(engineeringDepartment);
    }

    @Override
    public List<EngineeringDepartment> findAll() {
        return engineeringDepartmentDao.findAll();
    }

    @Override
    public EngineeringDepartment findById(Long id) {
        return engineeringDepartmentDao.findById(id);
    }

    @Override
    public void update(EngineeringDepartment engineeringDepartment) {
        engineeringDepartmentDao.update(engineeringDepartment);
    }

    @Override
    public void remove(EngineeringDepartment engineeringDepartment) {
        engineeringDepartmentDao.remove(engineeringDepartment);
    }

    @Override
    public void addEngineer(EngineeringDepartment engineeringDepartment, User engineer) {
        if (engineeringDepartment == null) {
            throw new IllegalArgumentException("Engineering Department is null");
        }
        if (engineer == null) {
            throw new IllegalArgumentException("Engineer Is null");
        }
        var engineers = engineeringDepartment.getEngineers();
        engineers.add(engineer);
        engineeringDepartment.setEngineers(engineers);
        engineeringDepartmentDao.update(engineeringDepartment);
    }

    @Override
    public void removeEngineer(EngineeringDepartment engineeringDepartment, User engineer) {
        var engineers = engineeringDepartment.getEngineers();
        engineers.remove(engineer);
        engineeringDepartment.setEngineers(engineers);
        engineeringDepartmentDao.update(engineeringDepartment);
    }

    @Override
    public void changeSubDepartment(Long DepartmentID, Long subDepartmentID) throws Exception {
        var subDepartment = subDepartmentDao.findById(subDepartmentID);
        var engDepartment = engineeringDepartmentDao.findById(DepartmentID);
        var type = subDepartment.getDepartmentType();
        switch (type) {
            case EngineDepartment: {
                engDepartment.setEngineDepartment(subDepartment);
                engineeringDepartmentDao.update(engDepartment);
                break;
            }
            case AeroDynamicsDepartment: {
                engDepartment.setAeroDepartment(subDepartment);
                engineeringDepartmentDao.update(engDepartment);
                break;
            }
            case SuspensionDepartment: {
                engDepartment.setSuspensionDepartment(subDepartment);
                engineeringDepartmentDao.update(engDepartment);
                break;
            }
            default:
                throw new Exception("subdepartment type does not exist");
        }

    }

}
