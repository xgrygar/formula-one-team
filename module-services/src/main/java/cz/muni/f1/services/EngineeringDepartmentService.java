package cz.muni.f1.services;

import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.User;

import java.util.List;

/**
 * EngineeringDepartmentService Service interface for EngineeringDepartmentService entity
 *
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
public interface EngineeringDepartmentService {
    /**
     * create eng dept.
     */
    void create(cz.muni.f1.entity.EngineeringDepartment engineeringDepartment);

    /**
     * find all eng depts
     */
    List<cz.muni.f1.entity.EngineeringDepartment> findAll();

    /**
     * find one eng dept
     */
    cz.muni.f1.entity.EngineeringDepartment findById(Long id);

    /**
     * update eng dept
     */
    void update(cz.muni.f1.entity.EngineeringDepartment engineeringDepartment);

    /**
     * remove eng dept
     */
    void remove(cz.muni.f1.entity.EngineeringDepartment engineeringDepartment);

    /**
     * add engieer to eng dept
     */
    void addEngineer(EngineeringDepartment engineeringDepartment, User engineer);

    /**
     * remove engineer from eng dept
     */
    void removeEngineer(EngineeringDepartment engineeringDepartment,User engineer);

    /**
     * change subdepartment of eng dept
     */
    void changeSubDepartment(Long DepartmentID, Long subDepartmentID) throws Exception;


}
