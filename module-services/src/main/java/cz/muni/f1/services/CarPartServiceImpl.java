package cz.muni.f1.services;

import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.entity.CarPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Urban <485632@mail.muni.cz>
 * @project Formula One Team
 */
@Service
public class CarPartServiceImpl implements CarPartService {
    @Autowired
    private CarPartDAO carPartDao;

    @Override
    public void create(CarPart carPart) {
        carPartDao.create(carPart);
    }

    @Override
    public List<CarPart> findAll() {
        return carPartDao.findAll();
    }

    @Override
    public CarPart findById(Long id) {
        return carPartDao.findById(id);
    }

    @Override
    public List<CarPart> findByName(String name) {
        return carPartDao.findByName(name);
    }

    @Override
    public void update(CarPart carPart) {
        carPartDao.update(carPart);
    }

    @Override
    public void remove(CarPart carPart) {
        carPartDao.remove(carPart);
    }
}
