package cz.muni.f1.facade;


import cz.muni.f1.dto.CarPart.CarPartCreateDto;
import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.CarPartService;
import cz.muni.f1.services.CarService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Service
@Transactional
public class CarPartFacadeImpl implements CarPartFacade {

    @Autowired
    private CarPartService carPartService;

    @Autowired
    private CarService carService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public List<CarPartDto> findAllCarParts() {
        return beanMappingService.mapTo(carPartService.findAll(), CarPartDto.class);
    }

    @Override
    public CarPartDto getCarPartById(Long id) {
        CarPart carPart = carPartService.findById(id);
        return beanMappingService.mapTo(carPart, CarPartDto.class);
    }

    @Override
    public List<CarPartDto> getAllFreeCarParts() {
        var cars = carService.findAll();
        var allCarParts = carPartService.findAll();
        List<CarPart> assignedCarParts = new ArrayList<>();
        for (var car : cars
        ) {
            assignedCarParts.add(car.getEngine());
            assignedCarParts.add(car.getFrontWing());
            assignedCarParts.add(car.getRearWing());
            assignedCarParts.add(car.getSuspension());
        }
        assignedCarParts = assignedCarParts.stream().filter(Objects::nonNull).collect(Collectors.toList());
        allCarParts.removeAll(assignedCarParts);
        return beanMappingService.mapTo(allCarParts, CarPartDto.class);
    }

    @Override
    public void create(CarPartDto carPartDto) {
        carPartService.create(beanMappingService.mapTo(carPartDto, CarPart.class));
    }

    @Override
    public CarPartDto createRest(CarPartCreateDto carPartDto) {
        CarPart newCarPart = new CarPart();
        BeanUtils.copyProperties(carPartDto, newCarPart);
        carPartService.create(newCarPart);
        return beanMappingService.mapTo(newCarPart, CarPartDto.class);
    }

    @Override
    public void update(CarPartDto carPartDto) {
        carPartService.update(beanMappingService.mapTo(carPartDto, CarPart.class));
    }

    @Override
    public void remove(long id) {
        var carPart = carPartService.findById(id);
        carPartService.remove(carPart);
    }


}
