package cz.muni.f1.facade;

import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentCreateDto;
import cz.muni.f1.dto.SubDepartment.SubDepartmentDto;
import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.SubDepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

/**
 * @author Adam Grygar
 */
@Service
@Transactional
public class SubDepartmentFacadeImpl implements SubDepartmentFacade {

    @Autowired
    private SubDepartmentService subDepartmentService;

    @Autowired
    private BeanMappingService beanMappingService;


    @Override
    public List<SubDepartmentDto> findAllSubDepartments() {
        return beanMappingService.mapTo(subDepartmentService.findAll(), SubDepartmentDto.class);
    }

    @Override
    public SubDepartmentDto getSubDepartmentById(Long id) {
        var subDepartment = subDepartmentService.findById(id);
        return beanMappingService.mapTo(subDepartment, SubDepartmentDto.class);
    }

    @Override
    public void addEngineer(Long subDepartmentID, UserDto engineer) {
        var newEngineer = new User();
        BeanUtils.copyProperties(engineer, newEngineer);
        subDepartmentService.addEngineeer(subDepartmentID, newEngineer);
    }

    @Override
    public void removeEngineer(Long subDepartmentID, UserDto engineer) {
        var newEngineer = new User();
        BeanUtils.copyProperties(engineer, newEngineer);
        subDepartmentService.removeEngineer(subDepartmentID, newEngineer);
    }

    @Override
    public void addCarpart(Long subDepartmentID, CarPartDto carPart) {
        var newCarPart = new CarPart();
        BeanUtils.copyProperties(carPart, newCarPart);
        subDepartmentService.addCarPart(subDepartmentID, newCarPart);
    }

    @Override
    public SubDepartmentDto createSubDepartment(SubDepartmentCreateDto subDepartmentDto) {
        SubDepartment subDepartment = new SubDepartment();
        BeanUtils.copyProperties(subDepartmentDto, subDepartment);
        subDepartmentService.create(subDepartment);
        return beanMappingService.mapTo(subDepartment, SubDepartmentDto.class);
    }

    @Override
    public Boolean deleteSubDepartment(Long id) {
        try {
            SubDepartment subDepartment = subDepartmentService.findById(id);
            subDepartmentService.remove(subDepartment);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public SubDepartmentDto updateSubDepartment(SubDepartmentDto subDepartmentDto) {
        SubDepartment subDepartment = subDepartmentService.findById(subDepartmentDto.getId());
        BeanUtils.copyProperties(subDepartmentDto, subDepartment);
        subDepartment.setDepartmentType(subDepartmentDto.getDepartmentType());
        subDepartment.setEngineers(new HashSet<>(beanMappingService.mapTo(subDepartmentDto.getEngineers(),User.class)));
        subDepartment.setCarParts(new HashSet<>(beanMappingService.mapTo(subDepartmentDto.getCarParts(),CarPart.class)));
        subDepartmentService.update(subDepartment);
        return beanMappingService.mapTo(subDepartment, SubDepartmentDto.class);
    }
}
