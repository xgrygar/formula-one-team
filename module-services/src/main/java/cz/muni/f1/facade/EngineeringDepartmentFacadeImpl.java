package cz.muni.f1.facade;

import cz.muni.f1.dto.EngineeringDepartment.EngineeringDepartmentDto;
import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.EngineeringDepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Service
@Transactional
public class EngineeringDepartmentFacadeImpl implements EngineeringDepartmentFacade {

    @Autowired
    private EngineeringDepartmentService engineeringDepartmentService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public List<EngineeringDepartmentDto> findAllEngineeringDepartments() {
        return beanMappingService.mapTo(engineeringDepartmentService.findAll(), EngineeringDepartmentDto.class);
    }

    @Override
    public EngineeringDepartmentDto getEngineeringDepartmentById(Long id) {
        EngineeringDepartment engineeringDepartment = engineeringDepartmentService.findById(id);
        return beanMappingService.mapTo(engineeringDepartment, EngineeringDepartmentDto.class);
    }

    @Override
    public void addEngineer(EngineeringDepartment engineeringDepartment, UserDto engineer) {
        User user = new User();
        BeanUtils.copyProperties(engineer, user);
        engineeringDepartmentService.addEngineer(engineeringDepartment, user);
    }

    @Override
    public void removeEngineer(EngineeringDepartment engineeringDepartment, UserDto engineer) {
        var user = new User();
        BeanUtils.copyProperties(engineer, new User());
        engineeringDepartmentService.removeEngineer(engineeringDepartment, user);
    }

    @Override
    public void changeSubDepartment(Long DepartmentID, Long subDepartmentID) throws Exception {
        engineeringDepartmentService.changeSubDepartment(DepartmentID, subDepartmentID);
    }

    @Override
    public EngineeringDepartmentDto createEngineeringDepartment(EngineeringDepartmentDto engineeringDepartmentDto) {
        EngineeringDepartment engineeringDepartment = new EngineeringDepartment();
        BeanUtils.copyProperties(engineeringDepartmentDto, engineeringDepartment);
        engineeringDepartmentService.create(engineeringDepartment);
        return beanMappingService.mapTo(engineeringDepartment, EngineeringDepartmentDto.class);
    }

    @Override
    public EngineeringDepartmentDto updateEngineeringDepartment(EngineeringDepartmentDto engineeringDepartmentDto) {
        EngineeringDepartment engineeringDepartment = new EngineeringDepartment();
        BeanUtils.copyProperties(engineeringDepartmentDto, engineeringDepartment);
        engineeringDepartmentService.update(engineeringDepartment);
        return beanMappingService.mapTo(engineeringDepartment, EngineeringDepartmentDto.class);
    }

    @Override
    public Boolean deleteEngineeringDepartment(Long id) {
        try {
            EngineeringDepartment engineeringDepartment = engineeringDepartmentService.findById(id);
            engineeringDepartmentService.remove(engineeringDepartment);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
