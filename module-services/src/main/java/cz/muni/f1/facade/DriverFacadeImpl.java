package cz.muni.f1.facade;

import cz.muni.f1.dto.Driver.DriverCreateDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.DriverService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Service
@Transactional
public class DriverFacadeImpl implements DriverFacade {

    @Autowired
    private DriverService driverService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Autowired
    CarFacade carFacade;

    @Override
    public List<DriverDto> findAllDrivers() {
        return beanMappingService.mapTo(driverService.findAll(), DriverDto.class);
    }

    @Override
    public DriverDto getDriverById(Long id) {
        Driver driver = driverService.findById(id);
        return beanMappingService.mapTo(driver, DriverDto.class);
    }

    @Override
    public DriverDto updateDriver(DriverDto driver) {
        Driver newDriver = new Driver();
        BeanUtils.copyProperties(driver, newDriver);
        driverService.update(newDriver);
        return beanMappingService.mapTo(newDriver, DriverDto.class);
    }

    @Override
    public DriverDto createDriver(DriverDto driver) {
        Driver newDriver = new Driver();
        BeanUtils.copyProperties(driver, newDriver);
        driverService.create(newDriver);
        return beanMappingService.mapTo(newDriver, DriverDto.class);
    }

    @Override
    public DriverDto createDriverRest(DriverCreateDto driver) {
        Driver newDriver = new Driver();
        BeanUtils.copyProperties(driver, newDriver);
        driverService.create(newDriver);
        return beanMappingService.mapTo(newDriver, DriverDto.class);
    }

    @Override
    public void deleteDriver(DriverDto driver) {
        Driver newDriver = new Driver();
        BeanUtils.copyProperties(driver, newDriver);
        var cars = carFacade.findAllCars();
        for (var car : cars) {
            if (car.getDriver() != null && Objects.equals(car.getDriver().getId(), driver.getId())) {
                carFacade.changeDriver(null, car.getId());
                break;
            }
        }
        driverService.remove(newDriver);
    }
}
