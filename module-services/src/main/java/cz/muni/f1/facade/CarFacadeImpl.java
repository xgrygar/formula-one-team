package cz.muni.f1.facade;


import cz.muni.f1.dto.Car.CarDto;
import cz.muni.f1.dto.CarPart.CarPartDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.entity.Car;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.CarPartService;
import cz.muni.f1.services.CarService;
import cz.muni.f1.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roman Duris <485642@mail.muni.cz>
 * @project Formula One Team
 */
@Service
@Transactional
public class CarFacadeImpl implements CarFacade {

    @Autowired
    private CarService carService;

    @Autowired
    private CarPartService carPartService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public List<CarDto> findAllCars() {
        return beanMappingService.mapTo(carService.findAll(), CarDto.class);
    }

    @Override
    public CarDto getCarById(Long id) {
        Car car = carService.findById(id);
        return beanMappingService.mapTo(car, CarDto.class);
    }

    @Override
    public void changeDriver(Long driverID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null");
        }
        carService.changeDriver(driverID, carID);
    }

    @Override
    public void changeEngine(Long engineID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null");
        }
        carService.changeEngine(engineID, carID);
    }

    @Override
    public void changeFrontWing(Long frontWingID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null");
        }
        carService.changeFrontWing(frontWingID, carID);
    }

    @Override
    public void changeRearWing(Long rearWingID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null");
        }
        carService.changeRearWing(rearWingID, carID);
    }

    @Override
    public void changeSuspension(Long suspensionID, Long carID) {
        if (carID == null) {
            throw new IllegalArgumentException("Car ID is null");
        }
        carService.changeDriver(suspensionID, carID);
    }

    @Override
    public void updateCar(CarDto carDto) {

        List<CarPartDto> assignedCarParts = new ArrayList<>();
        List<DriverDto> assignedDrivers = new ArrayList<>();
        for (var car : findAllCars()
        ) {
            if (car.getId().equals(carDto.getId())) {
                continue;
            }
            assignedCarParts.add(car.getFrontWing());
            assignedCarParts.add(car.getRearWing());
            assignedCarParts.add(car.getEngine());
            assignedCarParts.add(car.getSuspension());
            assignedDrivers.add(car.getDriver());
        }
        if (assignedCarParts.contains(carDto.getEngine()) ||
                assignedCarParts.contains(carDto.getFrontWing()) ||
                assignedCarParts.contains(carDto.getRearWing()) ||
                assignedCarParts.contains(carDto.getSuspension()) ||
                assignedDrivers.contains(carDto.getDriver())) {
            throw new IllegalArgumentException("Driver with one of parameters to change already exists");
        }
        carService.update(beanMappingService.mapTo(carDto, Car.class));
    }


}
