package cz.muni.f1.facade;

import cz.muni.f1.dto.Car.CarDto;
import cz.muni.f1.dto.Driver.DriverDto;
import cz.muni.f1.dto.Team.TeamDto;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.Team;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.CarService;
import cz.muni.f1.services.DriverService;
import cz.muni.f1.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


/**
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Service
@Transactional
public class TeamFacadeImpl implements TeamFacade {

    @Autowired
    private TeamService teamService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarService carService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public List<TeamDto> findAllTeams() {
        return beanMappingService.mapTo(teamService.findAll(), TeamDto.class);
    }

    @Override
    public TeamDto getTeamById(Long id) {
        var team = teamService.findById(id);
        return beanMappingService.mapTo(team, TeamDto.class);
    }

    @Override
    public CarDto addCar(Long teamID, Long carId) {
        Team team = teamService.findById(teamID);
        Car car = carService.findById(carId);
        teamService.addCar(team, car);
        return beanMappingService.mapTo(car, CarDto.class);
    }

    @Override
    public Boolean removeCar(Long teamID, Long carId) {
        Team team = teamService.findById(teamID);
        Car car = carService.findById(carId);
        teamService.removeCar(team, car);
        return true;
    }

    @Override
    public DriverDto addDriver(Long teamID, Long driverID) {
        Team team = teamService.findById(teamID);
        Driver driver = driverService.findById(driverID);
        teamService.addDriver(team, driver);
        return beanMappingService.mapTo(driver, DriverDto.class);
    }

    @Override
    public Boolean removeDriver(Long teamID, Long driverID) {
        Team team = teamService.findById(teamID);
        Driver driver = driverService.findById(driverID);
        teamService.removeDriver(team, driver);
        return true;
    }
}
