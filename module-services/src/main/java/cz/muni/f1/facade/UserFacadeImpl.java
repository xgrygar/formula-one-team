package cz.muni.f1.facade;

import cz.muni.f1.dto.User.UserCreateDto;
import cz.muni.f1.dto.User.UserDto;
import cz.muni.f1.entity.User;
import cz.muni.f1.services.BeanMappingService;
import cz.muni.f1.services.EngineeringDepartmentService;
import cz.muni.f1.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  Roman Duris <485642@mail.muni.cz>
 * @project formula-one-team
 */
@Service
@Transactional
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserService userService;

    @Autowired
    private BeanMappingService beanMappingService;

    @Autowired
    private EngineeringDepartmentService engineeringDepartmentService;

    @Override
    public List<UserDto> findAllUsers() {
        return beanMappingService.mapTo(userService.findAll(), UserDto.class);
    }

    @Override
    public UserDto getUserById(Long id) {
        var user = userService.findById(id);
        return beanMappingService.mapTo(user, UserDto.class);
    }

    @Override
    public List<UserDto> findAllManagers() {
        var allManagers = userService.findAllManagers();
        return beanMappingService.mapTo(allManagers, UserDto.class);
    }

    @Override
    public List<UserDto> findAllEngineers() {
        var allEngineers = userService.findAllEngineers();
        return beanMappingService.mapTo(allEngineers, UserDto.class);
    }

    @Override
    public UserDto logIn(String username, String password) {
        for (var user :
                findAllUsers()) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public UserDto updateUser(UserDto user) {
        User newUser = new User();
        BeanUtils.copyProperties(user, newUser);
        userService.update(newUser);
        return beanMappingService.mapTo(newUser, UserDto.class);
    }

    @Override
    public UserDto createUser(UserDto user) {
        User newUser = new User();
        BeanUtils.copyProperties(user, newUser);
        userService.create(newUser);
        return beanMappingService.mapTo(newUser, UserDto.class);
    }

    @Override
    public UserDto createUserRest(UserCreateDto user) {
        User newUser = new User();
        BeanUtils.copyProperties(user, newUser);
        userService.create(newUser);
        return beanMappingService.mapTo(newUser, UserDto.class);
    }

    @Override
    public void deleteUser(UserDto user) {
        User newUser = new User();
        BeanUtils.copyProperties(user, newUser);
        userService.remove(newUser);
    }

    @Override
    public void deleteEngineer(UserDto user) {
        var engDepts = engineeringDepartmentService.findAll();
        for (var dept: engDepts) {
            engineeringDepartmentService.removeEngineer(dept,beanMappingService.mapTo(user,User.class));
        }
        deleteUser(user);
    }
}
