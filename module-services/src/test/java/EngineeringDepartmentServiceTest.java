import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.EngineeringDepartmentDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.services.EngineeringDepartmentService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for EngineeringDepartmentService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class EngineeringDepartmentServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private EngineeringDepartmentDAO engineeringDepartmentDAO;

    @Mock
    private SubDepartmentDAO subDepartmentDAO;

    @Mock
    private UserDAO userDAO;

    @Mock
    private CarPartDAO carPartDAO;

    @Autowired
    @InjectMocks
    private EngineeringDepartmentService engineeringDepartmentService;

    private EngineeringDepartment dummyEngineeringDepartment;

    private User dummyEngineer1;
    private User dummyEngineer2;
    private User dummyEngineer3;

    private CarPart dummyCarPart1;
    private CarPart dummyCarPart2;
    private CarPart dummyCarPart3;

    private SubDepartment dummySubDepartment1;
    private SubDepartment dummySubDepartment2;
    private SubDepartment dummySubDepartment3;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    private User createDummyEngineer(Long id) {
        User engineer = new User();
        engineer.setName("Name" + id);
        engineer.setSurname("Surname" + id);
        engineer.setUsername("username" + id);
        engineer.setPassword("password" + id);
        engineer.setRole(UserType.ENGINEER);
        engineer.setId(90L + id);
        return engineer;
    }

    private CarPart createDummyCarPart(PartType partType) {
        CarPart carPart = new CarPart();
        carPart.setPartType(partType);
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartName("Dummy " + partType.name() + "S99" + partType.ordinal());
        carPart.setId(990L + partType.ordinal());
        return carPart;
    }

    @BeforeMethod
    public void createDummy() {
        dummyEngineeringDepartment = new EngineeringDepartment();
        dummyEngineer1 = createDummyEngineer(1L);
        dummyEngineer2 = createDummyEngineer(2L);
        dummyEngineer3 = createDummyEngineer(3L);
        dummyCarPart1 = createDummyCarPart(PartType.ENGINE);
        dummyCarPart2 = createDummyCarPart(PartType.SUSPENSION);
        dummyCarPart3 = createDummyCarPart(PartType.FRONT_WING);
        dummySubDepartment1 = new SubDepartment();
        dummySubDepartment2 = new SubDepartment();
        dummySubDepartment3 = new SubDepartment();

        // dummySubDepartment setup
        dummySubDepartment1.addEngineer(dummyEngineer1);
        dummySubDepartment1.addCarPart(dummyCarPart1);
        dummySubDepartment1.setDepartmentType(DepartmentType.EngineDepartment);
        dummySubDepartment1.setId(9991L);

        dummySubDepartment2.addEngineer(dummyEngineer2);
        dummySubDepartment2.addCarPart(dummyCarPart2);
        dummySubDepartment2.setDepartmentType(DepartmentType.SuspensionDepartment);
        dummySubDepartment2.setId(9992L);

        dummySubDepartment3.addEngineer(dummyEngineer3);
        dummySubDepartment3.addCarPart(dummyCarPart3);
        dummySubDepartment3.setDepartmentType(DepartmentType.AeroDynamicsDepartment);
        dummySubDepartment3.setId(9993L);

        // dummyEngineeringDepartment setup
        dummyEngineeringDepartment.addEngineer(dummyEngineer1);
        dummyEngineeringDepartment.addEngineer(dummyEngineer2);
        dummyEngineeringDepartment.addEngineer(dummyEngineer3);
        dummyEngineeringDepartment.setEngineDepartment(dummySubDepartment1);
        dummyEngineeringDepartment.setSuspensionDepartment(dummySubDepartment2);
        dummyEngineeringDepartment.setAeroDepartment(dummySubDepartment3);
        dummyEngineeringDepartment.setId(99991L);
    }

    @Test
    public void createEngineeringDepartment() {
        engineeringDepartmentService.create(dummyEngineeringDepartment);

        verify(engineeringDepartmentDAO, times(1)).create(dummyEngineeringDepartment);
    }

    @Test
    public void findAllEngineeringDepartments() {
        when(engineeringDepartmentDAO.findAll()).thenReturn(List.of(dummyEngineeringDepartment));

        List<EngineeringDepartment> engineeringDepartments = engineeringDepartmentService.findAll();
        assertThat(engineeringDepartments)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyEngineeringDepartment);
    }

    @Test
    public void findByIdDepartment() {
        when(engineeringDepartmentDAO.findById(dummyEngineeringDepartment.getId())).thenReturn(dummyEngineeringDepartment);

        EngineeringDepartment engineeringDepartment = engineeringDepartmentService.findById(dummyEngineeringDepartment.getId());
        assertEquals(engineeringDepartment, dummyEngineeringDepartment);
    }

    @Test
    public void updateEngineeringDepartment() {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setDepartmentType(DepartmentType.SuspensionDepartment);
        subDepartment.setId(9797L);
        dummyEngineeringDepartment.setSuspensionDepartment(subDepartment);

        engineeringDepartmentService.update(dummyEngineeringDepartment);
        verify(engineeringDepartmentDAO, times(1)).update(dummyEngineeringDepartment);
    }

    @Test
    public void removeEngineeringDepartment() {
        engineeringDepartmentService.remove(dummyEngineeringDepartment);

        verify(engineeringDepartmentDAO, times(1)).remove(dummyEngineeringDepartment);
    }

    @Test
    public void addEngineerEngDept() {
        User engineer = createDummyEngineer(22L);
        engineeringDepartmentService.addEngineer(dummyEngineeringDepartment, engineer);

        verify(engineeringDepartmentDAO, times(1)).update(dummyEngineeringDepartment);
    }

    @Test
    public void removeEngineerEngDept() {
        engineeringDepartmentService.removeEngineer(dummyEngineeringDepartment, dummyEngineer1);

        verify(engineeringDepartmentDAO, times(1)).update(dummyEngineeringDepartment);
    }

    @Test
    public void changeSubDeptEngDept() throws Exception {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.setDepartmentType(DepartmentType.SuspensionDepartment);
        subDepartment.setId(9797L);
        dummyEngineeringDepartment.setSuspensionDepartment(subDepartment);

        when(subDepartmentDAO.findById(subDepartment.getId())).thenReturn(subDepartment);
        when(engineeringDepartmentDAO.findById(dummyEngineeringDepartment.getId())).thenReturn(dummyEngineeringDepartment);

        engineeringDepartmentService.changeSubDepartment(dummyEngineeringDepartment.getId(), subDepartment.getId());
        verify(engineeringDepartmentDAO, times(1)).update(dummyEngineeringDepartment);
        verify(subDepartmentDAO, times(1)).findById(subDepartment.getId());
    }
}
