import cz.muni.f1.dao.CarDAO;
import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.dao.EngineeringDepartmentDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.TeamDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.Team;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.services.TeamService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;


/**
 * Tests for TeamService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class TeamServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private TeamDAO teamDAO;

    @Mock
    private CarDAO carDAO;

    @Mock
    private DriverDAO driverDAO;

    @Mock
    private EngineeringDepartmentDAO engineeringDepartmentDAO;

    @Mock
    private SubDepartmentDAO subDepartmentDAO;

    @Mock
    private UserDAO userDAO;

    @Mock
    private CarPartDAO carPartDAO;

    @Autowired
    @InjectMocks
    private TeamService teamService;

    private Team dummyTeam;

    private User dummyManager;

    private Driver dummyDriver;

    private Car dummyCar;

    private EngineeringDepartment dummyEngineeringDepartment;

    private User dummyEngineer1;
    private User dummyEngineer2;
    private User dummyEngineer3;

    private CarPart dummyCarPart_engine;
    private CarPart dummyCarPart_suspension;
    private CarPart dummyCarPart_front_wing;
    private CarPart dummyCarPart_rear_wing;

    private SubDepartment dummySubDepartment1;
    private SubDepartment dummySubDepartment2;
    private SubDepartment dummySubDepartment3;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    private User createDummyManager(Long id) {
        User manager = new User();
        manager.setName("ManagerName" + id);
        manager.setSurname("ManagerSurname" + id);
        manager.setNationality("Polandistan");
        manager.setUsername("manager");
        manager.setPassword("passwordManager" + id);
        manager.setId(69690L + id);
        return manager;
    }

    private User createDummyEngineer(Long id) {
        User engineer = new User();
        engineer.setName("Name" + id);
        engineer.setSurname("Surname" + id);
        engineer.setUsername("username" + id);
        engineer.setPassword("password" + id);
        engineer.setRole(UserType.ENGINEER);
        engineer.setId(90L + id);
        return engineer;
    }

    private CarPart createDummyCarPart(PartType partType) {
        CarPart carPart = new CarPart();
        carPart.setPartType(partType);
        carPart.setPartInfo(new JSONObject().toString());
        carPart.setPartName("Dummy " + partType.name() + "S99" + partType.ordinal());
        carPart.setId(990L + partType.ordinal());
        return carPart;
    }

    private Driver createDummyDriver() {
        Driver driver = new Driver();
        driver.setName("Max");
        driver.setSurname("Verstappen");
        driver.setNationality("Dutch");

        driver.setRacePerformance(9);
        driver.setWetPerformance(8);
        driver.setAgresiveness(7);
        driver.setRacePace(6);
        driver.setQualificationPace(5);
        driver.setId(11L);

        return driver;
    }

    @BeforeMethod
    public void createDummy() {
        dummyTeam = new Team();
        dummyManager = createDummyManager(1L);
        dummyDriver = createDummyDriver();
        dummyCar = new Car();
        dummyEngineeringDepartment = new EngineeringDepartment();
        dummyEngineer1 = createDummyEngineer(1L);
        dummyEngineer2 = createDummyEngineer(2L);
        dummyEngineer3 = createDummyEngineer(3L);
        dummyCarPart_engine = createDummyCarPart(PartType.ENGINE);
        dummyCarPart_suspension = createDummyCarPart(PartType.SUSPENSION);
        dummyCarPart_front_wing = createDummyCarPart(PartType.FRONT_WING);
        dummyCarPart_rear_wing = createDummyCarPart(PartType.REAR_WING);
        dummySubDepartment1 = new SubDepartment();
        dummySubDepartment2 = new SubDepartment();
        dummySubDepartment3 = new SubDepartment();

        // dummySubDepartment setup
        dummySubDepartment1.addEngineer(dummyEngineer1);
        dummySubDepartment1.addCarPart(dummyCarPart_engine);
        dummySubDepartment1.setDepartmentType(DepartmentType.EngineDepartment);
        dummySubDepartment1.setId(9991L);

        dummySubDepartment2.addEngineer(dummyEngineer2);
        dummySubDepartment2.addCarPart(dummyCarPart_suspension);
        dummySubDepartment2.setDepartmentType(DepartmentType.SuspensionDepartment);
        dummySubDepartment2.setId(9992L);

        dummySubDepartment3.addEngineer(dummyEngineer3);
        dummySubDepartment3.addCarPart(dummyCarPart_front_wing);
        dummySubDepartment3.setDepartmentType(DepartmentType.AeroDynamicsDepartment);
        dummySubDepartment3.setId(9993L);

        // dummyEngineeringDepartment setup
        dummyEngineeringDepartment.addEngineer(dummyEngineer1);
        dummyEngineeringDepartment.addEngineer(dummyEngineer2);
        dummyEngineeringDepartment.addEngineer(dummyEngineer3);
        dummyEngineeringDepartment.setEngineDepartment(dummySubDepartment1);
        dummyEngineeringDepartment.setSuspensionDepartment(dummySubDepartment2);
        dummyEngineeringDepartment.setAeroDepartment(dummySubDepartment3);
        dummyEngineeringDepartment.setId(99991L);

        // dummyCar setup
        dummyCar.setDriver(dummyDriver);
        dummyCar.setEngine(dummyCarPart_engine);
        dummyCar.setSuspension(dummyCarPart_suspension);
        dummyCar.setFrontWing(dummyCarPart_front_wing);
        dummyCar.setRearWing(dummyCarPart_rear_wing);
        dummyCar.setId(102L);

        // dummyTeam setup
        dummyTeam.setManager(dummyManager);
        dummyTeam.addDriver(dummyDriver);
        dummyTeam.addCar(dummyCar);
        dummyTeam.setEngineeringDepartment(dummyEngineeringDepartment);
        dummyTeam.setId(646858L);
    }

    @Test
    public void createTeam() {
        teamService.create(dummyTeam);

        verify(teamDAO, times(1)).create(dummyTeam);
    }

    @Test
    public void findAllTeams() {
        when(teamDAO.findAll()).thenReturn(List.of(dummyTeam));

        List<Team> teams = teamService.findAll();
        assertThat(teams)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyTeam);
    }

    @Test
    public void findByIdTeam() {
        when(teamDAO.findById(dummyTeam.getId())).thenReturn(dummyTeam);

        Team team = teamService.findById(dummyTeam.getId());
        assertEquals(team, dummyTeam);
    }

    @Test
    public void updateTeam() {
        User manager = createDummyManager(2L);
        dummyTeam.setManager(manager);
        teamService.update(dummyTeam);

        verify(teamDAO, times(1)).update(dummyTeam);
    }

    @Test
    public void removeTeam() {
        teamService.remove(dummyTeam);

        verify(teamDAO, times(1)).remove(dummyTeam);
    }

    @Test
    public void addCarToTeam() {
        Car car = new Car();
        car.setId(1651L);
        car.setEngine(createDummyCarPart(PartType.ENGINE));
        car.setSuspension(createDummyCarPart(PartType.SUSPENSION));
        car.setFrontWing(createDummyCarPart(PartType.FRONT_WING));
        car.setRearWing(createDummyCarPart(PartType.REAR_WING));
        car.setDriver(dummyDriver);

        teamService.addCar(dummyTeam, car);
        verify(teamDAO, times(1)).update(dummyTeam);
    }

    @Test
    public void removeCarFromTeam() {
        teamService.removeCar(dummyTeam, dummyCar);

        verify(teamDAO, atLeastOnce()).update(dummyTeam);
    }

    @Test
    public void addDriverToTeam() {
        Driver driver = createDummyDriver();
        driver.setName("Maxelinos");
        driver.setSurname("Ferstapenos");
        driver.setId(driver.getId() + 90000L);

        teamService.addDriver(dummyTeam, driver);
        verify(teamDAO, times(1)).update(dummyTeam);
    }

    @Test
    public void removeDriverFromTeam() {
        teamService.removeDriver(dummyTeam, dummyDriver);

        verify(teamDAO, times(1)).update(dummyTeam);
    }
}
