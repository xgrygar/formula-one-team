import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.services.DriverService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for DriverService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class DriverServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private DriverDAO driverDAO;

    @Autowired
    @InjectMocks
    private DriverService driverService;

    private Driver dummyDriver;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void createDummy() {
        dummyDriver = new Driver();
        dummyDriver.setName("Max");
        dummyDriver.setSurname("Verstappen");
        dummyDriver.setNationality("Dutch");

        dummyDriver.setRacePerformance(9);
        dummyDriver.setWetPerformance(8);
        dummyDriver.setAgresiveness(7);
        dummyDriver.setRacePace(6);
        dummyDriver.setQualificationPace(5);
        dummyDriver.setId(11L);
    }

    @Test
    public void createDriver() {
        driverService.create(dummyDriver);

        verify(driverDAO, times(1)).create(dummyDriver);
    }

    @Test
    public void findByIdDriver() {
        when(driverDAO.findById(dummyDriver.getId())).thenReturn(dummyDriver);

        Driver driver = driverService.findById(dummyDriver.getId());
        assertEquals(driver, dummyDriver);
    }

    @Test
    public void findByNamePerson() {
        when(driverDAO.findByName(dummyDriver.getName())).thenReturn(List.of(dummyDriver));

        List<Driver> drivers = driverService.findByName(dummyDriver.getName());
        assertThat(drivers)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyDriver);
    }

    @Test
    public void updatePerson() {
        dummyDriver.setName(dummyDriver.getName() + "avel");
        driverService.update(dummyDriver);
        verify(driverDAO, times(1)).update(dummyDriver);
    }

    @Test
    public void removePerson() {
        driverService.remove(dummyDriver);

        verify(driverDAO, times(1)).remove(dummyDriver);
    }

    @Test
    public void findAllDrivers() {
        when(driverDAO.findAll()).thenReturn(List.of(dummyDriver));

        List<Driver> drivers = driverService.findAll();
        assertThat(drivers)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyDriver);
    }
}
