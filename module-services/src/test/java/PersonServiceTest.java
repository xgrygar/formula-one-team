import cz.muni.f1.dao.PersonDAO;
import cz.muni.f1.entity.Person;
import cz.muni.f1.services.PersonService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for PersonService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class PersonServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private PersonDAO personDAO;

    @Autowired
    @InjectMocks
    private PersonService personService;

    private Person dummyPerson;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void createDummy() {
        dummyPerson = new Person();
        dummyPerson.setName("Dummy");
        dummyPerson.setSurname("Dummovic");
        dummyPerson.setNationality("Slovakistan");
        dummyPerson.setId(69L);
    }

    @Test
    public void createPerson() {
        personService.create(dummyPerson);

        verify(personDAO, times(1)).create(dummyPerson);
    }

    @Test
    public void findAllPersons() {
        when(personDAO.findAll()).thenReturn(List.of(dummyPerson));

        List<Person> persons = personService.findAll();
        assertThat(persons)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyPerson);
    }

    @Test
    public void findByIdPerson() {
        when(personDAO.findById(dummyPerson.getId())).thenReturn(dummyPerson);

        Person person = personService.findById(dummyPerson.getId());
        assertEquals(person, dummyPerson);
    }

    @Test
    public void findByNamePerson() {
        when(personDAO.findByName(dummyPerson.getName())).thenReturn(List.of(dummyPerson));

        List<Person> persons = personService.findByName(dummyPerson.getName());
        assertThat(persons)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyPerson);
    }

    @Test
    public void updatePerson() {
        dummyPerson.setName(dummyPerson.getName() + "avel");
        personService.update(dummyPerson);
        verify(personDAO, times(1)).update(dummyPerson);
    }

    @Test
    public void removePerson() {
        personService.remove(dummyPerson);

        verify(personDAO, times(1)).remove(dummyPerson);
    }
}
