import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.services.SubDepartmentService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for SubDepartmentService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class SubDepartmentServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private SubDepartmentDAO subDepartmentDAO;

    @Mock
    private UserDAO userDAO;

    @Mock
    private CarPartDAO carPartDAO;

    @Autowired
    @InjectMocks
    private SubDepartmentService subDepartmentService;

    private SubDepartment dummySubDepartment;

    private User dummyEngineer1;
    private User dummyEngineer2;

    private CarPart dummyCarPart1;
    private CarPart dummyCarPart2;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    private User createDummyEngineer1() {
        User engineer = new User();
        engineer.setName("Engineeris");
        engineer.setSurname("Numone");
        engineer.setNationality("Slovakistan");
        engineer.setUsername("numone");
        engineer.setPassword("w3ar3numb3r1");
        engineer.setRole(UserType.ENGINEER);
        engineer.setId(1L);
        return engineer;
    }

    private User createDummyEngineer2() {
        User engineer = new User();
        engineer.setName("Inziniero");
        engineer.setSurname("Twonumb");
        engineer.setNationality("Czechistan");
        engineer.setUsername("twonumb");
        engineer.setPassword("twofast4u->");
        engineer.setRole(UserType.ENGINEER);
        engineer.setId(2L);
        return engineer;
    }

    private CarPart createDummyFrontWing() {
        CarPart frontWing = new CarPart();
        frontWing.setPartName("Dummy Front Wing S42");
        frontWing.setPartInfo(new JSONObject().toString());
        frontWing.setPartType(PartType.FRONT_WING);
        frontWing.setId(42L);
        return frontWing;
    }

    public CarPart createDummyRearWing() {
        CarPart rearWing = new CarPart();
        rearWing.setPartName("Dummy Rear Wing S43");
        rearWing.setPartInfo(new JSONObject().toString());
        rearWing.setPartType(PartType.REAR_WING);
        rearWing.setId(43L);
        return rearWing;
    }

    @BeforeMethod
    public void createDummy() {
        dummySubDepartment = new SubDepartment();
        dummyEngineer1 = createDummyEngineer1();
        dummyEngineer2 = createDummyEngineer2();
        dummyCarPart1 = createDummyFrontWing();
        dummyCarPart2 = createDummyRearWing();

        // dummySubDepartment setup
        dummySubDepartment.addEngineer(dummyEngineer1);
        dummySubDepartment.addEngineer(dummyEngineer2);

        dummySubDepartment.addCarPart(dummyCarPart1);
        dummySubDepartment.addCarPart(dummyCarPart2);

        dummySubDepartment.setDepartmentType(DepartmentType.AeroDynamicsDepartment);
        dummySubDepartment.setId(987654L);
    }

    @Test
    public void createSubDepartment() {
        subDepartmentService.create(dummySubDepartment);

        verify(subDepartmentDAO, times(1)).create(dummySubDepartment);
    }

    @Test
    public void findAllSubDepartments() {
        when(subDepartmentDAO.findAll()).thenReturn(List.of(dummySubDepartment));

        List<SubDepartment> subDepartments = subDepartmentService.findAll();
        assertThat(subDepartments)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummySubDepartment);
    }

    @Test
    public void findByIdSubDepartment() {
        when(subDepartmentDAO.findById(dummySubDepartment.getId())).thenReturn(dummySubDepartment);

        SubDepartment subDepartment = subDepartmentService.findById(dummySubDepartment.getId());
        assertEquals(subDepartment, dummySubDepartment);
    }

    @Test
    public void updateSubDepartment() {
        dummySubDepartment.setDepartmentType(DepartmentType.EngineDepartment);

        subDepartmentService.update(dummySubDepartment);
        verify(subDepartmentDAO, times(1)).update(dummySubDepartment);
    }

    @Test
    public void removeSubDepartment() {
        subDepartmentService.remove(dummySubDepartment);

        verify(subDepartmentDAO, times(1)).remove(dummySubDepartment);
    }

    @Test
    public void addEngineer() {
        User engineer = createDummyEngineer2();
        engineer.setUsername(engineer.getUsername() + "xXx");
        engineer.setId(engineer.getId() + 90000L);

        when(subDepartmentDAO.findById(dummySubDepartment.getId())).thenReturn(dummySubDepartment);

        subDepartmentService.addEngineeer(dummySubDepartment.getId(), engineer);
        verify(subDepartmentDAO, times(1)).update(dummySubDepartment);
    }

    @Test
    public void removeEngineer() {
        when(subDepartmentDAO.findById(dummySubDepartment.getId())).thenReturn(dummySubDepartment);

        subDepartmentService.removeEngineer(dummySubDepartment.getId(), dummyEngineer1);
        verify(subDepartmentDAO, times(1)).update(dummySubDepartment);
    }

    @Test
    public void addCarPart() {
        CarPart carPart = createDummyFrontWing();
        carPart.setPartName(carPart.getPartName() + " V2");
        carPart.setId(carPart.getId() + 90000L);

        when(subDepartmentDAO.findById(dummySubDepartment.getId())).thenReturn(dummySubDepartment);

        subDepartmentService.addCarPart(dummySubDepartment.getId(), carPart);
        verify(subDepartmentDAO, times(1)).update(dummySubDepartment);
    }
}
