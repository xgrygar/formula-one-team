import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.services.CarPartService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for CarPart
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class CarPartServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private CarPartDAO carPartDAO;

    @Autowired
    @InjectMocks
    private CarPartService carPartService;

    private CarPart dummyCarPart;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    private CarPart createDummyFrontWing() {
        CarPart frontWing = new CarPart();
        frontWing.setPartName("Dummy Front Wing S42");
        frontWing.setPartInfo(new JSONObject().toString());
        frontWing.setPartType(PartType.FRONT_WING);
        frontWing.setId(42L);

        return frontWing;
    }


    @BeforeMethod
    public void createDummy() {
        dummyCarPart = createDummyFrontWing();
    }

    @Test
    public void createCarPart() {
        carPartService.create(dummyCarPart);

        verify(carPartDAO, times(1)).create(dummyCarPart);
    }

    @Test
    public void findAllCarParts() {
        when(carPartDAO.findAll()).thenReturn(List.of(dummyCarPart));

        List<CarPart> carParts = carPartService.findAll();
        assertThat(carParts)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyCarPart);
    }

    @Test
    public void findById() {
        when(carPartDAO.findById(dummyCarPart.getId())).thenReturn(dummyCarPart);

        CarPart carPart = carPartService.findById(dummyCarPart.getId());
        assertEquals(carPart, dummyCarPart);
    }

    @Test
    public void findByNameCarParts() {
        when(carPartDAO.findByName(dummyCarPart.getPartName())).thenReturn(List.of(dummyCarPart));

        List<CarPart> carParts = carPartService.findByName(dummyCarPart.getPartName());
        assertThat(carParts)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyCarPart);
    }

    @Test
    public void updateCarPart() {
        dummyCarPart.setPartName(dummyCarPart.getPartName() + " V2");
        String partInfo = "{\n" +
                "  \"type\": \"FRONT_WING\",\n" +
                "  \"manufacturer\": \"Huondanee\",\n" +
                "  \"aeroDynamicsRating\": 78\n" +
                "}";
        dummyCarPart.setPartInfo(new JSONObject(partInfo).toString());


        carPartService.update(dummyCarPart);
        verify(carPartDAO, times(1)).update(dummyCarPart);

    }

    @Test
    public void removeCarPart() {
        carPartService.remove(dummyCarPart);

        verify(carPartDAO, times(1)).remove(dummyCarPart);
    }
}
