import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.services.UserService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for UserService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class UserServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private UserDAO userDAO;

    @Autowired
    @InjectMocks
    private UserService userService;

    private User dummyUser;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void createDummy() {
        dummyUser = new User();
        dummyUser.setName("Dummy");
        dummyUser.setSurname("Dummovic");
        dummyUser.setNationality("Czechistan");

        dummyUser.setUsername("dummyuser420");
        dummyUser.setPassword("b3st_p4ss_69!");
        dummyUser.setRole(UserType.MANAGER);
        dummyUser.setId(420L);
    }

    @Test
    public void createUser() {
        userService.create(dummyUser);

        verify(userDAO, times(1)).create(dummyUser);
    }

    @Test
    public void findAllUsers() {
        when(userDAO.findAll()).thenReturn(List.of(dummyUser));

        List<User> users = userService.findAll();
        assertThat(users)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyUser);
    }

    @Test
    public void findByIdUser() {
        when(userDAO.findById(dummyUser.getId())).thenReturn(dummyUser);

        User user = userService.findById(dummyUser.getId());
        assertEquals(user, dummyUser);
    }

    @Test
    public void findByNameUser() {
        when(userDAO.findByName(dummyUser.getName())).thenReturn(List.of(dummyUser));

        List<User> users = userService.findByName(dummyUser.getName());
        assertThat(users)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyUser);
    }

    @Test
    public void updateUser() {
        dummyUser.setUsername(dummyUser.getUsername() + "_xXx");
        userService.update(dummyUser);
        verify(userDAO, times(1)).update(dummyUser);
    }

    @Test
    public void removeUser() {
        userService.remove(dummyUser);

        verify(userDAO, times(1)).remove(dummyUser);
    }

    @Test
    public void findAllManagers() {
        userService.findAllManagers();

        verify(userDAO, atLeastOnce()).findAll();
    }

    @Test
    public void findAllEngineers() {
        userService.findAllEngineers();

        verify(userDAO, times(1)).findAll();
    }

}
