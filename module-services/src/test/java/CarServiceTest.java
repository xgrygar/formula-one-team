import cz.muni.f1.dao.CarDAO;
import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.DriverDAO;
import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.services.CarService;
import cz.muni.f1.services.config.ServiceConfiguration;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertThrows;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Tests for CarService
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 * @project Formula One Team
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class CarServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private CarDAO carDAO;

    @Mock
    private DriverDAO driverDAO;

    @Mock
    private CarPartDAO carPartDAO;

    @Autowired
    @InjectMocks
    private CarService carService;

    private Car dummyCar;

    private Driver dummyDriver;

    private CarPart dummyCarPart_engine;
    private CarPart dummyCarPart_suspension;
    private CarPart dummyCarPart_front_wing;
    private CarPart dummyCarPart_rear_wing;

    @BeforeClass
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    private Driver createDummyDriver() {
        Driver driver = new Driver();
        driver.setName("Max");
        driver.setSurname("Verstappen");
        driver.setNationality("Dutch");

        driver.setRacePerformance(9);
        driver.setWetPerformance(8);
        driver.setAgresiveness(7);
        driver.setRacePace(6);
        driver.setQualificationPace(5);
        driver.setId(11L);

        return driver;
    }

    private CarPart createDummyEngine() {
        CarPart engine = new CarPart();
        engine.setPartName("Dummy Engine S40");
        engine.setPartInfo(new JSONObject().toString());
        engine.setPartType(PartType.ENGINE);
        engine.setId(40L);
        return engine;
    }

    private CarPart createDummySuspension() {
        CarPart suspension = new CarPart();
        suspension.setPartName("Dummy Suspension S41");
        suspension.setPartInfo(new JSONObject().toString());
        suspension.setPartType(PartType.SUSPENSION);
        suspension.setId(41L);
        return suspension;
    }

    private CarPart createDummyFrontWing() {
        CarPart frontWing = new CarPart();
        frontWing.setPartName("Dummy Front Wing S42");
        frontWing.setPartInfo(new JSONObject().toString());
        frontWing.setPartType(PartType.FRONT_WING);
        frontWing.setId(42L);
        return frontWing;
    }

    public CarPart createDummyRearWing() {
        CarPart rearWing = new CarPart();
        rearWing.setPartName("Dummy Rear Wing S43");
        rearWing.setPartInfo(new JSONObject().toString());
        rearWing.setPartType(PartType.REAR_WING);
        rearWing.setId(43L);
        return rearWing;
    }

    @BeforeMethod
    public void createDummy() {
        dummyCar = new Car();
        dummyDriver = createDummyDriver();
        dummyCarPart_engine = createDummyEngine();
        dummyCarPart_suspension = createDummySuspension();
        dummyCarPart_front_wing = createDummyFrontWing();
        dummyCarPart_rear_wing = createDummyRearWing();

        // dummyCar setup
        dummyCar.setDriver(dummyDriver);
        dummyCar.setEngine(dummyCarPart_engine);
        dummyCar.setSuspension(dummyCarPart_suspension);
        dummyCar.setFrontWing(dummyCarPart_front_wing);
        dummyCar.setRearWing(dummyCarPart_rear_wing);
        dummyCar.setId(102L);
    }

    @Test
    public void createCar() {
        carService.create(dummyCar);
        verify(carDAO, times(1)).create(dummyCar);
    }

    @Test
    public void findAllCars() {
        when(carDAO.findAll()).thenReturn(List.of(dummyCar));

        List<Car> cars = carService.findAll();
        assertThat(cars)
                .isNotEmpty()
                .hasSize(1)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(dummyCar);
    }

    @Test
    public void findByIdCar() {
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        Car car = carService.findById(dummyCar.getId());
        assertEquals(car, dummyCar);
    }

    @Test
    public void updateCar() {
        CarPart engine = createDummyEngine();
        engine.setPartName(engine.getPartName() + " V2");
        engine.setId(engine.getId() + 90000L);

        dummyCar.setEngine(engine);
        carService.update(dummyCar);
        verify(carDAO, times(2)).update(dummyCar);
    }

    @Test
    public void removeCar() {
        carService.remove(dummyCar);

        verify(carDAO, times(1)).remove(dummyCar);
    }

    @Test
    public void changeDriver() {
        Driver driver = createDummyDriver();
        driver.setName("Maxelinos");
        driver.setSurname("Ferstapenos");
        driver.setId(driver.getId() + 90000L);

        when(driverDAO.findById(driver.getId())).thenReturn(driver);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        carService.changeDriver(driver.getId(), dummyCar.getId());
        verify(carDAO, times(1)).update(dummyCar);
        verify(driverDAO, times(1)).findById(driver.getId());
    }

    @Test
    public void changeEngine() {
        CarPart engine = createDummyEngine();
        engine.setPartName(engine.getPartName() + " V2");
        engine.setId(engine.getId() + 90000L);

        when(carPartDAO.findById(engine.getId())).thenReturn(engine);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        carService.changeEngine(engine.getId(), dummyCar.getId());
        verify(carDAO, times(1)).update(dummyCar);
        verify(carPartDAO, times(1)).findById(engine.getId());
    }

    @Test
    public void changeSuspension() {
        CarPart suspension = createDummySuspension();
        suspension.setPartName(suspension.getPartName() + " V2");
        suspension.setId(suspension.getId() + 90000L);

        when(carPartDAO.findById(suspension.getId())).thenReturn(suspension);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        carService.changeSuspension(suspension.getId(), dummyCar.getId());
        verify(carDAO, times(1)).update(dummyCar);
        verify(carPartDAO, times(1)).findById(suspension.getId());
    }

    @Test
    public void changeSuspensionCarIdNull() {
        CarPart suspension = createDummySuspension();
        suspension.setPartName(suspension.getPartName() + " V2");
        suspension.setId(suspension.getId() + 90000L);

        when(carPartDAO.findById(suspension.getId())).thenReturn(suspension);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        assertThrows(IllegalArgumentException.class, () -> carService.changeSuspension(suspension.getId(), null));
    }


    @Test
    public void changeFrontWing() {
        CarPart frontWing = createDummyFrontWing();
        frontWing.setPartName(frontWing.getPartName() + " V2");
        frontWing.setId(frontWing.getId() + 90000L);

        when(carPartDAO.findById(frontWing.getId())).thenReturn(frontWing);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        carService.changeFrontWing(frontWing.getId(), dummyCar.getId());
        verify(carDAO, times(1)).update(dummyCar);
        verify(carPartDAO, times(1)).findById(frontWing.getId());
    }



    @Test
    public void changeRearWing() {
        CarPart rearWing = createDummyRearWing();
        rearWing.setPartName(rearWing.getPartName() + " V2");
        rearWing.setId(rearWing.getId() + 90000L);

        when(carPartDAO.findById(rearWing.getId())).thenReturn(rearWing);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        carService.changeRearWing(rearWing.getId(), dummyCar.getId());
        verify(carDAO, times(1)).update(dummyCar);
        verify(carPartDAO, times(1)).findById(rearWing.getId());
    }


    @Test
    public void changeFrontWingCarIdIsNull() {
        CarPart rearWing = createDummyRearWing();
        rearWing.setPartName(rearWing.getPartName() + " V2");
        rearWing.setId(rearWing.getId() + 90000L);

        when(carPartDAO.findById(rearWing.getId())).thenReturn(rearWing);
        when(carDAO.findById(dummyCar.getId())).thenReturn(dummyCar);

        assertThrows(IllegalArgumentException.class, () -> carService.changeRearWing(rearWing.getId(), null));
    }

}
