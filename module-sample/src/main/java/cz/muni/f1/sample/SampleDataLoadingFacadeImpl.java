package cz.muni.f1.sample;

import cz.muni.f1.entity.Car;
import cz.muni.f1.entity.CarPart;
import cz.muni.f1.entity.Driver;
import cz.muni.f1.entity.EngineeringDepartment;
import cz.muni.f1.entity.SubDepartment;
import cz.muni.f1.entity.Team;
import cz.muni.f1.entity.User;
import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.PartType;
import cz.muni.f1.enums.UserType;
import cz.muni.f1.services.CarPartService;
import cz.muni.f1.services.CarService;
import cz.muni.f1.services.DriverService;
import cz.muni.f1.services.EngineeringDepartmentService;
import cz.muni.f1.services.SubDepartmentService;
import cz.muni.f1.services.TeamService;
import cz.muni.f1.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

/**
 * Heavily motivated by seminars.
 * Sorry not sorry?
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 */
@Component
@Transactional
public class SampleDataLoadingFacadeImpl implements SampleDataLoadingFacade {

    @Autowired
    private CarPartService carPartService;

    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private EngineeringDepartmentService engineeringDepartmentService;

    @Autowired
    private SubDepartmentService subDepartmentService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserService userService;

    @Override
    @SuppressWarnings("unused")
    public void loadData() {
        // Manager
        User manager = createUser("Christian", "Horner", "British", "chris.horner", "password", UserType.MANAGER);

        // Engineers
        User engineer1 = createUser("Cory", "J. Lucero", "American", "cory.jlucero", "password", UserType.ENGINEER);
        User engineer2 = createUser("Radek", "Feri", "Czech", "radek.feri", "password", UserType.ENGINEER);
        User engineer3 = createUser("René", "Trommler", "German", "rene.trommler", "password", UserType.ENGINEER);

        var engineers = new HashSet<User>();
        engineers.add(engineer1);
        engineers.add(engineer2);
        engineers.add(engineer3);

        // Drivers
        Driver driver1 = createDriver("Max", "Verstappen", "Dutch", 8, 9, 9, 8, 8);
        Driver driver2 = createDriver("Sergio", "Perez", "Mexican", 7, 8, 7, 6, 6);
        Driver driver3 = createDriver("Liam", "Lawson", "New Zealand", 7, 5, 5, 6, 6);

        var drivers = new HashSet<Driver>();
        drivers.add(driver1);
        drivers.add(driver2);
        drivers.add(driver3);

        // Front Wings
        CarPart frontWing1 = createCarPart("Front Wing Honda S42 Top-Drag", PartType.FRONT_WING, new JSONObject().put("Strength", 10).put("Drag", 9).put("Aero Rating", 8));
        CarPart frontWing2 = createCarPart("Front Wing Sony WH-1000XM4", PartType.FRONT_WING, new JSONObject().put("Strength", 8).put("Drag", 2).put("Aero Rating", 11));
        CarPart frontWing3 = createCarPart("Front Wing Mercedes Thin Carbon", PartType.FRONT_WING, new JSONObject().put("Strength", 16).put("Drag", 5).put("Aero Rating", 15));

        // Rear Wings
        CarPart rearWing1 = createCarPart("Rear Wing Honda S42 Top-Drag", PartType.REAR_WING, new JSONObject().put("Strength", 10).put("Drag", 9).put("Aero Rating", 8));
        CarPart rearWing2 = createCarPart("Rear Wing Sony WH-1000XM4", PartType.REAR_WING, new JSONObject().put("Strength", 8).put("Drag", 2).put("Aero Rating", 11));
        CarPart rearWing3 = createCarPart("Rear Wing Mercedes Thin Carbon", PartType.REAR_WING, new JSONObject().put("Strength", 16).put("Drag", 5).put("Aero Rating", 15));

        var aeroCarParts = new HashSet<CarPart>();
        aeroCarParts.add(frontWing1);
        aeroCarParts.add(frontWing2);
        aeroCarParts.add(frontWing3);
        aeroCarParts.add(rearWing1);
        aeroCarParts.add(rearWing2);
        aeroCarParts.add(rearWing3);

        // Suspensions
        var suspension1 = createCarPart("Suspension Rock Shox Super Deluxe", PartType.SUSPENSION, new JSONObject().put("Durability", 9).put("Weight Rating", 6).put("Rebound", 7));
        var suspension2 = createCarPart("Suspension Formula MOD Lightweight", PartType.SUSPENSION, new JSONObject().put("Durability", 6).put("Weight Rating", 10).put("Rebound", 6));
        var suspension3 = createCarPart("Suspension Ohlins shock TTX GP", PartType.SUSPENSION, new JSONObject().put("Durability", 11).put("Weight Rating", 4).put("Rebound", 10));

        var suspCarParts = new HashSet<CarPart>();
        suspCarParts.add(suspension1);
        suspCarParts.add(suspension2);
        suspCarParts.add(suspension3);

        // Engines
        var engine1 = createCarPart("Engine Honda Fuel-Power", PartType.ENGINE, new JSONObject().put("Fuel Efficiency", 5).put("Power", 10));
        var engine2 = createCarPart("Engine Mercedes Balanced F1", PartType.ENGINE, new JSONObject().put("Fuel Efficiency", 8).put("Power", 6));
        var engine3 = createCarPart("Engine Shell V-Power", PartType.ENGINE, new JSONObject().put("Fuel Efficiency", 7).put("Power", 9));

        var engCarParts = new HashSet<CarPart>();
        engCarParts.add(engine1);
        engCarParts.add(engine2);
        engCarParts.add(engine3);

        // Cars
        var car1 = createCars(driver1, engine1, frontWing1, rearWing1, suspension1);
        var car2 = createCars(driver2, engine2, frontWing2, rearWing2, suspension2);

        // SubDepartments
        var aeroSubDepartment = createSubDepartment(DepartmentType.AeroDynamicsDepartment, engineer1, aeroCarParts);
        var engSubDepartment = createSubDepartment(DepartmentType.EngineDepartment, engineer2, engCarParts);
        var suspSubDepartment = createSubDepartment(DepartmentType.SuspensionDepartment, engineer3, suspCarParts);

        // EngineeringDepartment
        var engineeringDepartment = createEngineeringDepartment(engineers, engSubDepartment, aeroSubDepartment, suspSubDepartment);

        // Team
        var team = createTeam(manager, car1, car2, drivers, engineeringDepartment);
    }

    private User createUser(String name, String surname, String nationality, String username, String password, UserType role) {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setNationality(nationality);
        user.setUsername(username);
        user.setPassword(SHA256Hash(password));
        user.setRole(role);

        userService.create(user);
        return user;
    }

    private Driver createDriver(String name, String surname, String nationality, int agresiveness, int qualificationPace, int racePace, int racePerformance, int wetPerformance) {
        Driver driver = new Driver();
        driver.setName(name);
        driver.setSurname(surname);
        driver.setNationality(nationality);
        driver.setAgresiveness(agresiveness);
        driver.setQualificationPace(qualificationPace);
        driver.setRacePace(racePace);
        driver.setRacePerformance(racePerformance);
        driver.setWetPerformance(wetPerformance);

        driverService.create(driver);
        return driver;
    }

    private CarPart createCarPart(String partName, PartType partType, JSONObject partInfo) {
        CarPart carPart = new CarPart();
        carPart.setPartName(partName);
        carPart.setPartType(partType);
        carPart.setPartInfo(partInfo.toString());

        carPartService.create(carPart);
        return carPart;
    }

    private Car createCars(Driver driver, CarPart engine, CarPart frontWing, CarPart rearWing, CarPart suspension) {
        var car = new Car();
        car.setDriver(driver);
        car.setEngine(engine);
        car.setFrontWing(frontWing);
        car.setRearWing(rearWing);
        car.setSuspension(suspension);

        carService.create(car);
        return car;
    }

    private SubDepartment createSubDepartment(DepartmentType departmentType, User engineer, Set<CarPart> carParts) {
        SubDepartment subDepartment = new SubDepartment();
        subDepartment.addEngineer(engineer);
        subDepartment.setDepartmentType(departmentType);
        subDepartment.setCarParts(carParts);

        subDepartmentService.create(subDepartment);
        return subDepartment;
    }

    private EngineeringDepartment createEngineeringDepartment(Set<User> engineers, SubDepartment subDeptEng, SubDepartment subDeptAero, SubDepartment subDeptSusp) {
        EngineeringDepartment engineeringDepartment = new EngineeringDepartment();
        for (var engineer : engineers) {
            engineeringDepartment.addEngineer(engineer);
        }
        engineeringDepartment.setEngineDepartment(subDeptEng);
        engineeringDepartment.setAeroDepartment(subDeptAero);
        engineeringDepartment.setSuspensionDepartment(subDeptSusp);

        engineeringDepartmentService.create(engineeringDepartment);
        return engineeringDepartment;
    }

    private Team createTeam(User manager, Car car1, Car car2, Set<Driver> drivers, EngineeringDepartment engineeringDepartment) {
        var team = new Team();
        team.setManager(manager);
        team.addCar(car1);
        team.addCar(car2);
        team.setDrivers(drivers);
        team.setEngineeringDepartment(engineeringDepartment);
        teamService.create(team);
        return team;
    }

    /**
     * Helper function for generation of SHA256 hash for password.
     * For the generation of hashes from FE, typescript is used while sending new user.
     *
     * @param s string to be hashed
     * @return SHA256 hash of string `s`
     */
    private String SHA256Hash(String s) {
        // source: https://www.baeldung.com/sha-256-hashing-java
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(s.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return ""; // should never happen.
        }
    }

}
