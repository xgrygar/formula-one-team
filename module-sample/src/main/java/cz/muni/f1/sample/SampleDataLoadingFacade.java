package cz.muni.f1.sample;

/**
 * Heavily motivated by seminars.
 * Sorry not sorry?
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 */
public interface SampleDataLoadingFacade {

    void loadData();
}
