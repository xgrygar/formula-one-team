package cz.muni.f1.sample;

import cz.muni.f1.dao.CarPartDAO;
import cz.muni.f1.dao.EngineeringDepartmentDAO;
import cz.muni.f1.dao.SubDepartmentDAO;
import cz.muni.f1.dao.UserDAO;
import cz.muni.f1.enums.DepartmentType;
import cz.muni.f1.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.testng.annotations.Test;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Heavily motivated by seminars.
 * Sorry not sorry?
 *
 * @author Jan Popelas <xpopelas@fi.muni.cz>
 */
@ContextConfiguration(classes = {SampleDataConfiguration.class})
@TestExecutionListeners(TransactionalTestExecutionListener.class)
@Transactional
public class SampleDataLoadingFacadeTest extends AbstractTestNGSpringContextTests {

    @Autowired
    public UserDAO userDAO;

    @Autowired
    public CarPartDAO carPartDAO;

    @Autowired
    public SubDepartmentDAO subDepartmentDAO;

    @Autowired
    public EngineeringDepartmentDAO engineeringDepartmentDAO;

    @Test
    public void atLeastOneManagerIsCreated() {
        var allUsers = userDAO.findAll();
        assertThat(allUsers)
                .isNotEmpty()
                .filteredOn(x -> x.getRole().equals(UserType.MANAGER))
                .isNotEmpty();
    }

    @Test
    public void atLeastOneEngineerIsCreated() {
        var allUsers = userDAO.findAll();
        assertThat(allUsers)
                .isNotEmpty()
                .filteredOn(x -> x.getRole().equals(UserType.ENGINEER))
                .isNotEmpty();
    }

    @Test
    public void atLeastOneCarPartCreated(){
        var allCarParts = carPartDAO.findAll();
        assertThat(allCarParts)
                .isNotEmpty();
    }

    @Test
    public void atLeastOneEngineSubDepartmentCreated() {
        var allSubDepartments = subDepartmentDAO.findAll();
        assertThat(allSubDepartments)
                .isNotEmpty()
                .filteredOn(x -> x.getDepartmentType() == DepartmentType.EngineDepartment)
                .isNotEmpty();
    }

    @Test
    public void atLeastOneEngineeringDepartmentCreated() {
        var allEngineeringDepartments = engineeringDepartmentDAO.findAll();
        assertThat(allEngineeringDepartments)
                .isNotEmpty();
    }
}
